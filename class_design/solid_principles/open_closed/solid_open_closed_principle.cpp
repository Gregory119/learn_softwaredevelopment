/*
  SOLID DESIGN PRINCIPLES
  
  O - A class should be open for extension, but closed for modification.

 */

//////////////////////////////////////////////////////////////////////////

class Shape
{
  // special members
  
public:
  virtual void draw() = 0;
};

//////////////////////////////////////////////////////////////////////////

class Rectangle final : Shape
{
  // special members
  
public:
  void draw() override
  {
    // ..
  }
};

//////////////////////////////////////////////////////////////////////////

class Circle final : Shape
{
  // special members
  
public:
  void draw() override
  {
    // ..
  }
};

//////////////////////////////////////////////////////////////////////////

class GuiEditor final
{
  // special members
  
public:
  void drawShape(Shape* shape)
  {
    shape->draw();
  }
};

/*
  If a new oval shape is created, the GuiEditor will be able to draw the shape without having to change the code of the GuiEditor. In addition, the GuiEditor is not dependent on the shape type, so adding a new shape does not require a recompilation of the GuiEditor.
 */

int main(int argc, char** argv)
{
  std::unique_ptr<GuiEditor> d_gui_editor = std::make_unique<GuiEditor>();

  std::unique_ptr<Shape> d_shape = std::make_unique<Circle>();

  d_gui_editor->drawShape(d_shape.get());
  
  return 0;
}
