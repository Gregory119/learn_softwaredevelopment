/*
  SOLID DESIGN PRINCIPLES
  
  S - A class should satisfy a single responsibility (or reason/concern/).
 */

//////////////////////////////////////////////////////////////////////////
// Journal Class - with a single responsibility (adding/removing notes)
//////////////////////////////////////////////////////////////////////////

class Journal final
{
public:
  Journal(std::string name);
  void addNote(const std::string& msg);
  void removeLastNote();
  std::string& getName() const { return d_name; }
  std::list<std::list> getNotes() const { return d_notes; }

  // Having a save() function here would be adding another concern of this structure. If the class initially is designed to save to a file using:
  // void save(const std::string& filename) const
  // and then the class needs to be saved to a a google drive database, or only some need to be saved to a google drive, then the parameters, function name, and function parameters, and function implementation will probably need to change. These changes will cascade to every call of this function on every object. This can be avoided by passing the saving responsibility to a separate concrete class or, even better, a class derived from an interface. The latter case is more decoupled, removing the need to recompile the Journal class, if a change is made in the saving class.

private:
  std::string d_name;
  std::list<std::string> d_notes;
};

//----------------------------------------------------------------------//
Journal::Journal(std::string name)
  : d_name(std::move(name))
{}

//----------------------------------------------------------------------//
void Journal::addNote(const std::string& msg)
{
  d_notes.push_back(msg);
}

//----------------------------------------------------------------------//
void Journal::removeLastNote()
{
  d_notes.pop_back();
}

//////////////////////////////////////////////////////////////////////////
// Journal Saver (Interface)
//////////////////////////////////////////////////////////////////////////

// I could have an IJournalSaver owner interface, IJournalSaverOwner, for error callbacks to the owner of IJournalSaver from the child classes of IJournalSaver.

class IJournalSaver
{
public:
  virtual ~IJournalSaver() = default; // 1) needed as an interface
  IJournalSaver() = default; // 4) needed because copy constructor was declared (any declared constructor does not default the default constructor

protected:
  IJournalSaver(const IJournalSaver&) = default; // 2) needed because of the desctructor and it should only be copied by the child class
  IJournalSaver& operator=(const IJournalSaver&) = default; // 3) needed because of the desctructor and it should only be copied by the child class
  IJournalSaver(IJournalSaver&&) = default; // 5) needed because of the desctructor and it should only be moved by the child class
  IJournalSaver& operator=(IJournalSaver&&) = default; // 6) needed because of the desctructor and it should only be moved by the child class

public:
  virtual bool save(const Journal&) = 0;
  virtual bool load(Journal&) = 0;
};

//////////////////////////////////////////////////////////////////////////
// File Journal Saver
//////////////////////////////////////////////////////////////////////////

class FileJournalSaver final : IJournalSaver
{
public:
  FileJournalSaver(const std::string& filename);
  
  bool save(const Journal&) override;
  bool load(Journal&) override;

private:
  std::fstream d_file;
};

//----------------------------------------------------------------------//
FileJournalSaver::FileJournalSaver(const std::string& filename)
  : d_file(filename) // will close the file automatically on destruction
{}

//----------------------------------------------------------------------//
bool FileJournalSaver::save(const Journal&)
{
  std::cout << "Saving Journal to a file" << std::endl;
  return true;
}

//----------------------------------------------------------------------//
bool FileJournalSaver::load(Journal&)
{
  std::cout << "Loading Journal from a file" << std::endl;
  return true;
}
  

//////////////////////////////////////////////////////////////////////////
// Google Drive Journal Saver
//////////////////////////////////////////////////////////////////////////

class GDriveJournalSaver final : IJournalSaver
{
public:
  GDriveJournalSaver(const std::string& gdrive_name);
  bool connect(); // Should actually have a callback interface to handle a (un)successful connection instead of a bool
  bool save(const Journal&) override;
  bool load(Journal&) override;

private:
  // something to connect and read/write files from google drive
};

//----------------------------------------------------------------------//
GDriveJournalSaver::GDriveJournalSaver(const std::string& gdrive_name)
{
  // connect to the google drive
}

//----------------------------------------------------------------------//
bool GDriveJournalSaver::save(const Journal&)
{
  std::cout << "Saving Journal to Google drive." << std::endl;
  return true;
}

//----------------------------------------------------------------------//
bool GDriveJournalSaver::load(Journal&)
{
  std::cout << "Loading Journal from Google drive." << std::endl;
  return true;
}

//////////////////////////////////////////////////////////////////////////
// Possible Test Case
//////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
  std::unique_ptr<Journal> first_journal = std::make_unique<Journal>("First Journal");

  std::unique_ptr<IJournalSaver> journal_saver = std::make_unique<GDriveJournalSaver>("Greg's Drive");
  journal_saver->load(first_journal);
  
  return 0;
}
