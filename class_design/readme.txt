The programming language design abstractions are base on the following:

 ===== UML Notation =====
Class Diagrams - https://vaughnvernon.co/?page_id=31
Class and Sequence Diagrams - http://umich.edu/~eecs381/handouts/UMLNotationSummary.pdf

 ===== SOLID Design Principles =====
https://www.oodesign.com/
Lecture by "uncle Bob" - https://www.youtube.com/watch?v=TMuno5RZNeE
Video playlist - https://www.youtube.com/watch?v=hGf2upfDpdo&list=PL6n9fhu94yhXjG1w2blMXUzyDrZ_eyOme&index=2

 ===== Design Patterns =====
https://www.oodesign.com/
