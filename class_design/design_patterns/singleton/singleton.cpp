// early and lazy instantiation
// use with threading and locking
// thread_local?
// use with creational pattern
// look at logging implementation

/*
A singleton is used for something that there should only be one instance of.
Reading values from a singleton within a class is like reading from a global
variable. This makes testing difficult because every possible value of this
read global variable would need to be tested for every function that uses it. 
A better approach would be to pass the required value as a function/constructor 
parameter. Ideally have one class reading the singleton value/s and passing
them to the classes that require them.

Example: Configuration File
There could be one configuration file that should only be read.
The configuration can be read by multiple classes of different processes, 
but written to by a single class of a single process or by a user (multiple 
writes from different points would make debugging and control difficult
and messy).

Example: Factory
Only one factory is needed to create classes of a specific parent/base type.
The factory can be used by some top hierachy class to create a desired instance,
which can then be moved to the class that needs to use it.
*/

#include <iostream>
#include <string>
#include <mutex>
#include <memory>

using namespace std;

//----------------------------------------------------------------------//
// sensor.h
// polymorphic base class
class Sensor
{
public:
  Sensor(string name)
    : d_name(move(name))
  {}
  
  virtual ~Sensor() {}
  // Declare and define all other special member functions because the destructor has been user declared.
  
  virtual bool readPinVal() = 0; // a clear responsibility

  string& getName() { return d_name; } // a simple get functions does not add a new responsibility

private:
  string d_name = "No Name";
};

//----------------------------------------------------------------------//
class SensorA final : public Sensor
{
public:
  SensorA(string name)
    : Sensor(move(name))
  {}
  
  bool readPinVal() override;
};

//----------------------------------------------------------------------//
bool SensorA::readPinVal()
{
  cout << "readPinVal of sensor " << getName() << endl;
}

//----------------------------------------------------------------------//
// sensorb.h
class SensorB final : public Sensor
{
public:
  SensorB(string name)
    : Sensor(move(name))
  {}
  
  bool readPinVal() override;
};

//----------------------------------------------------------------------//
bool SensorB::readPinVal()
{
  cout << "readPinVal of sensor " << getName() << endl;
}

//----------------------------------------------------------------------//
// sensortypes.h
enum class SensorType
  {
    A,
    B
  };

//----------------------------------------------------------------------//
// LAZY INSTANTIATION
//----------------------------------------------------------------------//
// sensorfactorylazy.h
// #include "sensora.h"
// #include "sensorb.h"
// #include "sensortypes.h"
class SensorFactLazyInst
{
public:
  static shared_ptr<SensorFactLazyInst> getInstance(); // use internal thread synchronization for efficiency
  unique_ptr<Sensor> createSensor(SensorType t, string name); // no thread synchronization is needed here

private:
  SensorFactLazyInst() = default; // Forcing a call of getInstance() in order to call any member functions.
  
private:
  static shared_ptr<SensorFactLazyInst> d_single_sensor;
  static mutex d_m;

  //  class Enabler;
};

shared_ptr<SensorFactLazyInst> SensorFactLazyInst::d_single_sensor;
mutex SensorFactLazyInst::d_m;

//----------------------------------------------------------------------//
shared_ptr<SensorFactLazyInst> SensorFactLazyInst::getInstance()
{
  // This version is not thread safe because multiple instances can be created by multiple threads.
  /*
  if (d_single_sensor == nullptr) // multiple threads can get to this at the same time
    {
      d_single_sensor = make_unique<SensorFactLazyInst>();
    }
  */

  class Enabler : public SensorFactLazyInst {};
  
  // Using a double locking mechanism, we can make the access thread safe.
  if (d_single_sensor == nullptr) // multiple threads get here
    {
      lock_guard<mutex> lock(d_m); // synchronize at this point (one thread at a time)
      if (d_single_sensor == nullptr)
        {
          d_single_sensor = make_shared<Enabler>();
        }
    }

  // There is an alternative thread safe option here that uses the new c++11 memory model, but I need to understand it first before adding it here.
  return d_single_sensor;
}

//----------------------------------------------------------------------//
// sensorfactorylazy.cpp
unique_ptr<Sensor> SensorFactLazyInst::createSensor(SensorType t, string name)
{  
  switch (t)
    {
    case SensorType::A:
      return static_cast<unique_ptr<Sensor>>(make_unique<SensorA>(name));

    case SensorType::B:
      return unique_ptr<Sensor>(make_unique<SensorB>(name));
    }
  // assert and call error handler
}

//----------------------------------------------------------------------//
// EARLY INSTANTIATION
//----------------------------------------------------------------------//
// sensorfactoryearly.h
// #include "sensora.h"
// #include "sensorb.h"
// #include "sensortypes.h"
class SensorFactEarlyInst
{
public:
  // No thread synchronization is needed in the early instantiation version (Pro), but an instance will be created every time the program is loaded (Con).
  static shared_ptr<SensorFactEarlyInst> getInstance();
  unique_ptr<Sensor> createSensor(SensorType t, string name);

private:
  SensorFactEarlyInst() = default; // Forcing a call of getInstance() in order to call any member functions.
  
private:
  static shared_ptr<SensorFactEarlyInst> d_single_sensor; // This is created during the loading of the program memory.

  class Enabler;
};

class SensorFactEarlyInst::Enabler : public SensorFactEarlyInst {};

shared_ptr<SensorFactEarlyInst> SensorFactEarlyInst::d_single_sensor = make_shared<SensorFactEarlyInst::Enabler>();

//----------------------------------------------------------------------//
shared_ptr<SensorFactEarlyInst> SensorFactEarlyInst::getInstance()
{
  return d_single_sensor;
}

//----------------------------------------------------------------------//
unique_ptr<Sensor> SensorFactEarlyInst::createSensor(SensorType t,
                                                     string name)
{
  switch (t)
    {
    case SensorType::A:
      return unique_ptr<Sensor>(make_unique<SensorA>(name));

    case SensorType::B:
      return unique_ptr<Sensor>(make_unique<SensorB>(name));
    }
  // assert and call error handler
}

//----------------------------------------------------------------------//
// INSTANTIATION WITH CONSTRUCTOR (FORM OF LAZY INSTANTIATION)
//----------------------------------------------------------------------//
// sensorfactoryconstruct.h
// #include "sensora.h"
// #include "sensorb.h"
// #include "sensortypes.h"
class SensorFactEarlyInstConstr final
{
public:
  SensorFactEarlyInstConstr();
  
  static unique_ptr<Sensor> staticCreateSensor(SensorType t, string name);
  // Need to call constructor to create first instance, in order to call member functions without asserting/error handling.
  // These functions then use the singleton instance to call the private member functions.
  // Because this is static, there must be an instantiation check inside it. If there are multiple static functions, this adds repeated inefficient checks.

private:
  unique_ptr<Sensor> createSensor(SensorType t, string name);
  
private:
  static SensorFactEarlyInstConstr* d_single_sensor; // This is created during the loading of the program memory.
  mutex d_m;
};

SensorFactEarlyInstConstr* SensorFactEarlyInstConstr::d_single_sensor = nullptr;

//----------------------------------------------------------------------//
SensorFactEarlyInstConstr::SensorFactEarlyInstConstr()
{
  if (d_single_sensor == nullptr)
    {
      lock_guard<mutex> lock(d_m);
      if (d_single_sensor == nullptr)
        {
          d_single_sensor = this;
        }
    }
  else
    {
      // assert(false); ONLY ALLOW ONE INSTANCE
    }
}

//----------------------------------------------------------------------//
unique_ptr<Sensor> SensorFactEarlyInstConstr::staticCreateSensor(SensorType t,
                                                                 string name)
{
  if (d_single_sensor)
    {
      return d_single_sensor->createSensor(t, name);
    }
  else
    {
      // assert(false)
      return nullptr;
    }
}

//----------------------------------------------------------------------//
unique_ptr<Sensor> SensorFactEarlyInstConstr::createSensor(SensorType t,
                                                           string name)
{
  switch (t)
    {
    case SensorType::A:
      return unique_ptr<Sensor>(make_unique<SensorA>(name));

    case SensorType::B:
      return unique_ptr<Sensor>(make_unique<SensorB>(name));
    }
  // assert and call error handler
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  // ===== LAZY INSTANTIATION EXAMPLE (using getInstance()) =====
  unique_ptr<Sensor> sensor1 =
    SensorFactLazyInst::getInstance()->createSensor(SensorType::A,"Sensor1");

  // or first save the instance
  shared_ptr<SensorFactLazyInst> sensor_fact_lazy =
    SensorFactLazyInst::getInstance();

  unique_ptr<Sensor> sensor2 =
    sensor_fact_lazy->createSensor(SensorType::B,"Sensor2");

  unique_ptr<Sensor> sensor3 =
    sensor_fact_lazy->createSensor(SensorType::A,"Sensor3");

  // ===== LAZY INSTANTIATION EXAMPLE (using the constructor) =====
  // Need a single instance first. The lifetime of this single instance
  // must last while it is being used.
  
  // SensorFactEarlyInstConstr::createSensor(SensorType::A,"SensorGeneral"); // This will assert because and instance does not exist yet
  
  SensorFactEarlyInstConstr sensor_fact_early_constr;
  
  unique_ptr<Sensor> sensor4 =
    SensorFactEarlyInstConstr::staticCreateSensor(SensorType::A,"Sensor4"); // call directly through class type

  unique_ptr<Sensor> sensor5 =
    sensor_fact_early_constr.staticCreateSensor(SensorType::B,"Sensor5");
 
  // ===== EARLY INSTANTIATION EXAMPLE =====
  unique_ptr<Sensor> sensor_7 =
    SensorFactEarlyInst::getInstance()->createSensor(SensorType::A,"Sensor7");

  // or first save the instance
  shared_ptr<SensorFactEarlyInst> sensor_fact_early =
    SensorFactEarlyInst::getInstance();
  
  unique_ptr<Sensor> sensor_8 =
    sensor_fact_early->createSensor(SensorType::B,"Sensor8");
  unique_ptr<Sensor> sensor_9 =
    sensor_fact_early->createSensor(SensorType::A,"Sensor9");

  // ===== SUMMARY =====
  /*
    Lazy instantiation has the pro that an instance is created only when needed, 
    but it has the con that thread synchronization (double locking) is needed.
    The getInstance() version requires checking the validity of an instance
    creation. The constructor version has undesireable instance checks in the constructor
    and static member functions, and there is the possibility of an undesireable 
    assert in the constructor.

    Early instantiation has the pro that no thread synchronization is needed
    because an instance is created when the program is loaded into memory. In 
    addition, the validitiy of an instance creation is not needed. However,
    it has the con that the instance memory is wasted if it is not used in the program.

    Although it requires more code and checks, the lazy instantiation with getInstance()
    is more desireable in order to save memory. The latest double locking method that uses
    the new memory model should be looked in to.

    ADDITIONAL NOTES: 
    Static member variables (also known as class variables) must be initialized outside of the class declaration body.
   */
  
  return 0;
}
