#include <functional>
#include <iostream>
#include <memory>

//----------------------------------------------------------------------//
class Base
{
public:
  Base(int val)
    : d_val(val)
  {}
  
  virtual void doSomething() = 0;
  int getVal() const { return d_val; }

private:
  int d_val = 0;
};

//----------------------------------------------------------------------//
class Derived1 : public Base
{
public:
  Derived1(int val,
	   std::function<void(Base&)> func)
    : Base(val),
      d_func(func)
  {}
  
  void doSomething() override
  {
    std::cout << "Derived1::doSomething" << std::endl;
    d_func(*this);
  }

private:
  std::function<void(Base&)> d_func;
};

//----------------------------------------------------------------------//
class Derived2 : public Base
{
public:
  Derived2(int val,
	   std::function<void(Base&)> func)
    : Base(val),
      d_func(func)
  {}
  
  void doSomething() override
  {
    std::cout << "Derived2::doSomething" << std::endl;
    d_func(*this);
  }

private:
  std::function<void(Base&)> d_func;
};

//----------------------------------------------------------------------//
void funcA(Base& b)
{
  std::cout << "funcA\n"
	    << b.getVal() << std::endl;
}

//----------------------------------------------------------------------//
void funcB(Base& b)
{
  std::cout << "funcB\n"
	    << b.getVal() << std::endl;
}

//----------------------------------------------------------------------//
int main(int argc, char* argv[])
{
  std::unique_ptr<Base> b1 = std::make_unique<Derived1>(6,funcA);
  std::unique_ptr<Base> b2 = std::make_unique<Derived1>(10,funcB);
  std::unique_ptr<Base> b3 = std::make_unique<Derived2>(3,funcB);
  std::unique_ptr<Base> b4 = std::make_unique<Derived2>(1,funcA);

  b1->doSomething();
  b2->doSomething();
  b3->doSomething();
  b4->doSomething();
  
  return 0;
}
