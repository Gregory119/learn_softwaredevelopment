#include <vector>
#include <algorithm>
#include <memory>
#include <iostream>

class Deck final
{
public:
  enum Suit
    {
      Spade,
      Heart,
      Club,
      Diamond
    };

  enum Rank
    {
      Ace,
      Two,
      Three,
      Four,
      Five,
      Six,
      Seven,
      Eight,
      Nine,
      Ten,
      Jack,
      King,
      Queen
    };
  
  struct Card
  {
    Card() = default;
    Card(Suit s, Rank r)
      : suit(s),
        rank(r)
    {}

    Suit suit = Spade;
    Rank rank = Ace;
  };

public:
  Deck();

  bool dealCard(Card&);
  void shuffle();
  
private:
  std::vector<Card> d_cards;
};

//----------------------------------------------------------------------//
Deck::Deck()
{
  d_cards.reserve(52);

    for (int suit = Spade; suit<= Diamond; ++suit)
      {
        for (int rank = Ace; rank<=Queen; ++rank)
          {
            d_cards.emplace_back(static_cast<Suit>(suit),
                                 static_cast<Rank>(rank));
          }
      }
}

//----------------------------------------------------------------------//
bool Deck::dealCard(Card& card)
{
  if (d_cards.empty())
    {
      return false;
    }

  card = d_cards.back();
  d_cards.pop_back();
  return true;
}

//----------------------------------------------------------------------//
void Deck::shuffle()
{
  std::random_shuffle(d_cards.begin(),d_cards.end());
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  std::unique_ptr<Deck> deck = std::make_unique<Deck>();

  deck->shuffle();

  Deck::Card dealt_card;
  if (!deck->dealCard(dealt_card))
    {
      std::cout << "Failed to deal card" << std::endl;
    }
  
  return 0;
}
