#include <iostream>

class Base
{
public:
  virtual ~Base() = default;
private:
  int dogs = 5;
};

class Derived : public Base
{
public:
};
  
int main(int argc, char* argv[])
{
  Base base;
  Derived derived;

  Base *pBase = &base;
  std::cout << typeid(*pBase).name() << std::endl;

  pBase = &derived;
  std::cout << typeid(*pBase).name() << std::endl;

  Derived *pDerived = &derived;
  std::cout << typeid(*pDerived).name() << std::endl;
  
  return 0;
};
