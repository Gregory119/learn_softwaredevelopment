#include <iostream>

using namespace std;

class BaseNonVirtualWithStaticData
  {
  public:
    static int num;
  };

int main(int argc, char* argv[])
{
  // References:
  // https://www.cprogramming.com/tutorial/size_of_class_object.html
  // https://wr.informatik.uni-hamburg.de/_media/teaching/wintersemester_2013_2014/epc-14-haase-svenhendrik-alignmentinc-paper.pdf
  /*
    Depends on:
    - Architecture
    - OS
    - compiler
    - alignment (alignment type, data member order, data member sizes)
    - Size of directly inherited classes
    - Presence of virtual functions
   */
  
  /*
    ======================================================================
    Struct/class sizes based on alignment.
    ======================================================================
   */
  // Assuming a 64 bit machine on linux, the natural alignment and size for each data type are:
  // char => 1 byte
  // short => 2 byte
  // int => 4 byte
  // long => 8 bytes
  // float => 4 bytes (specified by IEEE)
  // double => 8 bytes (specified by IEEE)
  // NB!! Natural alignment always uses the byte size of the largest data member.
  
  struct Test1
  {
    // The largest data member here is 8 bytes so the alignment is 8 bytes.
    char c; // 1 byte => 8 byte
    double a; // 8 byte (fixed IEEE standard size) => 8 byte 
    char d; // 1 byte => 8 byte
  };
  // Size should be 8*3 = 24 bytes
  cout << "sizeof(Test1) = " << sizeof(Test1) << endl;

  struct Test2
  {
    // The largest data member here is 4 bytes so the alignment is 4 bytes.
    char c; // 1 byte => 4 byte
    int a; // 4 byte => 4 byte 
    short d; // 2 byte => 4 byte
  };
  // Size should be 4*3 = 12 bytes
  cout << "sizeof(Test2) = " << sizeof(Test2) << endl;

  struct Test3
  {
    // The largest data member here is 4 bytes so the alignment is 4 bytes.
    char c; // 1 byte => 4 byte
    short d; // 2 byte => fits into the remaing space of the previous four bytes.
    int a; // 4 byte => 4 byte 
  };
  // Size should be 4*2 = 8 bytes
  // Placing the smaller sized members first reduces the struct size.
  cout << "sizeof(Test3) = " << sizeof(Test3) << endl;

  /*
    ======================================================================
    Struct/class sizes based on functions, data members, and inheritance.
    ======================================================================
   */
  
  class Empty
  {};
  // Will be 1 byte.
  cout << "sizeof(Empty) = " << sizeof(Empty) << endl;
  
  class BaseNonVirtualWithFuncsOnly
  {
    void func1() {}
    void func2() {}
  };
  // Will be 1 byte. Functions are stored external to the class type to save memory on each instance. The object instance is detected with the 'this' pointer, passed as a function parameter.
  cout << "sizeof(BaseNonVirtualWithFuncsOnly) = " << sizeof(BaseNonVirtualWithFuncsOnly) << endl;

  // Will be 1 byte. The static member is not stored in the class type (class declared at the top of the file).
  cout << "sizeof(BaseNonVirtualWithStaticData) = " << sizeof(BaseNonVirtualWithStaticData) << endl;

  class BaseVirtual
  {
    virtual void func1() {}
    virtual void func2() {}
  };
  // Includes the virtual table pointer, which is 8 bytes.
  cout << "sizeof(BaseVirtual) = " << sizeof(BaseVirtual) << endl;

  /*
    ======================================================================
    Compiler dependent.
    ======================================================================
  */
  class BaseNonVirtualWithNonStaticData
  {
  private:
    int a;
    char c;
  };
  
  class Derived1 : public BaseNonVirtualWithNonStaticData
  {
  public:
    char c;
    int b;
  };
  cout << "sizeof(Derived1) = " << sizeof(Derived1) << endl;
  
  return 0;
}
