#include <iostream>

#include <signal.h>
#include <unistd.h>

void signalCallbackHandler(int signum)
{
  std::cout << "Caught signal " << signum
	    << " and exiting with the same signal." << std::endl;

  exit(signum);
}

int main (int argc, char** argv)
{
  signal(SIGINT, signalCallbackHandler);
  signal(SIGSEGV, signalCallbackHandler);

  while (1)
    {
      std::cout << "Looping.." << std::endl;
      sleep(1);

      // This will cause a segmentation vault signal.
      int* val = nullptr;
      *val = 5;
    }
  
  return EXIT_SUCCESS;
}
