#include <iostream>
#include <string>

using namespace std;

// Global scope

// Global variables:
// - All have static duration.
// - External or internal linkage.
// - All have file scope, but this term informally refers to global variables with internal linkage.
// - All have global scope, but this term informally refers to global variables with external linkage.

const int g_internal_const = 5; // Internal linkage, static duration, file scope (global variable), constant.
static int g_internal_nonconst = 4; // Internal linkage, static duration, file/global/(global namespace) scope, non-constant.
extern int g_external_nonconst = 6; // External linkage, static duration, file/global/(global namespace) scope, non-constant.
int external_nonconst = 7; // External linkage, static duration, file/global/(global namespace) scope, non-const.

void print()
{// Block scope
  int num = 2; // No linkage, automatic duration, local scope.
  static int num2 = 6; // No linkage, automatic duration, local scope.
  cout << "g_internal_const = " << g_internal_const << endl;
}

int main(int argc, char ** argv)
{
  int g_internal_const = 7; // Using the same name for shadowing purposes.
  cout << "g_internal_const = " << g_internal_const << endl;
  
  return 0;
}
