#include <memory>

using namespace std;

// Testing code of page 160
class Base
{
public:
  virtual ~Base() {}
  Base() = default;

  // default copyable
  Base(const Base&) = default;
  Base& operator=(const Base&) = default;

  // default moveable
  Base(Base&&) = default;
  Base& operator=(Base&&) = default;

public:
  virtual void vf1() = 0;
};

void Base::vf1()
{
  ///
}

class Derived final : public Base
{
public:
  void vf1() override
  {
    Base::vf1(); // This default inplementation call requires that Base::vf1() refers to a default implementation.
  }
};

void func(Base* bb)
{
  bb->vf1();
}

int main(int argc, char** argv)
{
  //unique_ptr<Derived> d = make_unique<Derived>();
  unique_ptr<Base> b = unique_ptr<Base>(new Derived());
  shared_ptr<Derived> sb = make_unique<Derived>();
  func(b.get());
  b->vf1();
  sb->vf1();
  
  return 0;
}
