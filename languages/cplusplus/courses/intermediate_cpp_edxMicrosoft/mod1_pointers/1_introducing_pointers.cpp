#include <iostream>

using namespace std;

int main(int argc, char* argv[])
{
  int num = 3;
  int* pNum = &num;
  cout << pNum << endl;
  cout << *pNum << endl;

  *pNum = 45;
  cout << pNum << endl;
  cout << *pNum << endl;
  
  return 0;
}
