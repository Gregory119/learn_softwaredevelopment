#include <iostream>

using namespace std;

void passByRef(int& val)
{
  cout << "&val = " << &val << endl;
  cout << "val = " << val << endl;
  ++val;
  cout << "val = " << val << endl;
}

int main(int argc, char* argv[])
{
  int num =5;
  cout << "&num = " << &num << endl;
  cout << "Before passByRef(), num = " << num << endl;
  passByRef(num);
  cout << "After passByRef(), num = " << num << endl;
  
  return 0;
}
