#include <iostream>
#include <string>

using namespace std;

class Person
{
public:
  Person(string name)
    : d_name(move(name))
  {
    cout << "Person(string) is called." << endl;
  }

  ~Person()
  {
    cout << "~Person() is called." << endl;
  }

  const string& getName()
  {
    return d_name;
  }

private:
  string d_name;
};

int main(int argc, char* argv[])
{
  Person* pPerson = new Person("Greg");
  const string &name = pPerson->getName();

  cout << "pPerson = " << pPerson << endl;
  cout << "name = " << name << endl;
  cout << "sizeof(pPerson) = " << sizeof(pPerson) << endl;

  delete pPerson;
  pPerson = nullptr; 
  
  return 0;
}
