#include <iostream>

using namespace std;

int main(int argc, char* argv[])
{
  // Fundamental types
  int* pInt = new int;
  *pInt = 5;

  double* pDouble = new double;
  *pDouble = 7;

  cout << "pInt = " << pInt << endl;
  cout << "pDouble = " << pDouble << endl;
  cout << "*pInt = " << *pInt << endl;
  cout << "*pDouble = " << *pDouble << endl;
  cout << "sizeof(pInt) = " << sizeof(pInt) << endl;
  cout << "sizeof(pDouble) = " << sizeof(pDouble) << endl;

  // Release memory.
  // Only neccessary to set pointer to null after deletion if it's possible that it could be used after releasing its memory. At the end of a function it is not neccessary because it would go out of scope.
  delete pInt;
  pInt = nullptr; 
  delete pDouble;
  pDouble = nullptr;
  
  return 0;
}
