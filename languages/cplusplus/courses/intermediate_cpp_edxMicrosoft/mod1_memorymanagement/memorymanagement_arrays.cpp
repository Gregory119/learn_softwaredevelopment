#include <iostream>

using namespace std;

int main(int argc, char* argv[])
{
  int size = 10;
  int* pInt = new int[size];
  *pInt = 5;
  // Initialze all the elements.
  for (int i=0; i<size; ++i)
    {
      pInt[i] = 0;
    }
  pInt[0] = 3;
  pInt[1] = 5;

  cout << "pInt = " << pInt << endl;
  cout << "*pInt = " << *pInt << endl;

  for (int i=0; i<size; ++i)
    {
      cout << "pInt[" << i << "] = " << pInt[i] << endl;
    }
  
  cout << "sizeof(pInt) = " << sizeof(pInt) << endl;

  // Release memory.
  // Only neccessary to set pointer to null after deletion if it's possible that it could be used after releasing its memory. At the end of a function it is not neccessary because it would go out of scope.
  delete[] pInt; // Note the using of "[]" for arrays.
  pInt = nullptr; 
  
  return 0;
}
