#include <string>
#include <vector>
#include <iostream>
#include <memory>

struct SomeNode
{
  // could easily be used to mix up x and y values, eg. SomeNode(5,6);
  SomeNode(unsigned y, unsigned x)
    : d_x(x),
      d_y(y)
  {
    // Assert if values are out of bounds
    // Calling an owner callback function here is calling a virtual function of the owner (the owner may not be fully constructed => results in using uninitialized variables and unwanted behaviour). To resolve this, the owner callback can be called on a zero timer. No other initialized variables in this constructor should call the error callback of this struct/class (theoretical) from their constructors, because the same problem will happen.
    // Another option for handling an error here is throwing an exception, which must be handled by the calling code. The user of the calling code can easily not do this.
    // The final option for handling an error here is not handle it here, and rather have an init/start function that can return a bool or call an error callback directly. It will be obvious to the library user in testing that the functionality will not start without calling this function, but it must be described. There is still a risk of it not being called after the construction.
  }

  

  // Asserting in the function does not allow the user of the library to act on the failure in other ways. eg. decide to display a pop up error message. This can be overcome with a callback function to the class owner, but this requires an interface that the owner must inherit. This interface is more suitable for a complex class with many functions and possible errors, which avoids having to deal with a direct bool failure for every call of a function that could fail.
  // The other option is providing the ability to set an error callback function using a struct function. If an error occurs, the struct must internally check that the callback has been set before calling it, and assert if it has not been set, which is not failsafe. To reduce this risk, every constructor must be used to initialize the callback function, but a constructor could be missed by mistake.
  // For a simple structure, returning a bool is simple, but creates more error handling code for the user as he/she needs to handle the bool for every call. It's possible to enforce a check of the bool at compile time (Cathexis used a macro that I do not know the contents of). 
  // This is an init function that is descriptive about the values it sets, although some set functions might only set one variable, which increases the risk of not setting a value. In addition SetXY does not specify which value is incorrect on failure, but a single varialbe set function does.
  bool SetXY(unsigned x, unsigned y) { d_x = x; d_y = y; return true; }  
  
  unsigned d_x=0;
  unsigned d_y=0;
};

class BaseClass final
{
public:
  enum class Error
    {
      A,
      B,
      C
    };
  
  class Owner
  {
    // Special members
  public:
    // The owner interface allows copying (no member variables to worry about)
    Owner(const Owner&) = default; // copy constructor
    Owner& operator=(const Owner&) = default; // copy operator
    
    Owner(Owner&&) = default; // move constructor
    Owner& operator=(Owner&&) = default; // move operator

  protected:
    Owner() = default; // needed because other constructors have been declared
    virtual ~Owner() = default; // => should define copy constructor and assignment operator (to handle memory) and define move constructor and operator (they are not declared if a destructor is present)

  public:
    // Interface functions
    virtual void handleError(Error e) = 0;
    virtual void handleMsg(const std::string& msg) = 0;
  };
  
public:
  // talk about constructors, assigments, moving, other operators
  BaseClass(Owner* o,
            std::vector<SomeNode> nodes)
    : d_owner(o),
      d_nodes(std::move(nodes))
  {}
  // no destructor (because final class) => defaulted copy constructor and assignment operator, and defaulted move constructor and move operator

  void clearOwner() { d_owner = nullptr; } // Used by owner in callback before deleting on a timeout. The owner does not receive any other callbacks from this class after calling this function.
  
  // mutable members, const functions
  void constFunc(int a) const
  {
    std::cout << "a = " << a << std::endl;
    d_is_happy = false; // mutable variables can be modified in const functions
  }

private:
  Owner* d_owner = nullptr;
  std::vector<SomeNode> d_nodes;
  mutable bool d_is_happy = true;
};

//----------------------------------------------------------------------//
class Saucey final : BaseClass::Owner
{
  // Because this class is final, it cannot be inherited, so a virtual destructor is not needed.
public:
  void print()
  {
    std::cout << "Randomly print." << std::endl;
  }

private:
  void handleError(BaseClass::Error e) override
  {
    std::cout << "handleError" << std::endl;
    d_base_class.clearOwner(); // disable callbacks from this member
    // call a delete timer with d_base_class
  }
  
  void handleMsg(const std::string& msg) override
  {
    std::cout << "handleMsg" << std::endl;
  }

private:
  std::unique_ptr<BaseClass> d_base_class = std::make_unique<BaseClass>(static_cast<BaseClass::Owner*>(this),std::vector<SomeNode>({SomeNode(5,7)}));
};

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  std::unique_ptr<Saucey> saucey = std::make_unique<Saucey>();
  saucey->print();
  
  return 0;
}
