#include <iostream>

using namespace std;

class Base
{
public:
  // This is an explicit and an implicit converter from an int to a Base. To avoid implicit conversions, use the explicit keyword before the name of the constructor.
  Base(int num = 2, int num_again = 0)
    : d_num(num),
      d_num_again(num_again)
  {
    cout << "Base(int num, int num_again)" << endl;
  }

  // Implicit conversion operator from Base to int
  operator int() const
  {
    cout << "Base::operator int() const" << endl;
    return d_num;
  } 
  
  // This function handles Base*int, but not int*Base (called operator first looks at the left operand when performing the operator function overloading)
  Base& operator*(int rhs)
  {
    cout << "Base::operator*(int rhs)" << endl;
    d_num *= rhs;
    return *this;
  }

private:
  int d_num = 5;
  int d_num_again = 5;

  //friend Base& operator*(Base&,int); This function is not necessary because the member function does the same thing; it would be ambiguous.
  
  friend Base& operator*(int,Base&); // This handles what the member function cannot.
};

/*
//----------------------------------------------------------------------//
Base& operator*(Base& lhs, int rhs)
{
  cout << "Base& operator*(Base& lhs, int rhs)" << endl;
  lhs.d_num *=rhs;
  return lhs;
}
*/
 
//----------------------------------------------------------------------//
Base& operator*(int lhs, Base& rhs)
{
  cout << "Base& operator*(int lhs, Base& rhs)" << endl;
  rhs.d_num *= lhs;
  return rhs;
}

//----------------------------------------------------------------------//
int main(int argc, char ** argv)
{
  int a = 6;
  double c = 7.1;
  Base base;

  cout << "----------------------------------------------------------------------" << endl;
  cout << a*c << endl; // assuming it calls operator*(int, double) => promotes result to double
  cout << c*a << endl; // assuming it calls operator*(double, int) => promotes result to double

  cout << "----------------------------------------------------------------------" << endl;
  Base b;
  b = base*5; // calls Base& Base::operator*(int rhs)
  cout << "----------------------------------------------------------------------" << endl;
  b = 3*base; // calls Base& operator*(int lhs, Base& rhs), but if this operator function did not exist, then the implicit conversion function Base::operator int() const would be called.
  cout << "----------------------------------------------------------------------" << endl;
  b = 3; 
  int d = b; // calls the implicit conversion function Base::operator int() const.

  // To maintain one way conversion, from int to Base, the following function can be defined instead:
  // Base& operator*(Base& lhs, const Base& rhs)
  // One way conversion makes understanding the conversion process more clear.
  
  return 0;
}
