#include <iostream>
#include <memory>

// CHECK OUT THE "class_conversions.cpp" FILE FOR IMPLICIT CLASS OBJECT CASTING.

using namespace std;

//----------------------------------------------------------------------//
class Car
{
public:
  virtual ~Car() = default;
};

//----------------------------------------------------------------------//
class CheapCar final : public Car
{
public:
  CheapCar() = default;
  explicit CheapCar(int num)
    : d_num(num)
  {}

private:
  int d_num = 0;
};

//----------------------------------------------------------------------//
class SportsCar final : public Car
{
public:
  SportsCar() = default;
  SportsCar(const CheapCar&) {} // Constructor & implicit conversion function from a CheapCar
};

//----------------------------------------------------------------------//
class Animal
{
public:
  virtual ~Animal() = default;
};

//----------------------------------------------------------------------//
class Dog final : Animal
{
  
};

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  // implicit static cast
  char num = 5;
  int a = num; // implicit conversion to a larger (int is larger than char), similar type (int and char are similar) => integral promotion
  cout << a << endl;

  int num2 = 50000000;
  //char b = num2; // implicit conversion to a smaller, similar type => possible loss of data => compiler warns, if using the -Wconversions flag, otherwise it is allowed.
  //cout << num2 << endl << b << endl;

  //----------------------------------------------------------------------//
  // EXPLICIT CASTING
  //----------------------------------------------------------------------//
  // STATIC CAST
  // - Cast from one object to another (calls the appropriate conversion function, which may be user defined) TYPICAL USAGE (incl. enum classes, if necessary)
  // - Cast a pointer/reference of one type to another with a compile time check
  // - Does not check downcast validity for base->derived conversion for a polymorphic, ambiguous, or inaccessible base class. If the base class is none of the latter, then it is well formed. Down casting is shows bad design anyway.
  
  char c = static_cast<char>(num2); // no warning with -Wconversion flag because it is explicit
  CheapCar cheap_car = CheapCar(3); // calls the explicit conversion function
  CheapCar cheap_car_1 = static_cast<CheapCar>(3); // calls the explicit conversion function
  SportsCar sports_car;
  //sports_car = cheap_car; // requires an equals operator like SportsCar& SportsCar::operator=(const CheapCar&)
  SportsCar sports_car2 = cheap_car; // calls conversion constructor
  // SportsCar sports_car3 = 5; // Its possible to convert a 5 into a CheapCar and then into a SportsCar, but the compiler does not detect this possibility because it requires two conversions.

  //Dog dog = static_cast<Dog>(sports_car); // Explict conversion will not work because there is no defined conversion function.
  //Dog dog1 = sports_car; // Implicit conversion will not work because there is no defined conversion function (cannot get a Dog from a SportsCar or create a Dog from a SportsCar).

  Car* car_ptr = &sports_car; // implicit static cast
  //Animal* animal_ptr = &sports_car; // implicit static cast fails at compile time because it is known at compile time that sports_car is a SportsCar
  //Animal* animal_ptr2 = static_cast<Animal*>(&sports_car); // Explicit static cast fails at compile time for the same reason.
  // SportsCar* sports_car_ptr = car_ptr; // Implicit down cast fails at compile
  CheapCar* sports_car_ptr2 = static_cast<CheapCar*>(car_ptr); // Explicit down cast works without checking validity (PROBLEM)

  void* void_ptr = &sports_car; // implicit cast to void* works
  //Animal* animal_ptr3 = void_ptr; // fails to compile
  Animal* animal_ptr4 = static_cast<Animal*>(void_ptr); // works (THIS IS A PROBLEM BECAUSE void_ptr DOES NOT POINT TO AN Animal)

  int i = float(5.5); // warning with -Wconversion
  int i1 = static_cast<int>(float(5.5)); // works

  //----------------------------------------------------------------------//
  // DYNAMIC CAST
  // - Mainly used for checking the validity of a down cast from a polymorphic base class at runtime.
  CheapCar* conv = dynamic_cast<CheapCar*>(car_ptr);
  // conv is equal to nullptr on failure, otherwise equal to the cast pointer
  
  //----------------------------------------------------------------------//
  // CONST CAST
  // - remove the constness away from a variable
  // - used on the same type
  const char* word = "hello";
  char* w = const_cast<char*>(word);
  
  //----------------------------------------------------------------------//
  // REINTERPRET CAST
  // - the most powerful => dangerous
  // - casts between any two types
  // - the bits (memory content) of the variable being cast will be reinterpreted as the new specified type.
  int p = 555555;
  CheapCar* dont_do = reinterpret_cast<CheapCar*>(p); // p is now considered to be a pointer to a CheapCar
  
  return 0;
}
