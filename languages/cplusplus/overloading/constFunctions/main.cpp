#include <iostream>
#include <map>

template <class Key,
	  class T>
class Rabbit
{
public:
  T& operator[](const Key& key)
  {
    return m_numToName[key];
  }
  
  auto find(const Key& key)
  {
    std::cout << "find(const Key& key)" << std::endl;
    return m_numToName.find(key);
  }

  auto find(const Key& key) const
  {
    std::cout << "find(const Key& key) const" << std::endl;
    return m_numToName.find(key);
  }
  
private:
  std::map<Key,T> m_numToName;
};

//----------------------------------------------------------------------//
int main()
{
  Rabbit<int,std::string> rabbitMap;
  rabbitMap[0] = "hello";
  rabbitMap[1] = "big";

  const auto it = rabbitMap.find(1);
  std::cout << "found: " << it->second << std::endl;

  // We need to make a const object to call the const version of the function.
  const Rabbit<int,std::string> rabbitMap2 = rabbitMap;
  const auto it2 = rabbitMap2.find(1);
  std::cout << "found: " << it2->second << std::endl;
  
  return 0;
}
