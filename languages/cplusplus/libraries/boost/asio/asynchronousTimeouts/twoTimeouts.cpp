#include <boost/asio.hpp>

//----------------------------------------------------------------------//
int main (int argc, char** argv)
{
  boost::asio::io_service ioService;
  
  boost::asio::deadline_timer timer1 ( ioService,
				       boost::posix_time::milliseconds
				       ( 1000 ));

  timer1.async_wait ( [&](const boost::system::error_code& error){
      // This will be called when either the timer has expired, or the timer was cancelled, in which case the handler is passed the error code boost::asio::error::operation_aborted.
      if (error != boost::asio::error::operation_aborted)
	{
	  std::cout << "Timer 1 timed out" << std::endl;
	  //ioService.stop();
	}
    } );

  boost::asio::deadline_timer timer2 ( ioService,
				       boost::posix_time::milliseconds
				       ( 1000 ));

  timer2.async_wait ( [&](const boost::system::error_code& error){
      if (error != boost::asio::error::operation_aborted)
	{
	  std::cout << "Timer 2 timed out" << std::endl;
	  //ioService.stop();
	}
    } );
  
  // Run the io service to asynchronously process the write and timeout. This requires one of the handlers stopping the service.
  //ioService.run();

  // OR, Do not stop the service in the handlers and just allow the first dispatched handler to run. There can still be other queue handlers that have not been dispatched.
  ioService.run_one();

  // Stop the timer from being processed by the io_service.
  //timer1.cancel();

  //ioService.reset();
  ioService.poll(); // Execute ready handlers only, in case there were existing asynchronous operations to be performed before calling this code.
    
  return EXIT_SUCCESS;
}
