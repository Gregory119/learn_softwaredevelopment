#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 3)
    {
      std::cerr << "Usage: client <host name/ip> <host service/port>" << std::endl;
      return 1;
    }

    boost::asio::io_service io_service;

    // Create a list of potential endpoints with the host and service (accommodates IPv4 and IPv6).
    tcp::resolver resolver(io_service);
    std::string hostNameOrIp { argv[1] };
    std::string hostServiceOrPort { argv[2] };
    tcp::resolver::query query(hostNameOrIp, hostServiceOrPort);
    tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

    tcp::socket socket(io_service);
    // Try each endpoint in a list, synchronously, until the socket is successfully connected.
    boost::asio::connect(socket, endpoint_iterator);
     
    // Now connected, so read some data.
    for (;;)
    {
      boost::array<char, 128> buf;
      boost::system::error_code error;

      size_t len = socket.read_some(boost::asio::buffer(buf), error);

      if (error == boost::asio::error::eof)
        break; // Connection closed cleanly by peer.
      else if (error)
        throw boost::system::system_error(error); // Some other error.

      std::cout.write(buf.data(), len);
    }
  }
  catch (std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }

  return 0;
}
