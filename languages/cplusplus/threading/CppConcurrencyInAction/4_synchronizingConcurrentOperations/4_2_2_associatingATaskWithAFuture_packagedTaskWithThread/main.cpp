#include <future>
#include <iostream>
#include <thread>

/*
  This program shows how to create similar functionality
  of using std::async with a function by using a std::thread and a
  std::packaged_task.

  The advantage of using std::package_task is not apparent for this
  case because it requires more code, however the advantage is more
  clear when passing multiple different task implementations to a
  thread via a queue.
 */

//----------------------------------------------------------------------//
double Divide(int a, int b)
{
  std::cout << "Dividing.." << std::endl;
  return static_cast<double>(a)/b;
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  int a {0};
  int b {0};
  std::cout << "Enter an integer numerator: ";
  std::cin >> a;
  std::cout << "Enter an integer denominator: ";
  std::cin >> b;
  
  //----------------------------------------------------------------------//
  // Using std::async
  //----------------------------------------------------------------------//

  std::cout << "//----------------------------------------------------------------------//\n"
	    << "// Using std::async\n"
	    << "//----------------------------------------------------------------------//"
	    << std::endl;
  
  auto futureRes = std::async(Divide,a,b);
  
  std::this_thread::sleep_for(std::chrono::seconds(1));
  std::cout << "Result is = " << futureRes.get() << std::endl;

  //----------------------------------------------------------------------//
  // Using std::thread and std::packaged_task
  //----------------------------------------------------------------------//
  std::cout << "//----------------------------------------------------------------------//\n"
	    << "// Using std::thread and std::packaged_task\n"
	    << "//----------------------------------------------------------------------//"
	    << std::endl;

  std::packaged_task<double(int,int)> task(Divide);

  // std::packaged_task is not copy constructable, so pass as a reference to std::function.
  std::function<void(int,int)> wrapTask {std::ref(task)}; 
  std::future<double> futureRes2 = task.get_future();

  // This thread will make a copy of the std::function passed in, but this std::function holds a reference to the packaged task, so a copy of this reference is made.
  std::thread t(wrapTask,a,b);

  std::this_thread::sleep_for(std::chrono::seconds(1));
  std::cout << "Result is = " << futureRes2.get() << std::endl;
  t.join();
  
  return EXIT_SUCCESS;
}
