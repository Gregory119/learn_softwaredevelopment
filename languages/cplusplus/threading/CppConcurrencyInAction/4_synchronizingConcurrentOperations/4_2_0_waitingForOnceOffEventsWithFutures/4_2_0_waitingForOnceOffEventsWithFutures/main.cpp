#include <iostream>
#include <future>

//----------------------------------------------------------------------//
/* This thread function should be run with std::async, which will catch the possible exception and return it in a std::future.

   The std::future is used as an rvalue to take ownership of it, in order to avoid the possibility of other threads accessing it, causing synchronization issues. Otherwise, accessing the future would need to be synchronized.
 */
int threadFunc(std::future<void>&& shutdown)
{
  if (!shutdown.valid())
    {
      throw std::future_error(std::future_errc::no_state);
    }
  
  // Continue looping until the shutdown signal has been sent.
  while (shutdown.wait_for(std::chrono::milliseconds(0)) !=
      std::future_status::ready)
    {
      std::cout << "Thread (" << std::this_thread::get_id() << ") processing..." << std::endl;
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
  
  return EXIT_SUCCESS;
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  //----------------------------------------------------------------------//
  // Test std::promise and std::future.
  //----------------------------------------------------------------------//
  std::promise<void> promiseShutdown;

  std::future<int> res =
    std::async(threadFunc,
	       promiseShutdown.get_future());

  std::this_thread::sleep_for(std::chrono::seconds(1));
  promiseShutdown.set_value(); // Shutdown the thread function.

  // If the future does not refer to a shared state, then a call of get() will be undefined.
  if (!res.valid())
    {
      std::cout << "Future does not refer to valid state. Exiting." << std::endl;
      return EXIT_FAILURE;
    }

  try
    {
      int exitVal = res.get(); // Wait for the returned value of the thread function.
      std::cout << "Exit value is: " << exitVal << std::endl;
    }
  catch (const std::exception& e)
    {
      std::cout << "Caught exception: " << e.what() << std::endl;
    }

  //----------------------------------------------------------------------//
  // Make the thread function throw the future error exception.
  //----------------------------------------------------------------------//

  /* 
     Pass a default constructed future that does not refer to a shared value.
     The thread function will detect this with the call to valid() on the future
     passed in and then throw. This new exception will then be rethrown on the call the get()
     below.
   */
  std::future<int> res2 = std::async(threadFunc,
				     std::future<void>());
  
  try
    {
      int exitVal = res2.get();
      std::cout << "Exit value is: " << exitVal << std::endl;
    }
  catch (const std::exception& e)
    {
      std::cout << "Caught exception: " << e.what() << std::endl;
    }
  
  return EXIT_SUCCESS;
}
