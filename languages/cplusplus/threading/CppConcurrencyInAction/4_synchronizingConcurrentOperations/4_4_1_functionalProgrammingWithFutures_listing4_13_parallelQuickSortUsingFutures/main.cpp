#include <iostream>

#include <algorithm>
#include <future>
#include <list>

//----------------------------------------------------------------------//
// This implementation is not the most efficient. See the text for
// details.
template <typename T>
std::list<T> ParallelQuickSort(std::list<T> input)
{
  if (input.empty())
    {
      return input;
    }

  std::list<T> result;

  // Transfer the pivot value to the result. It will need to go there
  // eventually. Splicing moves the node pointer from the input list
  // to the result list.
  result.splice(result.end(),input,input.begin());
  T const& pivot = *result.begin();

  // Partition the values based on the pivot; the values are not
  // sorted. The returned value is the an iterator to the first value
  // in the partition containing values larger than the pivot.
  auto dividePoint = std::partition(input.begin(),
				    input.end(),
				    [&](T const& val){
				      return val < pivot;
				    });

  // Splice the current lower part from the input.
  std::list<T> lowerPart;
  lowerPart.splice(lowerPart.end(),
		   input,
		   input.begin(),
		   dividePoint);

  // Generate a possibly recursive asynchronous sort operation. Each
  // async call could generate a thread or synchronously call the
  // ParallelQuickSort function.
  std::future<std::list<T>> newLower {
    std::async(&ParallelQuickSort<T>, std::move(lowerPart)) };

  // Sort the higher part on the current thread so that the current
  // thread has something to do.
  auto newHigher { ParallelQuickSort(std::move(input)) };
  result.splice(result.end(), newHigher);
  
  // Use the get() function on the future to get the result as an rvalue.
  result.splice(result.begin(), newLower.get());
  
  return result;
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  std::list<int> vals = {5,7,3,5,4,51,2,28,3,1,34,8,7,99,11};
  for (const auto& e : vals)
    {
      std::cout << ", " << e ;
    }
  std::cout << std::endl;

  std::list<int> sorted = ParallelQuickSort(std::move(vals));
  for (const auto& e : sorted)
    {
      std::cout << ", " << e ;
    }
  std::cout << std::endl;
  
  return EXIT_SUCCESS;
}
