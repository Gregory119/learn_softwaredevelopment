#include <thread>
#include <future>
#include <boost/thread/scoped_thread.hpp>

/*
  This program shows how to create a class that takes advantage of the capabilities of std::packaged_task. Packaged tasks are used to wrap different function implementations that are run by an internal thread by transferring the tasks with a synchronized queue.
 */

std::mutex m;

//----------------------------------------------------------------------//
void print(const std::string& msg)
{
  std::lock_guard<std::mutex> lg(m);
  std::cout << msg << std::endl;
}

//----------------------------------------------------------------------//
/* Using thread safe queue from listing 4.5
   Update this to use the implementation that I wrote!!!
 */
//----------------------------------------------------------------------//
#include <queue>
#include <memory>
#include <mutex>
#include <condition_variable>

template<typename T>
class ThreadSafeQueue final
{
public:
  //----------------------------------------------------------------------//
  ThreadSafeQueue() = default;

  //----------------------------------------------------------------------//
  ~ThreadSafeQueue()
  {
    print("~ThreadSafeQueue");
  }

  //----------------------------------------------------------------------//
  ThreadSafeQueue(ThreadSafeQueue const& rhs)
  {
    std::lock_guard<std::mutex> lk(rhs.m_mut);
    m_dataQueue=rhs.m_dataQueue;
  }

  //----------------------------------------------------------------------//
  // ADDED
  ThreadSafeQueue operator=(const ThreadSafeQueue& rhs)
  {
    std::unique_lock<std::mutex> lkLhs(m_mut,std::defer_lock);
    std::unique_lock<std::mutex> lkRhs(rhs.m_mut,std::defer_lock);
    std::lock(lkLhs,lkRhs);
    m_dataQueue=rhs.m_dataQueue;
  }

  //----------------------------------------------------------------------//
  // ADDED
  ThreadSafeQueue(ThreadSafeQueue&& rhs)
  {
    std::lock_guard<std::mutex> lk(rhs.m_mut);
    m_dataQueue=std::move(rhs.m_dataQueue);
  }

  //----------------------------------------------------------------------//
  // ADDED
  ThreadSafeQueue& operator=(ThreadSafeQueue&& rhs)
  {
    std::unique_lock<std::mutex> lkLhs(m_mut,std::defer_lock);
    std::unique_lock<std::mutex> lkRhs(rhs.m_mut,std::defer_lock);
    std::lock(lkLhs,lkRhs);
    m_dataQueue=std::move(rhs.m_dataQueue);
  }
  
  //----------------------------------------------------------------------//
  void push(const T& new_value)
  {
    std::lock_guard<std::mutex> lk(m_mut);
    m_dataQueue.push(new_value);
    m_cv.notify_one();
  }

  //----------------------------------------------------------------------//
  // ADDED
  void push(T&& new_value)
  {
    std::lock_guard<std::mutex> lk(m_mut);
    m_dataQueue.push(std::move(new_value));
    m_cv.notify_one();
  }

  //----------------------------------------------------------------------//
  bool waitAndPop(T& value)
  {
    std::unique_lock<std::mutex> lk(m_mut);
    m_cv.wait(lk,[this]{return !m_dataQueue.empty() || m_stopWait;});

    // A request has been made to stop waiting, so don't continue processing.
    if (m_stopWait)
      {
	return false;
      }
    
    value=m_dataQueue.front();
    m_dataQueue.pop();
    return true;
  }

  //----------------------------------------------------------------------//
  // ADDED
  // Always check the validity.
  bool waitAndPopMove(T& value)
  {
    std::unique_lock<std::mutex> lk(m_mut);
    m_cv.wait(lk,[this]{return !m_dataQueue.empty() || m_stopWait;});

    // A request has been made to stop waiting, so don't continue processing.
    if (m_stopWait)
      {
	return false;
      }
    
    value=std::move(m_dataQueue.front());
    m_dataQueue.pop();
    return true;
  }

  //----------------------------------------------------------------------//
  // Always check the validity of the returned value, because it could be empty/null.
  std::shared_ptr<T> waitAndPop()
  {
    std::unique_lock<std::mutex> lk(m_mut);
    m_cv.wait(lk,[this]{return !m_dataQueue.empty() || m_stopWait;});

    // A request has been made to stop waiting, so don't continue processing.
    if (m_stopWait)
      {
	return std::shared_ptr<T>{};
      }
    
    std::shared_ptr<T> res {new T {m_dataQueue.front()}};
    m_dataQueue.pop();
    return res;
  }

  //----------------------------------------------------------------------//
  bool tryPop(T& value)
  {
    std::lock_guard<std::mutex> lk(m_mut);
    if(m_dataQueue.empty())
      return false;
    
    value=m_dataQueue.front();
    m_dataQueue.pop();
    return true;
  }

  //----------------------------------------------------------------------//
  std::shared_ptr<T> tryPop()
  {
    std::lock_guard<std::mutex> lk(m_mut);
    if(m_dataQueue.empty())
      return std::shared_ptr<T>();

    std::shared_ptr<T> res {new T {m_dataQueue.front()}};
    m_dataQueue.pop();
    return res;
  }

  //----------------------------------------------------------------------//
  bool empty() const
  {
    std::lock_guard<std::mutex> lk(m_mut);
    return m_dataQueue.empty();
  }

  //----------------------------------------------------------------------//
  // BE CAREFUL WITH THIS
  // The wait calls are notified and their functions return immediately.
  void stopWaitingAndReturn()
  {
    {
      std::lock_guard<std::mutex> lk(m_mut);
      m_stopWait = true;
    }
    
    // Do not lock here because the woken up thread will then have to wait.
    m_cv.notify_all();
  }

private:
  mutable std::mutex m_mut;
  std::queue<T> m_dataQueue;
  std::condition_variable m_cv;
  bool m_stopWait = false;
};

//----------------------------------------------------------------------//
// Scoped thread from listing 2.6
//----------------------------------------------------------------------//

class ScopedThread
{
public:
  ScopedThread(std::thread&& t)
    : m_thread(std::move(t))
  {
    if (!m_thread.joinable())
      {
	throw std::logic_error("No thread.");
      }
  }

  ~ScopedThread()
  {
    print("~ScopedThread");
    m_thread.join();
  }

  // Not copyable, as before
  ScopedThread(const ScopedThread&) = delete;
  ScopedThread& operator=(const ScopedThread&) = delete;

  // Added these in myself. The internal thread move constructor/operator will be called by one of these.
  ScopedThread(ScopedThread&&) = default;
  ScopedThread& operator=(ScopedThread&&) = default;
  
private:
  std::thread m_thread;
};

//----------------------------------------------------------------------//
// Implementation using std::packaged_task
//----------------------------------------------------------------------//

/*
  ThreadLoop is used to synchronize calls between checking a shutdown signal of a loop and calling data structures that wait on condition variables. This could be extended further to be used with a scoped thread or a detached thread.
 */
class ThreadLoop final
{
public:
  //----------------------------------------------------------------------//
  ThreadLoop(std::function<void()> loopFunc)
    : m_loopFunc(std::move(loopFunc))
  {
    if (!m_loopFunc)
      {
	throw std::logic_error {"Function is not valid."};
      }

    m_futShutdown = m_promShutdown.get_future();
  }

  //----------------------------------------------------------------------//
  // This function should notify all possible condition variables that could be waiting in the supplied loop function (provided in the constructor of this class).
  void setNotifyFunc(std::function<void()> func)
  {
    m_notifyFunc = std::move(func);
  }

  //----------------------------------------------------------------------//
  ~ThreadLoop()
  {
    print("~ThreadLoop");
    
    m_promShutdown.set_value();
    print("ThreadLoop promise just set.");

    // The shutdown signal has been sent so if the loop is not exiting, it's because it's waiting. Continue to try stop it from waiting until it wakes up and checks the shutdown signal (std::future) to exit the loop.
    while (!m_loopExited)
      {
	m_notifyFunc();
      }
    print("Detected loop exit.");
  }
  
  //----------------------------------------------------------------------//
  void operator() ()
  {
    while (true)
      {
	print("ThreadLoop checking future.");

	if (!m_futShutdown.valid())
	  {
	    print("Future state not valid!");
	  }
	
	if (m_futShutdown.wait_for(std::chrono::milliseconds(0)) ==
	    std::future_status::ready)
	  {
	    print("ThreadLoop stopping.");
	    m_loopExited = true;
	    return;
	  }

	m_loopFunc();
      }
  }
  
private:
  std::promise<void> m_promShutdown;
  std::future<void> m_futShutdown;
  std::function<void()> m_loopFunc;
  std::function<void()> m_notifyFunc;
  bool m_loopExited = false;
};

class ThreadTasker final
{
public:
  ThreadTasker()
    : m_taskQueue(new ThreadSafeQueue<std::packaged_task<void()>>{})
  {}

  // Copying won't work anyway because the unique pointer members are
  // not copyable.
  ThreadTasker(const ThreadTasker&) = default;
  ThreadTasker& operator=(const ThreadTasker&) = default;

  ThreadTasker(ThreadTasker&&) = default;
  ThreadTasker& operator=(ThreadTasker&&) = default;

  //----------------------------------------------------------------------//
  std::future<void> postTask(std::function<void()> f)
  {
    // Synchronized lazy initialization of thread to process tasks.
    std::call_once(m_isRunning,
		   &ThreadTasker::initThread,
		   this,
		   f);

    std::packaged_task<void()> task {f};
    std::future<void> res = task.get_future();
    m_taskQueue->push(std::move(task));
    return res;
  }

private:
  //----------------------------------------------------------------------//
  void initThread(std::function<void()>& f)
  {
    d_threadLoop.reset(new ThreadLoop {[&](){
	  std::packaged_task<void()> task;
	  if (!m_taskQueue->waitAndPopMove(task))
	    {
	      return;
	    }

	  task();
	}});

    d_threadLoop->setNotifyFunc([&](){
	m_taskQueue->stopWaitingAndReturn();
      });

    std::thread t(std::ref(*d_threadLoop));
    //#define SCOPED // See note in private member section.
#ifdef SCOPED
    d_scopedThread.reset(new ScopedThread(std::move(t)));
#else
    t.detach();
#endif
  }
      
private:
  std::unique_ptr<ThreadSafeQueue<std::packaged_task<void()>>> m_taskQueue;

  /*
    The order of destruction of these two members is important. 
    The ThreadLoop should first be destroyed so that the ScopedThread will not wait forever at the call of join() on the thread in the destructor.

    In order to avoid this desctruction order being switched accidentally, these two members should be combined in a separate class.

    The alternative is to detach the thread, instead of using it with a scopedThread.
   */
  std::unique_ptr<ScopedThread> d_scopedThread;
  std::unique_ptr<ThreadLoop> d_threadLoop;

  std::once_flag m_isRunning;
};

//----------------------------------------------------------------------//
std::function<void()> func(int num)
{
  return std::function<void()>{[=](){
      print("Task "+std::to_string(num));
    }};
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  ThreadTasker tasker;

  std::queue<std::future<void>> taskResults;

  std::mutex m;
  auto pushTasks = [&](){
    static int taskCount = 0;
    for (int i=0; i<5; ++i)
      {
	std::unique_lock<std::mutex> ul(m);
	std::future<void> result = tasker.postTask(func(taskCount));
	++taskCount;
	ul.unlock();
	
	taskResults.push(std::move(result));
      }
  };

  // Notice that multiple threads are posting tasks to one thread at the same time (the ThreadTasker uses one thread).
  std::vector<std::thread> threads;
  for (unsigned i=0; i<5; ++i)
    {
      threads.push_back(std::thread(pushTasks));
    }

  // Give some time for the threads to startup and run.
  std::this_thread::sleep_for(std::chrono::seconds(1));
  print("Pushed all tasks.");

  int count = 0;
  while (!taskResults.empty())
    {
      print("Waiting on task "+std::to_string(count));
      taskResults.front().get();
      taskResults.pop();
      ++count;
    }

  print("Joining threads.");

  for (std::thread& t : threads)
    {
      t.join();
    }
  
  print("Done.");
  
  return EXIT_SUCCESS;
}
