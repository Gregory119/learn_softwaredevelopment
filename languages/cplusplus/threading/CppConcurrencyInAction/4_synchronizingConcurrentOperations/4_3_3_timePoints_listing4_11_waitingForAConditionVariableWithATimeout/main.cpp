#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>

std::mutex m;
std::condition_variable cv;
bool done = false;

//----------------------------------------------------------------------//
bool waitUntilLoop(const std::chrono::milliseconds& waitTime)
{
  const auto timePoint = std::chrono::steady_clock::now() + waitTime;
  
  std::unique_lock<std::mutex> ul{m};
  
  while (!done)
    {
      /*
	This wait_until() call does not use a predicate to check if
	the condition variable should continue waiting or not. In
	addition to handling the case of being incorrectly notified to
	wake up with an explicit call of notify() or notify_all(),
	predicates with condition variables are useful for ensuring
	that spurious wakes do not cause the wait function to exit
	(before exiting this function, the mutex will be locked).
	
	Without a predicate, a possible spurious wake will cause the
	call on the wait() function to exit (before exiting this
	function, the mutex will be locked) before the timeout has
	been reached. Or the same could happen if a notify() has been
	called on the condition variable in some other thread.

	A loop is needed here to handle the reasons for wakeup above
	because this waitUntilLoop function is meant to wait until the
	timeout is reached.

	Using an absolute timeout here maintains an absolute time
	threshold on the loop, and avoids the problems described in
	the waitForLoop() function below.
       */
      if (cv.wait_until(ul, timePoint) == std::cv_status::timeout)
	{
	  break;
	}

      std::cout << "waitUntilLoop() woke up" << std::endl;
    }
  std::cout << "waitUntilLoop() exiting" << std::endl;
  return done;
}

//----------------------------------------------------------------------//
bool waitForLoop(const std::chrono::milliseconds& waitTime)
{
  std::unique_lock<std::mutex> ul{m};
  
  while (!done)
    {
      /*
	The problem here is that if this wait is woken up before the timeout by ways mentioned previously, then the loop will loop around and this wait() will be called again using the duration from a newly updated, current time point. Thus, this loop could go on indefinitely.
       */
      if (cv.wait_for(ul, waitTime) == std::cv_status::timeout)
	{
	  break;
	}

      std::cout << "waitForLoop() woke up" << std::endl;
    }
  std::cout << "waitForLoop() exiting" << std::endl;
  return done;
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  // Correct implementation.
  waitUntilLoop(std::chrono::milliseconds(500));

  // Incorrect implementation. Will possibly loop continuously.
  // waitForLoop(std::chrono::milliseconds(500));
  
  return EXIT_SUCCESS;
}
