#include <future>
#include <iostream>
#include <thread>
#include <vector>

/*
  Test std::shared_future with multiple threads. All the created
  threads will do some initial processing before waiting on the future
  value. Eventually all of them wait at the same time. When the future
  is ready, they all continue processing at the same time.
 */

//----------------------------------------------------------------------//
std::mutex m;
void print(const std::string& msg)
{
  std::lock_guard<std::mutex> lg(m);
  std::cout << msg << std::endl;
}

//----------------------------------------------------------------------//
// Ensures that the shared future is a copy by passing it by value.
void printAndWaitToProcess(const std::string& msg,
			   std::shared_future<int> futVal)
{
  print(msg);
  print("Waiting on the shared future.");
  print(std::string("Received the value ") + std::to_string(futVal.get()) + " from the future.");
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  std::vector<std::thread> threads;

  std::promise<int> prom;
  auto fut = prom.get_future().share();
  
  for (unsigned i=0; i<10; ++i)
    {
      // We can pass the shared future by reference because the
      // function running in the thread takes a copy of it as a
      // parameter.
      threads.emplace_back(printAndWaitToProcess,
			   "First message",
			   std::ref(fut));
    }

  // Wait for all the threads to be waiting on the future.
  std::this_thread::sleep_for(std::chrono::seconds(1));

  prom.set_value(5);
  
  for (auto& t : threads)
    {
      t.join();
    }
  
  return EXIT_SUCCESS;
};
