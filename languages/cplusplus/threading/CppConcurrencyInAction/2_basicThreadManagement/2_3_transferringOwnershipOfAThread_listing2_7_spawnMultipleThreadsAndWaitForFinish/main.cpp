#include <algorithm>
#include <functional>
#include <iostream>
#include <thread>
#include <vector>

//----------------------------------------------------------------------//
void DoWork(unsigned val)
{
  std::cout << "Current thread number = " << std::this_thread::get_id() << "\n"
	    << "with a data value of " << val << std::endl;
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  std::vector<std::thread> threadPool;
  for (unsigned i=0; i<10; ++i)
    {
      // Perfect forward the parameters to the std::thread constructor.
      threadPool.emplace_back(DoWork,i);
    }

  std::for_each(threadPool.begin(),
		threadPool.end(),
		std::mem_fn(&std::thread::join));
  
  return 0;
}
