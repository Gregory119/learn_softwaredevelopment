#include <iostream>
#include <thread>

class ScopedThread
{
public:
  /* This class will now take ownership of the thread passed in, so
     the previous lifetime issues are not present. Only this class
     will be able to join the thread, after calling this constructor,
     so the joinable state is checked here.

     I made the constructor take an rvalue of the thread because
     threads are not copyable, so there is no benefit of using a pass
     by value.
  */
  ScopedThread(std::thread&& t)
    : m_thread(std::move(t))
  {
    if (!m_thread.joinable())
      {
	throw std::logic_error("No thread.");
      }
  }

  ~ScopedThread()
  {
    std::cout << "joining thread.." << std::endl;
    m_thread.join();
    std::cout << "thread joined.." << std::endl;
  }

  // Not copyable, as before => not moveable
  ScopedThread(const ScopedThread&) = delete;
  ScopedThread& operator=(const ScopedThread&) = delete;

  // Added these in myself. The internal thread move constructor/operator will be called by one of these. These are actually not necessary because the assignment operator has been declared, the move operators are not declared.
  // ScopedThread(ScopedThread&&) = default;
  // ScopedThread& operator=(ScopedThread&&) = default;
  
private:
  std::thread m_thread;
};

//----------------------------------------------------------------------//
class Func
{
public:
  Func(int num)
    : m_data(num)
  {}

  Func& operator() ()
  {
    std::cout << "m_data = " << m_data << std::endl;
    return *this;
  }
  
private:
  int m_data = 0;
};

//----------------------------------------------------------------------//
void doSomething()
{
  std::cout << "do something" << std::endl;
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  int num = 10;
  Func func(num);

  //std::thread t(func);
  //ScopedThread scopedThread1(std::move(t));

  // or
  ScopedThread scopedThread2(std::thread{func});

  // The next line is similar to the vexing parse, and is seen as a function declaration (doing nothing).
  ScopedThread scopedThread3(std::thread(func));

  doSomething();
}
