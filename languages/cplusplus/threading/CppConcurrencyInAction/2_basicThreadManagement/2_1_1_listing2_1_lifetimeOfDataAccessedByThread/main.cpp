#include <iostream>
#include <thread>

/*
   This bug is hard to find. Allowing a pointer or reference to a local variable to persist past its lifetime is a bug.
*/

struct Func
{
public:
  explicit Func(int& num)
    : m_num(num),
      m_orig(num)
  {
    printState();
  }

  //----------------------------------------------------------------------//
  void operator () () const
  {
    /*
      Potential access to dangling reference, m_num, after function "oops" exits and the local variable in "oops" named "number" is destroyed.
     */
    for (int i=0; i<1000000; ++i) 
      {
	if (m_num != m_orig)
	  {
	    std::cout << "The number suddenly changed!!" << std::endl;
	    printState();
	    return;
	  }
      }
  }

  //----------------------------------------------------------------------//
  void printState() const
  {
    std::cout << "m_orig = " << m_orig << std::endl;
    std::cout << "m_num = " << m_num << std::endl;
    std::cout << std::endl;
  }
  
private:
  int& m_num;
  int m_orig;
};

//----------------------------------------------------------------------//
void oops()
{
  /*
    You could copy the data of number into the thread instead of using a reference, but you still need to be wary of objects containg pointers or references to external variables. Don't create a thread within a function if that has access to the local variables, unless the thread is guaranteed to complete execution before the function exits.
   */

  int number=5;
  Func f(number);

  std::thread t(f);
  std::this_thread::sleep_for(std::chrono::milliseconds(1));
  t.detach(); // Don't wait for thread to finish.
  std::cout << "detached" << std::endl;
} // Thread may still be running.

int main(int argc, char** argv)
{
  oops();
  std::this_thread::sleep_for(std::chrono::seconds(1));
  
  return 0;
}
