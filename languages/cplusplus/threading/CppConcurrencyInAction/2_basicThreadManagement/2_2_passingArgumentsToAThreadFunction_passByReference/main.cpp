#include <iostream>
#include <thread>

struct Data
{
  int val=0;
};

//----------------------------------------------------------------------//
void func(int& data)
{
  data=5;
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  /*
    I tried to pass some variables by value to the function accepting a modifiable reference, but the compiler picks this up as an error that is not clear to me. It would only compile when using "std::ref", which is a good thing actually, because the function expects a reference.

    If I modify the function "func" to use a constant reference then the compiler error does not appear.

    I tried the same thing with the Data struct.
  */
  
  int data = 10;

  std::cout << "Initially string value: " << data << std::endl;
  std::thread t(func,std::ref(data));
  
  t.join();
  std::cout << "Final string value: " << data << std::endl;
  
  return 0;
}
