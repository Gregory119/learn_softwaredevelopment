#include <thread>
#include <iostream>
#include <mutex>
#include <stdexcept>

/*
  Use RAII idiom to join a thread.
 */
class ThreadGuard final
{
public:
  ThreadGuard(std::thread& thread_ref)
    : m_thread_ref(thread_ref)
  {}

  ~ThreadGuard()
  {
    // The thread could have already been joined, so make sure that it is joinable.
    if (m_thread_ref.joinable())
      {
	std::cout << "joining thread in ~ThreadGuard()" << std::endl;
	m_thread_ref.join();
	std::cout << "thread joined in ~ThreadGuard()" << std::endl;
      }
  }
  
  // Disable copying in order to avoid lifetime issues related to storing a reference to an std::thread.
  //For example, you won't be able to pass this to an std::thread constructor, such that it makes a copy, and then lasts longer than the thread that created it (with a detach() call).
  // You could cause this ThreadGuard class to have a longer lifetime than the thread that it holds a reference to by creating a std::unique_ptr<ThreadGuard> within a function, which then returns the unique_pointer instance. The thread that it holds a reference to, created as a local variable in the function, would be destroyed at the end of the function.
  ThreadGuard(const ThreadGuard&) = delete;
  ThreadGuard& operator=(const ThreadGuard&) = delete;

private:
  std::thread& m_thread_ref;
};

//----------------------------------------------------------------------//
class Func final
{
public:
  Func(int val)
    : m_val(val)
  {}
  
  Func& operator() ()
  {
    std::cout << "m_val = " << m_val << std::endl;
    return *this;
  }

private:
  int m_val=0;
};

//----------------------------------------------------------------------//
void someFuncThatCanThrow(bool state)
{
  if (state)
    {
      throw std::invalid_argument("Exception text");
    }
}

//----------------------------------------------------------------------//
void usingThreadGuard()
{
  int val = 10;
  Func func(val);

  std::thread t1(func);
  ThreadGuard guard1(t1);

  // The progam is not terminated if this exception is thrown because of the use of ThreadGuard.
  someFuncThatCanThrow(true);
}

//----------------------------------------------------------------------//
void notUsingThreadGuard()
{
  int val = 10;
  Func func(val);
  std::thread t1(func);

  // This will throw, and then skip the join, which will cause the destructor of std::thread to call std::terminate.
  someFuncThatCanThrow(true);

  t1.join();
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  try
    {
      usingThreadGuard();
    }
  catch (std::invalid_argument e)
    {
      std::cout << "Exception from usingThreadGuard. Message: " << e.what() << std::endl;
    }

  try
    {
      notUsingThreadGuard();
    }
  catch (std::exception& e)
    {
      std::cout << "exception message: " << e.what() << std::endl;
    }
  
  return 0;
}
