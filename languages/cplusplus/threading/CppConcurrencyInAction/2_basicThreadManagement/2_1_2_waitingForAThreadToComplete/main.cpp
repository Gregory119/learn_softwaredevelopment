#include <iostream>
#include <thread>

void threadFunc()
{
  std::cout << "Hello" << std::endl;
}

int main(int argc, char** argv)
{
  std::thread t(threadFunc);
  
  if (t.joinable())
    {
      std::cout << "Thread IS joinable." << std::endl;
    }
  
  t.join();
  /*
    Calling join on a thread cleans up any storage associated with the thread, and now the thread object is not associated with the now-finished thread.
   */

  if (!t.joinable())
    {
      std::cout << "Thread is NOT joinable." << std::endl;
    }

  /*
    Calling join again on the thread will throw an exception of std::system_error.
   */
  //t.join();

  // Create a new thread and move it into the existing thread object.
  t = std::thread(threadFunc);
  std::cout << "Detaching thread." << std::endl;
  t.detach();
  /*
    Calling detach will remove the association of the running-thread from the std::thread object. I add a sleep next, to give time to the detached thread to output it's text.
   */

  std::this_thread::sleep_for(std::chrono::seconds(1));
  
  return 0;
}
