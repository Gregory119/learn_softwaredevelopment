#include <iostream>
#include <thread>

class BackgroundTask
{
public:
  void operator () () const
  {
    // do something
    std::cout << "do something" << std::endl;
  }
};

int main(int argc, char** argv)
{
  /*
    The function object is copied into the storage belonging to the newly created thread and invoked from there. The copy should behave as the original.
   */
  BackgroundTask bt;
  std::thread t1(bt);
  t1.join();

  /*
    Avoid "C++'s most vexing parse" if attempting to pass a temporary type to a thread constructor. The following is a declaration of a function t that returns an std::thread and takes a single parameter (of type pointer to a function taking no parameters and returning a BackgroundTask object).
    
    std::thread t(BackgroundTask());
   */
  std::thread t2((BackgroundTask()));
  // or using uniform initialization syntax:
  std::thread t3{BackgroundTask()};
  t2.join();
  t3.join();

  /*
    Consider using Lambda expressions.
   */
  std::thread t_lambda([](){
      std::cout << "Do something using a lambda expression." << std::endl;
    });
  t_lambda.join();
  
  return 0;
}
