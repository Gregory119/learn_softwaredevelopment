#include <iostream>
#include <thread>

class Car
{
public:
  Car(int num)
    : m_num(num)
  {}
  
  void run()
  {
    std::cout << "Car::run with m_num=" << m_num << std::endl;
  }

private:
  int m_num=0;
};

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  Car car(6);

  std::thread t(&Car::run,&car);
  t.join();
}
