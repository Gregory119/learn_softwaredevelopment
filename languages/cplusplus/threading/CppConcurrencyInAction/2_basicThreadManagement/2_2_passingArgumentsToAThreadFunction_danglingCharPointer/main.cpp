#include <thread>
#include <iostream>
#include <cstdio>

//----------------------------------------------------------------------//
void func(int num, const std::string& str)
{
  std::cout << "num = " << num
	    << "str = " << str << std::endl;
}

//----------------------------------------------------------------------//
void oops(int num)
{
  char text[1024];
  sprintf(text,"%i",num);

  // Variable "text" is passed in as a char * and implicitly converted to an std::string inside the thread object, because the function "func" takes an std::string parameter. This conversion is likely not to finish before this "oops" function exits, leading to undefined behaviour because the character pointer is dangling.
  std::thread t(func, 5, text); 
  t.detach();
}	      

//----------------------------------------------------------------------//
void fix(int num)
{
  char text[1024];
  sprintf(text,"%i",num);

  // Explicitly convert the char * to an std::string before passing it as a paramter.
  std::thread t(func, 5, std::string(text)); 
  t.detach();
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  if (argc != 2)
    {
      std::cout << "Provider a number as a parameter." << std::endl;
      return 0;
    }

  int num = 0;
  try
    {
      num = std::stoi(argv[1]);
    }
  catch (...)
    {
      std::cout << "Provide a numerical argument." << std::endl;
    }

  fix(num);
  
  return 0;
}
