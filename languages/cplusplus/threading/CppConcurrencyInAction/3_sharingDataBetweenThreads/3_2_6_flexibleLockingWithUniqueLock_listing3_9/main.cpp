#include <iostream>
#include <thread>
#include <mutex>
#include <algorithm>
#include <memory>

// Description: Using std::lock and std::unique_lock in a swap operation for thread safety.

class BigData
{
  int num=0;
};

void swap(BigData& lhs, BigData rhs)
{
  std::swap(lhs,rhs);
}

class Something
{
public:
  explicit Something (std::unique_ptr<BigData> bigData)
    : m_bigData(std::move(bigData))
  {}

  static void swap(Something& lhs, Something& rhs)
  {
    if (&lhs == &rhs) { return; }

    // The defer_lock signals unique_lock to not lock the mutex on construction.
    std::unique_lock<std::mutex> ul1(lhs.m_mutex, std::defer_lock);
    std::unique_lock<std::mutex> ul2(rhs.m_mutex, std::defer_lock);
    
    // Lock both mutexes with one operation. 
    std::lock(ul1, ul2);

    /*
      Global scope operator needed because the function name
      "Something::swap" hides the global function name "swap" during
      function lookup of compilation. This is actually bad design; one
      of the functions should be renamed.
    */
    ::swap(*lhs.m_bigData,*rhs.m_bigData);
  }
  
private:
  std::mutex m_mutex;
  std::unique_ptr<BigData> m_bigData;
};

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  Something something1(std::unique_ptr<BigData>(new BigData()));
  Something something2(std::unique_ptr<BigData>(new BigData()));
  
  std::thread thread1(Something::swap,
		      std::ref(something1),
		      std::ref(something2));
  
  std::thread thread2(Something::swap,
		      std::ref(something2),
		      std::ref(something1));

  thread1.join();
  thread2.join();
  
  return 0;
}
