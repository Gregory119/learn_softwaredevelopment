#include <thread>
#include <string>
#include <unordered_map>
#include <mutex>
#include <boost/thread/shared_mutex.hpp>

class DnsCache final
{
public:
  //----------------------------------------------------------------------//
  std::string findEntryForDomain(const std::string& domain) const
  {
    // Shared locks on a shared mutex can happen at the same time. A
    // non-shared lock and shared lock on a shared mutex cannot happen
    // at the same time. Non-shared locks on a shared mutex cannot
    // happen at the same time.
    boost::shared_lock<boost::shared_mutex> sl(m_sharedMutex);

    std::cout << "findEntryForDomain after the shared lock." << std::endl;
    const auto& it = m_entries.find(domain);
    /* OR
       const std::unordered_map<std::string,std::string>::const_iterator it =
       m_entries.find(domain);
    */
    if (it == m_entries.end())
      {
	return std::string();
      }

    return it->second;
  }

  //----------------------------------------------------------------------//
  void updateEntryForDomain(const std::string& domain,
			    const std::string& entry)
  {
    std::lock_guard<boost::shared_mutex> lg(m_sharedMutex);
    m_entries[domain] = entry;
  }

  //----------------------------------------------------------------------//
  void print() const
  {
    std::lock_guard<boost::shared_mutex> lg(m_sharedMutex);
    for (const auto& e : m_entries)
      {
	std::cout << e.first << " " << e.second << " " << std::endl;
      }
    std::cout << std::endl;
  }
  
private:
  mutable boost::shared_mutex m_sharedMutex;

  std::unordered_map<std::string,std::string> m_entries;
};

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  std::unique_ptr<DnsCache> dnsCache = std::make_unique<DnsCache>();

  dnsCache->updateEntryForDomain("dev.seegrid","192.168.1.1");
  dnsCache->updateEntryForDomain("sales.seegrid","192.168.1.2");
  dnsCache->updateEntryForDomain("marketing.seegrid","192.168.1.3");

  // print
  std::thread printThread1 {[&](){
      dnsCache->print();
    }};
  
  // update
  std::thread t1 {[&](){
      dnsCache->updateEntryForDomain("test1.seegrid","192.168.1.6");
    }};

  std::thread t2 {[&](){
      dnsCache->updateEntryForDomain("test2.seegrid","192.168.1.8");
    }};
  
  std::thread t3 {[&](){
      dnsCache->updateEntryForDomain("test3.seegrid","192.168.1.9");
    }};

  // print
  std::thread printThread2 {[&](){
      dnsCache->print();
    }};

  // update
  std::thread ta {[&](){
      dnsCache->updateEntryForDomain("test1.seegrid","1.168.1.6");
    }};

  std::thread tb {[&](){
      dnsCache->updateEntryForDomain("test2.seegrid","1.168.1.8");
    }};
  
  std::thread tc {[&](){
      dnsCache->updateEntryForDomain("test3.seegrid","1.168.1.9");
    }};

  // print
  std::thread printThread3 {[&](){
      dnsCache->print();
    }};
  
  // find
  std::thread t4 {[&](){
      dnsCache->findEntryForDomain("test1.seegrid");
    }};

  std::thread t5 {[&](){
      dnsCache->findEntryForDomain("test2.seegrid");
    }};

  std::thread t6 {[&](){
      dnsCache->findEntryForDomain("test3.seegrid");
    }};

  dnsCache->print();
  
  t1.join();
  t2.join();
  t3.join();
  ta.join();
  tb.join();
  tc.join();
  t4.join();
  t5.join();
  t6.join();

  // print
  std::thread printThread4 {[&](){
      dnsCache->print();
    }};
  
  printThread1.join();
  printThread2.join();
  printThread3.join();
  printThread4.join();

  return EXIT_SUCCESS;
}
