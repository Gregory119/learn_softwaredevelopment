#include <iostream>
#include <thread>
#include <mutex>
//#include <algorithm>
#include <memory>

class DataPacket
{

};

class Messenger
{
public:
  Messenger(std::string connectionDetails)
    : m_connectionDetails(std::move(connectionDetails))
  {}

  void sendMessage(const DataPacket& dataPacket)
  {
    std::call_once(m_connectionInitFlag, &Messenger::connect, this);
    // Then send the message using the connection.
    return;
  }

  DataPacket receiveMessage()
  {
    std::call_once(m_connectionInitFlag, &Messenger::connect, this);
    // Then receive the message using the connection.
    return DataPacket();
  }
  
private:
  void connect()
  {
    std::cout << "Connecting with the details: " << m_connectionDetails << std::endl;
  }
  
  std::string m_connectionDetails;
  std::once_flag m_connectionInitFlag;
};

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  std::unique_ptr<Messenger> messenger = std::make_unique<Messenger>("connect details");
  DataPacket dataPacket;

  // Note: std::call_once needs to be used with at least one thread, other than the main thread.

  messenger->sendMessage(dataPacket);

  std::this_thread::sleep_for(std::chrono::seconds(1));
  
  std::thread t1(&Messenger::sendMessage,messenger.get(),std::ref(dataPacket));
  std::thread t2(&Messenger::sendMessage,messenger.get(),std::ref(dataPacket));
  
  t1.join();
  t2.join();

  return 0;
}
