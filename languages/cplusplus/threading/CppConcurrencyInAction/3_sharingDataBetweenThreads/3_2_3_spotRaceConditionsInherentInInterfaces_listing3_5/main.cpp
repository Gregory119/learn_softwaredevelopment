#include <exception>
#include <iostream>
#include <memory>
#include <mutex>
#include <stack>
#include <thread>

struct EmptyStackException : std::exception
{
  const char* what() const throw() override
  {
    return "Empty stack!";
  }
};

template <class T>
class ThreadSafeStack
{
public:
  ThreadSafeStack() = default;
  ThreadSafeStack(const ThreadSafeStack& rhs)
  {
    std::lock_guard<std::mutex> lg(rhs.m_mutex);
    m_data = rhs.m_data;
  }

  ThreadSafeStack& operator=(const ThreadSafeStack&) = delete;

  //----------------------------------------------------------------------//
  void push(const T& val)
  {
    std::lock_guard<std::mutex> lg(m_mutex);
    m_data.push(val);
  }

  //----------------------------------------------------------------------//
  /*
    This pop() function is a bad implementation because it has the problem described by Tom Cargill.
   */
  #ifdef NONE
  T pop()
  {
    std::lock_guard<std::mutex> lg(m_mutex);
    if (m_data.empty()) throw EmptyStackException();

    /*
      If this copy generates an exception (eg. a large std::vector), then the stack data will not be popped, which is good. The real problem comes next.
     */
    T val = m_data.top();
    m_data.pop();

    /*
      If this type does not implement move semantics, then it will be copied when returned. 
      If this copy generates an exception, then the element is not returned to the caller
      and it has been popped off the stack; this element has now been lost.
     */
    return val;
  }
  #endif
  
  //----------------------------------------------------------------------//
  /*
    This uses the reference option. This is good for small elements in the stack, which don't require much memory (eg. fundamental types like int, short..).
   */
  void pop(T& val)
  {
    std::lock_guard<std::mutex> lg(m_mutex);
    if (m_data.empty()) throw EmptyStackException();

    /* This will result in a copy of the stack element. 
       If this copy generates an exception (eg. copying a large std::vector), 
       the element is not popped from the stack; the element is not lost.*/
    val = m_data.top();
    m_data.pop();
  }

  //----------------------------------------------------------------------//
  /*
    This uses the pointer option. This is useful for large stack elements, which require a significant amount of memory (eg. a large user-defined type like a class or struct).
   */
  std::shared_ptr<T> pop()
  {
    std::lock_guard<std::mutex> lg(m_mutex);
    if (m_data.empty()) throw EmptyStackException();

    /* 
       This will result in a copy of the stack element into heap memory. 
       If this copy generates an exception (eg. copying a large std::vector), 
       the element is not popped from the stack; the element is not lost.
    */
    std::shared_ptr<T> ePtr = std::make_shared<T>(m_data.top());
    m_data.pop();
    return ePtr;
  }

  //----------------------------------------------------------------------//
  /*
    This is a public function that HOLDS the invariant when called, so it internally uses a lock. If any of the other public functions call this internally, there is a likely chance for a deadlock. Rather use a private empty() function that assumes the invariant IS NOT held when called (eg. the empty() of the internal stack).
   */
  bool empty()
  {
    std::lock_guard<std::mutex> lg(m_mutex);
    return m_data.empty();
  }
  
private:
  mutable std::mutex m_mutex;
  std::stack<T> m_data;
};

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  std::unique_ptr<ThreadSafeStack<int>> threadSafeStack =
    std::make_unique<ThreadSafeStack<int>>();

  // Thread pushing.
  std::thread pushThread([&](){
      for (int i=0; i<30; ++i)
	{
	  threadSafeStack->push(i);
	  std::cout << "Pushed " << i << std::endl;
	  std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}
    });

  // Thread popping and printing
  std::thread popThread([&](){
      // Wait for the thread to not be empty. This should actually use a promise and future.
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      while (!threadSafeStack->empty())
	{
	  std::this_thread::sleep_for(std::chrono::milliseconds(500));
	  int val {0};
	  try
	    {
	      threadSafeStack->pop(val);
	    }
	  catch (std::exception& e)
	    {
	      std::cout << "exception message: " << e.what() << std::endl;
	    }
	  std::cout << "Popped " << val << std::endl;
	}
      std::cout << std::endl;
    });
  
  pushThread.join();
  popThread.join();
  
  return EXIT_SUCCESS;
}
