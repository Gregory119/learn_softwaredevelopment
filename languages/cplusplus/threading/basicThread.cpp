#include <iostream>
#include <stack>

struct Node
{
  Node* left = nullptr;
  Node* right = nullptr;
  int data = 0;
};

void postOrderTraversal(Node* root)
{
  if (root == nullptr)
    {
      return;
    }
  
  stack<Node*> nodes_being_checked;
  queue<Node*> nodes_to_visit;
  nodes_to_visit.push(root);
  Node* cur = root;
  bool iterate_left = true;

  while (!nodes_to_visit.empty())
    {
      // iterate left
      if (iterate_left)
	{
	  while (cur != nullptr)
	    {
	      nodes_to_visit.push(cur);
	      cur = cur.left;
	    }
	}

      cur = nodes_to_visit.top();
      // move right
      if (cur.right != nullptr)
	{
	  cur = cur.right;
	  iterate_left = true;
	  continue;
	}
      else
	{
	  cout << cur.data << endl; // visit current node
	  nodes_to_visit.pop(); // pop current node
	  cout << nodes_to_visit.top(); << endl; // visit the most recent node to visit
	  nodes_to_visit.pop(); // pop the most recent visited node
	  cur = nodes_to_visit.top(); // start from the next most recent node to visit
	  iterate_left = false;
	}
    }
}

int main(int argc, char* argv[])
{
  
  
  
  return 0;
}
