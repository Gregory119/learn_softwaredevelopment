#pragma once

#include "Person.h"
#include <cassert>

namespace People
{
  // Polymorphic base class
  // Single responsibility: use and owner for callbacks
  class ObservedPerson : public Person
  {
  public:
    class Observer
    {
    protected:
      Owner() = default; // Must only be inherited.
      ~Owner() = default; // Can only be destroyed by derived class.
      // These two conditions imply using the following:
      // Moveable
      Owner(Owner&& rhs) = default;
      Owner& operator=(Owner&& rhs) = default;
      // Not copyable because there should only be one owner of the ObservedPerson class.
      Owner(const Owner& rhs) = delete;
      Owner& operator=(const Owner& rhs) = delete;

    private:
      virtual void handleNameChanged(ObservedPerson*) = 0;
      friend ObservedPerson;
    };

  public:
  ObservedPerson(Person::Owner* o, std::string name)
    : Person(o, name)
      {}
    
    virtual ~ObservedPerson() = default;
    // not copyable
    ObservedPerson(const ObservedPerson&) = delete;
    ObservedPerson& operator=(const ObservedPerson&) = delete;
    // moveable
    ObservedPerson(ObservedPerson&& rhs) = default;
    ObservedPerson& operator=(ObservedPerson&& rhs) = default;
    
    void setNullOwner(Owner* o) { owner = null; }
    void addObserver(Observer* ob);

    override void setName(std::string name);
    
  private:
    void notifyNameChanged()
    {
      notify([this](Observer* ob){
          ob->handleNameChanged(this);
        });
    }

    void notify(std::function<void(Observer*)> f)
    {
      if (!f)
        {
          assert(false);
          owner->handleError(this);
        }
      for (const auto& observer : observers)
        {
          f(observer);
        }
    }
    
  private:
    std::unordered_set<Observer*> obervers;
  };
};
