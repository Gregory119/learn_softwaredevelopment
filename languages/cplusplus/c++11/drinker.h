#pragma once

namespace People
{
  class Drinker
  {
    virtual void drink() = 0;
  };
};
