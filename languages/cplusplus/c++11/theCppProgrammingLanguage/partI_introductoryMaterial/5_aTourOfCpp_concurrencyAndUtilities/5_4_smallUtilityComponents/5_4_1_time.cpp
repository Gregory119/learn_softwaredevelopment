#include <iostream>
#include <chrono>
#include <unistd.h>

int main(int argc, char** argv)
{
  auto t0 = std::chrono::high_resolution_clock::now();
  
  std::cout << "Do work" << std::endl;
  sleep(1);
  
  auto t1 = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t1-t0);

  std::cout << "duration is " << duration.count() << std::endl;
  
  return EXIT_SUCCESS;
}
