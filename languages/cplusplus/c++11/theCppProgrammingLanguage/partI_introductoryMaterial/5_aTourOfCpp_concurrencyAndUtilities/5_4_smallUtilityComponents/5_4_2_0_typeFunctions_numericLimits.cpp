#include <limits>
#include <iostream>

int main(int argc, char** argv)
{
  constexpr float minFloat = std::numeric_limits<float>::min();
  constexpr float maxFloat = std::numeric_limits<float>::max();

  std::cout << "The minimum value of a float on this architecture, with this compiler, is: "
	    << minFloat << std::endl;
  std::cout << "The maximum value of a float on this architecture, with this compiler, is: "
	    << maxFloat << std::endl;
  
  return 0;
}
