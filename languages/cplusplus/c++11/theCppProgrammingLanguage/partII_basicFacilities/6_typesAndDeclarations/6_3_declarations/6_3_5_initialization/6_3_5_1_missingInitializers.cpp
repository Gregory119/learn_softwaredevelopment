#include <iostream>
#include <vector>

void print(int* arr, std::size_t size)
{
  for (std::size_t i=0; i<size; ++i)
    {
      std::cout << arr[i] << " ";
    }
  std::cout << std::endl;
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  constexpr int size = 20;
  int* arr {new int[size] {}}; // Default initializes all of the elements to zero => performance hit.

  print(arr,size);  
  
  return EXIT_SUCCESS;
}
