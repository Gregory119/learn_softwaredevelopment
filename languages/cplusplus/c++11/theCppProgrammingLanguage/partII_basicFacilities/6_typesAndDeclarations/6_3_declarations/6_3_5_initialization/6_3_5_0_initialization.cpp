#include <iostream>
#include <vector>

//----------------------------------------------------------------------//
template<class T>
void print(T val)
{
  std::cout << val << std::endl;
}

//----------------------------------------------------------------------//
void func(short i)
{
  print(i);
}

//----------------------------------------------------------------------//
template <class T>
void printVector(const std::vector<T>& v)
{
  std::cout << "vec = ";
  for (const auto& val : v)
    {
      std::cout << val << " ";
    }
  std::cout << std::endl;
}

struct Data
{
  int x1 = 5;
  int x2 = 6;
};

//----------------------------------------------------------------------//
void printArr(int* arr, std::size_t size)
{
  for (std::size_t i=0; i<size; ++i)
    {
      std::cout << arr[i] << " ";
    }
  std::cout << std::endl;
}  

//----------------------------------------------------------------------//
double retDoubleFunc(int a)
{
  return 2.0*a;
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  int x1 = 0; // Ok
  int x2 {0}; // Better
  short x3 = 1000000; // Bad, compiles with overflow WARNING.
  //short x4 {1000000}; // Good, narrowing compile ERROR because the value will cause an overflow.

  int y1 {1000000};
  short x4 = y1; // Bad, implicit conversion, NO compile WARNING.
  short x5 {y1}; // Good, narrowing compile WARNING.

  func(y1); // Bad, implicit conversion, NO compile WARNING.
  func({y1}); // Good, narrowing compile WARNING.
  
  print (x3);
  print (x4);

  std::cout << "----------------------------------------------------------------------" << std::endl;
  
  std::vector<int> vec1 {10}; // Vector constructed with an initialization list with its elements containing the values in the list.
  std::vector<int> vec2 (10); // Vector constructed with a size of 10, with all the elements initialized with a default value of zero.
  printVector(vec1);
  printVector(vec2);

  std::cout << "----------------------------------------------------------------------" << std::endl;
  // Empty initializer list uses the default value on initialization.
  Data data {}; // Calls the default constructor.
  std::cout << data.x1 << std::endl;

  std::vector<int> vec3 {}; // Default constructed.
  printVector(vec3);

  double d {}; // Sets it to 0.0.
  std::cout << d << std::endl;

  std::cout << "----------------------------------------------------------------------" << std::endl;

  constexpr int size = 5;
  int *p1 {new int[size]}; // Elements of the array are not well defined.
  int *p2 {new int[size] {1,3,2,4,5}}; // Elements of the array are initialized to the values in the initialization list.
  
  printArr(p2,size);
  printArr(p1,size); // These elements could print out as anything because they are not well defined.

  float num1 {retDoubleFunc(1)}; // Good, checks for narrowing when implicitly converting returned function value.
  auto num2 = retDoubleFunc(3); // Good, deduced as the return type of the function instead of an initialization list containing the returned value.

  std::cout << num1 << std::endl;
  
  return EXIT_SUCCESS;
}
