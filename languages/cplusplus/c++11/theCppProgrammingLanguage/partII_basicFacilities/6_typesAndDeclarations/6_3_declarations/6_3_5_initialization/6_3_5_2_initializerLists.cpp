#include <iostream>
#include <vector>

struct Car
{
  Car(int a, int b)
  {
    std::cout << "Car (int,int)" << std::endl;
    
    m_vals.push_back(a);
    m_vals.push_back(b);
  }

  Car(std::initializer_list<int> l)
  {
    std::cout << "Car (std::initializer_list<int>)" << std::endl;
    
    m_vals = l;
  }
  
  std::vector<int> m_vals;
};

void print(const std::vector<int> v)
{
  for (const auto val : v)
    {
      std::cout << val << " ";
    }
  std::cout << std::endl;
}

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  Car car {}; // Use empty initializer list.
  print(car.m_vals);

  Car car2 (1,2);
  print(car2.m_vals);

  Car car3(); // This is a function declaration.
  
  return EXIT_SUCCESS;
}
