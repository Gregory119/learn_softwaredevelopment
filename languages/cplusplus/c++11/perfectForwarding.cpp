#include <iostream>
#include <vector>

using namespace std;

//----------------------------------------------------------------------//
struct Data
{
  Data()
  {
    cout << "Data()" << endl;
  }

  Data(int val)
    : num(val)
  {
    cout << "Data(int)" << endl;
  }
  
  Data(const Data& rhs)
  {
    cout << "Data(const Data&)" << endl;
    num = rhs.num;
  }

  Data& operator=(const Data& rhs)
  {
    cout << "operator=(const Data&)" << endl;
    num = rhs.num;
  }

  Data(Data&& rhs)
  {
    cout << "Data(Data&&)" << endl;
    num = rhs.num;
  }

  Data& operator=(Data&& rhs)
  {
    cout << "operator=(Data&&)" << endl;
    num = rhs.num;
  }
  
  // default special members
  int num = 5;
};

//----------------------------------------------------------------------//
void func(Data&& data)
{
  cout << "func(Data&& data)" << endl;
  data.num = 4;
  cout << data.num << endl;
}

//----------------------------------------------------------------------//
void func(Data& data)
{
  cout << "func(Data& data)" << endl;
  data.num = 3;
  cout << data.num << endl;
}

//----------------------------------------------------------------------//
void func(const Data& data)
{
  cout << "func(const Data& data)" << endl;
  cout << data.num << endl;
}

//----------------------------------------------------------------------//
// This param will take on an l value or r value
template <class T>
void funcTemplateWithForwarding(T&& val)
{
  func(std::forward<T>(val));
}

//----------------------------------------------------------------------//
template <class T>
void funcTemplateNoForwarding(T&& val)
{
  // This does not maintain the reference type passed in.
  // val in this scope is an l value to and r value reference, and so the function with the l value reference parameter is called.
  func(val);
}

//----------------------------------------------------------------------//
int main(int argc, char* argv[])
{
  Data data;

  cout << "----------------------------------------------------------------------" << endl;
  funcTemplateNoForwarding(data);
  cout << "----------------------------------------------------------------------" << endl;
  funcTemplateNoForwarding(std::move(data));
  cout << "----------------------------------------------------------------------" << endl;
  funcTemplateWithForwarding(data);
  cout << "----------------------------------------------------------------------" << endl;
  funcTemplateWithForwarding(std::move(data));
  cout << "----------------------------------------------------------------------" << endl;
  cout << endl << endl;

 
  std::vector<Data> vec_data;
  cout << "----------------------------------------------------------------------" << endl;
  cout << "push_back vs emplace_back" << endl;
  Data data2;
  cout << "----------------------------------------------------------------------" << endl;
  cout << "vec_data.push_back(data2);" << endl;
  vec_data.push_back(data2);
  vec_data.clear();
  cout << "----------------------------------------------------------------------" << endl;
  cout << "vec_data.push_back(std::move(data2));" << endl;
  vec_data.push_back(std::move(data2));
  vec_data.clear();
  cout << "----------------------------------------------------------------------" << endl;
  cout << "vec_data.push_back(std::move(Data()));" << endl;
  vec_data.push_back(std::move(Data()));
  vec_data.clear();
  cout << "----------------------------------------------------------------------" << endl;
  cout << "vec_data.push_back(Data());" << endl;
  vec_data.push_back(Data());
  vec_data.clear();
  cout << "----------------------------------------------------------------------" << endl;
  cout << "vec_data.emplace_back(7);" << endl;
  vec_data.emplace_back(7);
  vec_data.clear();
  cout << "----------------------------------------------------------------------" << endl;
  
  return 0;
}
