nullptr instead of 0 or NULL.
Enum classes
Keywords override and final
Range based for loop
Keyword using for type aliases
Keyword auto
Uniform initialization
R-value references
Move constructors and assignment operators.
Smart pointers (unique, shared, ..?)
Lambda expressions
