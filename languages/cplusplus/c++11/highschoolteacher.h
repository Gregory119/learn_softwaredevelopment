#pragma once

#include "teacher.h"

namespace People
{
  class HighSchoolTeacher final : public Teacher
  {
  public:
    virtual void teach() = 0;
  };
};
