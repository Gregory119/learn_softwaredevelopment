#include <iostream>

using namespace std;

//----------------------------------------------------------------------//
template <class T>
class MoveableBuf final
{
public:
  MoveableBuf(int size)
    : d_size(size)
  {
    cout << "MoveableBuf normal constructor" << endl;
    d_data = new T[d_size];
  }

  ~MoveableBuf()
  {
    cout << "MoveableBuf destructor" << endl;
    delete d_data;
  }
  
  MoveableBuf(const MoveableBuf& rhs)
  {
    cout << "MoveableBuf copy constructor" << endl;
    operator=(rhs);
  }

  MoveableBuf& operator=(const MoveableBuf& rhs)
  {
    cout << "MoveableBuf equals operator" << endl;
    if (this == &rhs)
      {
	return *this;
      }

    if (d_size == rhs.d_size)
      {
	std::copy(rhs.d_data,rhs.d_data+d_size,d_data);
	return *this;
      }

    delete d_data;
    d_data = new T[d_size];
    std::copy(rhs.d_data,rhs.d_data+d_size,d_data);
    return *this;
  }
  
  MoveableBuf(MoveableBuf&& rhs)
  {
    cout << "MoveableBuf move constructor" << endl;
    operator=(std::move(rhs));
  }
  
  MoveableBuf& operator=(MoveableBuf&& rhs)
  {
    cout << "MoveableBuf move equals operator" << endl;
    if (this == &rhs)
      {
	return *this;
      }
    
    delete d_data;
    d_data = rhs.d_data;
    rhs.d_data = nullptr;
  }

private:
  T* d_data = nullptr;
  T d_size = 0;
};

//----------------------------------------------------------------------//
template <class T>
class CopyableOnlyBuf final
{
public:
  CopyableOnlyBuf(int size)
    : d_size(size)
  {
    cout << "CopyableOnlyBuf normal constructor" << endl;
    d_data = new T[d_size];
  }

  ~CopyableOnlyBuf()
  {
    cout << "CopyableOnlyBuf destructor" << endl;
    delete d_data;
  }
  
  CopyableOnlyBuf(const CopyableOnlyBuf& rhs)
  {
    cout << "CopyableOnlyBuf copy constructor" << endl;
    operator=(rhs);
  }

  CopyableOnlyBuf& operator=(const CopyableOnlyBuf& rhs)
  {
    cout << "CopyableOnlyBuf equals operator" << endl;
    if (this == &rhs)
      {
	return *this;
      }

    if (d_size == rhs.d_size)
      {
	std::copy(rhs.d_data,rhs.d_data+d_size,d_data);
	return *this;
      }

    delete d_data;
    d_data = new T[d_size];
    std::copy(rhs.d_data,rhs.d_data+d_size,d_data);
    return *this;
  }

private:
  T* d_data = nullptr;
  T d_size = 0;
};

//----------------------------------------------------------------------//
template <class T>
MoveableBuf<T> funcRetMove(bool test = true) // parameters present to avoid compilation optimization of return value
{
  MoveableBuf<T> res(10);
  return res;
}

//----------------------------------------------------------------------//
template <class T>
CopyableOnlyBuf<T> funcRetCopy(bool test = true) // parameters present to avoid compilation optimization of return value
{
  CopyableOnlyBuf<T> res(10);
  return res;
}

int main(int argc, char* argv[])
{
  cout << "----------------------------------------------------------------------" << endl;
  cout << "Return by value with moveable object:" << endl;
  MoveableBuf<int> moveableBuf1 = funcRetMove<int>();
  cout << "Return by value with copyable only object:" << endl;
  CopyableOnlyBuf<int> copyableBuf1 = funcRetCopy<int>();
  cout << "----------------------------------------------------------------------" << endl;
  cout << "Copy operator with moveable object:" << endl;
  MoveableBuf<int> moveableBuf2(0);
  moveableBuf2 = moveableBuf1;
  cout << "Move operator with moveable object:" << endl;
  moveableBuf2 = std::move(moveableBuf1);
  cout << "----------------------------------------------------------------------" << endl;
  cout << "Copy operator with copyable only object:" << endl;
  CopyableOnlyBuf<int> copyableBuf2(0);
  copyableBuf2 = copyableBuf1;
  cout << "Move equals operator with copyable only object (ends up using the copy equals operator):" << endl;
  copyableBuf2 = std::move(copyableBuf1);
  cout << "----------------------------------------------------------------------" << endl;

  return 0;
}
