#include <iostream>

using namespace std;

struct Data
{
  // default special members
  int num = 5;
};

int main(int argc, char* argv[])
{
  Data data;
  const Data const_data;

  // l value references
  Data& data_lvalueref1 = data;
  Data& data_lvalueref2 = data_lvalueref1;
  //Data& data_lvalueref3 = const_data; // does not compile
  //Data& data_lvalueref4 = std::move(data); // does not compile
  //Data& data_lvalueref6 = std::move(const_data); // does not compile

  // const lvalue references
  const Data& data_lvalueref7 = data;
  const Data& data_lvalueref8 = data_lvalueref7;
  const Data& data_lvalueref9 = const_data;
  const Data& data_lvalueref10 = std::move(data); // NB!!!!!!!!!
  const Data& data_lvalueref11 = std::move(const_data); // NB!!!!!!!!!

  // r value references
  Data&& data_rvalueref1 = std::move(data);
  Data&& data_rvalueref2 = std::move(data_rvalueref1); // NB!!!!!!!!!
  //Data&& data_rvalueref2 = data_rvalueref1; // NB!!!!! does not compile
  //Data&& data_rvalueref3 = data; // does not compile
  //Data&& data_rvalueref4 = const_data; // does not compile
  //Data&& data_rvalueref5 = std::move(const_data); // does not compile
  
  return 0;
}
