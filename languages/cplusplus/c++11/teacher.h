#pragma once

#include ""

namespace People
{
  class Teacher
  {
  public:
    virtual void teach() = 0;
  };
};
