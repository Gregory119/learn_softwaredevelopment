#include <iostream>

using namespace std;

//----------------------------------------------------------------------//
struct Data
{
  // default special members
  int num = 5;
};

//----------------------------------------------------------------------//
// Takes an r value (which is now an l value of an r value)
void funcMoveParam(Data&& data)
{
  data.num = 4;
  cout << data.num << endl;
}

//----------------------------------------------------------------------//
void funcRefParam(Data& data)
{
  data.num = 3;
  cout << data.num << endl;
}

//----------------------------------------------------------------------//
void funcConstRefParam(const Data& data)
{
  cout << data.num << endl;
}

//----------------------------------------------------------------------//
// This param will take on an l value or r value
template <class T>
void funcTemplate(T&& val)
{
  // Assuming the function parameter has this member
  cout << val.num << endl; 
}

//----------------------------------------------------------------------//
int main(int argc, char* argv[])
{
  Data data;
  const Data const_data;

  cout << "----------------------------------------------------------------------" << endl;
  cout << "Normal functions with lvalue and rvalue references" << endl;
  cout << "----------------------------------------------------------------------" << endl;
  cout << "funcRefParam(data);" << endl;
  funcRefParam(data);
  cout << "----------------------------------------------------------------------" << endl;
  //funcMoveParam(data); // does not compile
  cout << "funcMoveParam(std::move(data));" << endl;
  funcMoveParam(std::move(data));
  cout << "----------------------------------------------------------------------" << endl;
  cout << "funcConstRefParam(data);" << endl;
  funcConstRefParam(data);
  cout << "----------------------------------------------------------------------" << endl;
  cout << "funcConstRefParam(std::move(data));" << endl;
  funcConstRefParam(std::move(data));
  cout << "----------------------------------------------------------------------" << endl;
  cout << endl << endl;

  
  cout << "----------------------------------------------------------------------" << endl;
  cout << "Template function with rvalue reference" << endl;
  cout << "----------------------------------------------------------------------" << endl;
  cout << "funcTemplate(data);" << endl;
  funcTemplate(data); // Parameter becomes an lvalue reference.
  cout << "----------------------------------------------------------------------" << endl;
  cout << "funcTemplate(std::move(data));" << endl;
  funcTemplate(std::move(data)); // Parameter becomes an rvalue reference.
  cout << "----------------------------------------------------------------------" << endl;
  cout << "funcTemplate(const_data);" << endl;
  funcTemplate(const_data); // Parameter becomes a const l value reference.
  cout << "----------------------------------------------------------------------" << endl;
  cout << "funcTemplate(std::move(const_data));" << endl;
  funcTemplate(std::move(const_data)); // Parameter becomes a const r value reference.
  cout << "----------------------------------------------------------------------" << endl;
  
  return 0;
}
