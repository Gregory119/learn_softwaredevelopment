#include <iostream>

class Data
{
public:
  Data()
  {
    std::cout << "Data()" << std::endl;
  }
  
  Data(Data&&)
  {
    std::cout << "Data(Data&&)" << std::endl;
  }

  Data& operator=(Data&&)
  {
    std::cout << "Data& operator=(Data&&)" << std::endl;
  }
};

class Base
{
public:
  Base(Data data)
    : m_data(std::move(data))
  {}

private:
  Data m_data;
};

//----------------------------------------------------------------------//
int main(int argc, char** argv)
{
  Data data;
  Base base {std::move(data)};

  std::cout << "----------------------------------------------------------------------" << std::endl;

  Base base2 {Data{}};
  
  return EXIT_SUCCESS;
}
