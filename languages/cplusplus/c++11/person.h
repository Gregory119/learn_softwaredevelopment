#pragma once

namespace People
{
  // Polymorphic abstract base class
  class Person : public Eater, Drinker
  {
  public:
    // The Owner class is useful for callback functionality, when:
    // - You want to enforce a contract for handling callbacks.
    // - More than one public interface function may result in the same synchronous callback condition (handling exceptions and error conditions for each function call will result in repetitive code).
    // - Asynchronous callback conditions are handled.
    class Owner
    {
    protected:
      Owner() = default; // Must only be inherited.
      ~Owner() = default; // Can only be destroyed by derived class.
      // These two conditions imply using the following:
      // Moveable (ownership of OwnedObservedPerson is transferred)
      Owner(Owner&& rhs) = default;
      Owner& operator=(Owner&& rhs) = default;
      // Not copyable because there should only be one owner of the OwnedObservedPerson class.
      Owner(const Owner& rhs) = delete;
      Owner& operator=(const Owner& rhs) = delete;

    private:
      virtual void handleError(OwnedObservedPerson*) = 0;
      friend OwnedObservedPerson;
    };    

  Person(Owner o, std::string name)
    : owner(o),
      this->name(name)
        {}
    
    virtual ~Person() = default;
    // is not copyable (a person should be unique enough with these member variables)
    Person(const Person&) = delete;
    Person& operator=(const Person&) = delete;
    // is moveable
    Person(Person&&) = default;
    Person& operator=(Person&&) = default;

    virtual void setName(std::string name);
    
  private:
    Owner owner* = nullptr;
    std::string name;
  };
};
