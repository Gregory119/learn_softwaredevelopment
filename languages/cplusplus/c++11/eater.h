#pragma once

namespace People
{
  class Eater
  {
    virtual void eat() = 0;
  };
};
