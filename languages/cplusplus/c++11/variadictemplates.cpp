#include <iostream>
#include <vector>

using namespace std;

//----------------------------------------------------------------------//
struct Data
{
  Data()
  {
    cout << "Data()" << endl;
  }

  Data(int val)
    : num(val)
  {
    cout << "Data(int)" << endl;
  }
  
  Data(const Data& rhs)
  {
    cout << "Data(const Data&)" << endl;
    num = rhs.num;
  }

  Data& operator=(const Data& rhs)
  {
    cout << "operator=(const Data&)" << endl;
    num = rhs.num;
  }

  Data(Data&& rhs)
  {
    cout << "Data(Data&&)" << endl;
    num = rhs.num;
  }

  Data& operator=(Data&& rhs)
  {
    cout << "operator=(Data&&)" << endl;
    num = rhs.num;
  }
  
  // default special members
  int num = 5;
};

//----------------------------------------------------------------------//
void func(int a)
{
  cout << "func(int a)" << endl;
}

//----------------------------------------------------------------------//
void func(double a, int b)
{
  cout << "func(double a, int b)" << endl;
}

//----------------------------------------------------------------------//
void func(Data& data, double a, int b)
{
  cout << "func(Data& data, double a, int b)" << endl;
}

//----------------------------------------------------------------------//
// Variadic function template with type deduction and perfect forwarding.
template <class... T> // parameter pack
void funcVariadic(T&&... vals) // parameter pack expansion
{
  func(std::forward<T>(vals)...); // parameter pack expansion
}

//----------------------------------------------------------------------//
int main(int argc, char* argv[])
{
  int a = 1;
  int b = 2;
  Data data;

  funcVariadic(a);
  funcVariadic(a,b);
  funcVariadic(data,a,b);
  
  return 0;
}
