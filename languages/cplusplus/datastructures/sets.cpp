#include <iostream>
#include <array>
#include <set>
#include <unordered_set>

using namespace std;

//----------------------------------------------------------------------//
struct Person
{
  // needed for trees
  bool operator<(const Person& rhs) const
  {
    // sort by name
    bool ret = name<rhs.name;
    cout << "Person::operator< is " << ret << endl;
    
    return ret;
  }

  // needed for hashing
  bool operator==(const Person& rhs) const
  {
    return name == rhs.name && id == rhs.id;
  }
  
  void print() const
  {
    cout << "name = " << name << "\nid = " << id << endl;
  }

  std::string name;
  int id;
};

//----------------------------------------------------------------------//
namespace std
{
  template <>
  struct hash<Person>
  {
    std::size_t operator()(const Person& p) const
    {
      cout << "hash<Person>()" << p.name << endl;
      return hash<string>()(p.name);
    }
  };
};

//----------------------------------------------------------------------//
class Document
{
public:
  std::string name;
};

//----------------------------------------------------------------------//
int main(int argc, char* argv[])
{
  std::array<Person,4> people{"Greg",1,"Hillary",2,"Mandy",3,"Jen",4};
  std::array<Document,4> docs = {"Doc1","Doc2","Doc3","Doc4"};

  std::set<Person> set;
  
  cout << "----------------------------------------------------------------------" << endl;
  cout << "Tree Set:" << endl;
  for (const auto& p : people)
    {
      p.print();
      auto it = set.find(p);
      if (it == set.end())
	{
	  set.insert(p);
	}
    }

  cout << "----------------------------------------------------------------------" << endl;
  cout << "Hash Set:" << endl;
  cout << "Adding to hash set:" << endl;
  std::unordered_set<Person> hash_set{people.begin(),people.end()};

  cout << "Checking hash set:" << endl;
  for (const auto& p : people)
    {
      auto it = hash_set.find(p);
      if (it == hash_set.end())
	{
	  hash_set.insert(p);
	}
      p.print();
    }
  
  return 0;
}
