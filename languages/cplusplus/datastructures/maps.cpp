#include <iostream>
#include <array>
#include <map>
#include <unordered_map>

using namespace std;

//----------------------------------------------------------------------//
struct Person
{
  // needed for trees
  bool operator<(const Person& rhs) const
  {
    // sort by name
    bool ret = name<rhs.name;
    cout << "Person::operator< is " << ret << endl;
    
    return ret;
  }

  // needed for hashing
  bool operator==(const Person& rhs) const
  {
    return name == rhs.name && id == rhs.id;
  }
  
  void print() const
  {
    cout << "name = " << name << "\nid = " << id << endl;
  }

  std::string name;
  int id;
};

//----------------------------------------------------------------------//
namespace std
{
  template <>
  struct hash<Person>
  {
    std::size_t operator()(const Person& p) const
    {
      cout << "hash<Person>()" << p.name << endl;
      return hash<string>()(p.name);
    }
  };
};

//----------------------------------------------------------------------//
class Document
{
public:
  bool operator==(const Document& rhs)
  {
    return name == rhs.name;
  }
  
  std::string name;
};

//----------------------------------------------------------------------//
int main(int argc, char * argv[])
{
  // Add people and documents to map and then print them out in order.
  constexpr std::size_t size = 4;
  std::array<Person,size> people{"Greg",1,"Hillary",2,"Mandy",3,"Jen",4};
  std::array<Document,size> docs = {"Doc1","Doc2","Doc3","Doc4"};

  std::map<Person,Document> map;
  
  cout << "----------------------------------------------------------------------" << endl;
  cout << "Using std::map" << endl;
  cout << "----------------------------------------------------------------------" << endl;
  // COMMON WAYS TO INSERT
  map.insert(std::pair<Person,Document>(Person{"Jayson",55},Document{"old"})); // Construct and then move temporary.
  map.emplace(Person{"Tom",10},Document{"secrets"}); // Construct in-place
  std::pair<Person,Document> pair; // Can be created without assigning values.
  cout << "pair.key = " << pair.first.name << "\npair.value = " << pair.second.name << endl; // values are defaulted

  // INSERT/CHECK WITH []
  Person new_person = Person{"Joe",6};
  map[new_person] = Document{"Ish"};
  if (map[new_person] == Document{"Ish"})
    {
      cout << "Joe is mapped to document Ish" << endl;
    }

  if (map.find(new_person) != map.end())
    {
      cout << "Joe is mapped to document Ish" << endl;
    }

  for (unsigned i=0; i<size; ++i)
    {
      // could have used emplace
      map.insert(std::pair<Person,Document>(people[i],docs[i]));
    }

  cout << "Printing out the mapping value in order of keys:" << endl;
  for (const auto& pair : map)
    {
      cout << "key = " << pair.first.name << ", value = " << pair.second.name << endl;
    }
  
  cout << "----------------------------------------------------------------------" << endl;
  cout << "Using std::unordered_map" << endl;
  cout << "----------------------------------------------------------------------" << endl;

  // Add people and documents to unordered_map, find one, modify it
  std::unordered_map<Person,Document> hashmap;

  hashmap.insert(std::pair<Person,Document>(Person{"Joe",5},Document{"Ish"}));
  Person new_person2{"Tom",10};
  hashmap[new_person2] = Document{"secrets"};
  if (hashmap.find(new_person2) != hashmap.end())
    {
      cout << "Tom is in the hashset." << endl;
    }

  for (unsigned i=0; i<size; ++i)
    {
      hashmap.emplace(people[i],docs[i]);
    }
  
  for (const auto& pair : hashmap)
    {
      cout << "key = " << pair.first.name << ", value = " << pair.second.name << endl;
    }
  
  cout << "----------------------------------------------------------------------" << endl;
  
  return 0;
}
