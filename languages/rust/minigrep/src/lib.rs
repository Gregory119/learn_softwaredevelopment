use std::env;
use std::fs;

pub fn run(config: Config) -> Result<(), String> {
    let contents;
    match fs::read_to_string(&config.filename) {
        Ok(text) => contents = text,
        Err(err) => return Err(format!("filename: {}. Error: {}", config.filename, err)),
    }
    println!("Contents: \n{}", contents);

    println!("Matched lines:");
    let matches;
    if config.case_sensitive {
        matches = search(&config.query, &contents);
    } else {
        matches = search_case_insensitive(&config.query, &contents);
    }

    for line in matches {
        println!("{}", line);
    }
    Ok(())
}

pub struct Config {
    pub query: String,
    pub filename: String,
    pub case_sensitive: bool,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        let query = match args.get(1) {
            Some(arg1) => arg1,
            None => return Err("Missing argument - No text to search"),
        };

        let filename = match args.get(2) {
            Some(arg2) => arg2,
            None => return Err("Missing argument - No filename"),
        };

        let case_sensitive = env::var("CASE_INSENSITIVE").is_err();

        Ok(Config {
            query: query.clone(),
            filename: filename.clone(),
            case_sensitive,
        })
    }
}

pub fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let mut results = Vec::new();

    for line in contents.lines() {
        if line.contains(query) {
            results.push(line);
        }
    }

    results
}

pub fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let query = query.to_lowercase();
    let mut results = Vec::new();

    for line in contents.lines() {
        if line.to_lowercase().contains(&query) {
            results.push(line);
        }
    }

    results
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn case_sensitive() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape.";
        assert_eq!(vec!["safe, fast, productive."], search(query, contents));
    }

    #[test]
    fn case_insensitive() {
        let query = "rUsT";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";
        assert_eq!(
            vec!["Rust:", "Trust me."],
            search_case_insensitive(query, contents)
        );
    }
}
