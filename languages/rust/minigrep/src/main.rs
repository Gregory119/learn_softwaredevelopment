use minigrep::Config;
use std::env;
use std::process;

fn main() -> Result<(), String> {
    let args: Vec<String> = env::args().collect();

    let config = match Config::new(&args) {
        Ok(args) => args,
        Err(reason) => return Err(format!("Failed to parse parameters. Reason: {}", reason)),
    };
    println!("Query: {}, Filename: {}", config.query, config.filename);

    if let Err(e) = minigrep::run(config) {
        eprintln!("Application failed: {}", e);
        process::exit(1);
    }
    Ok(())
}
