# Learning Rust

Code created while following the rust book: https://doc.rust-lang.org/book/

## Basic Commands
* `rustup update` - update
* `rustc --version` - print version information `rustc <version> (<commit hash> <commit date>)`
* `rustup doc` - read rust documentation locally in browser
* `curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh` - download and install
* `rustup self uninstall` - uninstall rust

## Common Tools
* `rustup` - toolchain installer
* `rustc` - compiler (can be used to compile a single rust file manually but not helpful for multi-file code)
* `rustfmt` - code formatter
* `cargo` - package manager

## First Project
* `cargo new <project name>` - creates directory with project name and initial files.
* `Cargo.toml` - contains package and dependency information.
* `Cargo.lock` - automatically keeps track of dependency versions.
* `src/` - contains all source code.
* `cargo build` - download dependencies and build project.
* `target/debug/` directory contains `debug` build artifacts, including the executable binary.
* `cargo run` - build and run a project.
* `cargo check` - compile code with producing artifacts (helpful for quick compilation verification).
* `cargo build --release` - builds a release version with optimizations.
* `target/release/` directory contains `release` version artifacts.

## Creating a Guessing a Game
* More Cargo.toml keys and definitions: https://doc.rust-lang.org/cargo/reference/manifest.html
* `cargo doc --open` - build local documentation for all dependencies

## Terminology
* `crates` - packages of code used as dependencies

