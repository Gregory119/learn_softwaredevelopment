#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn square(side: u32) -> Rectangle {
        Rectangle { width: side, height: side }
    }

    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        (self.width > other.width) && (self.height > other.height)
    }
}

fn main() {
    // function
    let width = 5;
    let height = 10;
    println!("Area of rectangle is {}", area_vars(width, height));

    // tuple
    let dimensions = (width, height);
    println!("Area of rectangle is {}", area_tuple(dimensions));

    // struct
    let rect = Rectangle { width, height };
    println!("Area of rectangle is {}", area_struct(&rect));
    println!("Area of rectangle is {}", area_struct(&rect));

    // print struct as debug
    println!("rect = {:#?}", rect);

    // member funtion
    println!("Area of rectangle is {}", rect.area());

    // can_hold
    let rect2 = Rectangle {
        width: 3,
        height: 4,
    };
    println!("rect can hold rect2? {}", rect.can_hold(&rect2));

    println!("My first square: {:#?}", Rectangle::square(15));
}

fn area_vars(width: u32, height: u32) -> u32 {
    width * height
}

fn area_tuple(dimensions: (u32, u32)) -> u32 {
    dimensions.0 * dimensions.1
}

fn area_struct(rect: &Rectangle) -> u32 {
    rect.width * rect.height
}
