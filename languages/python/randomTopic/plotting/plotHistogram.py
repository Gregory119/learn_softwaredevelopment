#!/usr/bin/env python

# References:
# https://matplotlib.org/users/pyplot_tutorial.html

import numpy as np
import matplotlib.pyplot as plt

np.random.seed(19608)

mu, sigma = 100, 15
x = mu + sigma * np.random.randn(10000)

n, bins, patches = plt.hist(x, bins=50, normed=None, facecolor='g', alpha=0.75)
plt.xlabel('Smarts')
plt.ylabel('Probability')
plt.title('Histogram of IQ')
#plt.text(
plt.grid(True)

plt.annotate('my IQ', xy=(90,500), xytext=(60,400), arrowprops=dict(facecolor='blue',shrink=0.05))

plt.show()
