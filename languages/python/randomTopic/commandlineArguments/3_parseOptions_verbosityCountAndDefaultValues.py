#!/usr/bin/env python#!/usr/bin/env python

# This is part of the tutorial at https://docs.python.org/2/howto/argparse.html#id1
# Verbosity is now controlled by the number of occurences of the option.

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("square", help="The value to be squared.", type=int)
parser.add_argument("-v","--verbosity",
                    action="count",
                    default=0,
                    help="increase output verbosity")
args = parser.parse_args()

answer = args.square**2

if args.verbosity >= 2:
    print "The square of {} is {}".format(args.square,answer)
elif args.verbosity >= 1:
    print "{}^2 == {}".format(args.square,answer)
else:
    print answer
