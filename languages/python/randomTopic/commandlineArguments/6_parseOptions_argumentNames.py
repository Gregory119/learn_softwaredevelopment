#!/usr/bin/env python

import argparse

"""
Description:
Raise a base x to an exponent y. The verbosity level is used to print more text.
Uses a mutually exclusive group and adds a program description.

This was modified to test the variable name of an argument that has a separator in the name. eg "--verbose-var" or "verbose__var"
"""

parser = argparse.ArgumentParser(description="Raise a base to an exponent.")
group = parser.add_mutually_exclusive_group()

# Note that the argument "--verbose--var" gets converted to the variable verbose_var.
group.add_argument("-v","--verbose--var", action="store_true")
group.add_argument("-q","--quiet",action="store_true")
parser.add_argument("x",type=int,help="the base")
parser.add_argument("y",type=int,help="the exponent")
args = parser.parse_args()

answer = args.x**args.y
if args.quiet:
    print answer
elif args.verbose_var:
    print "{} to the power {} equals {}".format(args.x, args.y, answer)
else:
    print "{}^{} == {}".format(args.x,args.y,answer)
