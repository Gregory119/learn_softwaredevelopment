#!/usr/bin/env python

# This is part of the tutorial at https://docs.python.org/2/howto/argparse.html#id1

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("square", help="echo the string you use here", type=int)
parser.add_argument("-v","--verbose", help="increase output verbosity", action="store_true")
args = parser.parse_args()

answer = args.square**2
if args.verbose:
    print "The square of {} is {}".format(args.square,answer)
else:
    print answer
