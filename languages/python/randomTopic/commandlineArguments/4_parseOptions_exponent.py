#!/usr/bin/env python

import argparse

"""
Description:
Raise a base x to an exponent y. The verbosity level is used to print more text.
This is part of the tutorial at https://docs.python.org/2/howto/argparse.html#id1
"""

parser = argparse.ArgumentParser()
parser.add_argument("x",
                    type=int,
                    help="The base.")
parser.add_argument("y",
                    type=int,
                    help="The exponent.")
parser.add_argument("-v",
                    "--verbosity",
                    action="count",
                    default=0,
                    help="Every occurence increases the verbosity count.")
args = parser.parse_args()

answer = args.x**args.y
if args.verbosity >= 2:
    print "Running {}".format(__file__)

if args.verbosity >= 1:
    print "{}^{} == {}".format(args.x, args.y, answer)

print answer
