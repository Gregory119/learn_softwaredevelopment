#!/usr/bin/env python

import argparse

"""
Description:
Raise a base x to an exponent y. The verbosity level is used to print more text.
Uses a mutually exclusive group and adds a program description.
This is part of the tutorial at https://docs.python.org/2/howto/argparse.html#id1
"""

parser = argparse.ArgumentParser(description="Raise a base to an exponent.")
group = parser.add_mutually_exclusive_group()
group.add_argument("-v","--verbose", action="store_true")
group.add_argument("-q","--quiet",action="store_true")
parser.add_argument("x",type=int,help="the base")
parser.add_argument("y",type=int,help="the exponent")
args = parser.parse_args()

answer = args.x**args.y
if args.quiet:
    print answer
elif args.verbose:
    print "{} to the power {} equals {}".format(args.x, args.y, answer)
else:
    print "{}^{} == {}".format(args.x,args.y,answer)
