#!/usr/bin/env python

# push C-c C-c to execute this module within emacs

#----------------------------------------------------------------------#
# Testing args
#----------------------------------------------------------------------#
# Use this to pass in a variable number of arguments.
# *argv could have been named anything else after the * (eg. *params)
def testVarArgs(fArg, *argv):
    print "First normal arg: ", fArg
    for arg in argv:
        print "Another arg through *argv: ", arg

testVarArgs('hello',4,5.6,'bla')

#----------------------------------------------------------------------#
# Testing kwargs
#----------------------------------------------------------------------#
# Use this to pass in a keyworded variable number of arguments.
def wowKeywords(**kwargs):
    if kwargs is not None:
        for key, value in kwargs.iteritems():
            print "%s == %s" %(key,value)

wowKeywords(var1='food',var2=5)
