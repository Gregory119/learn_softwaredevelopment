#!/usr/bin/env python

#----------------------------------------------------------------------#
# Validator function and exception handling
#----------------------------------------------------------------------#
def test(val):
    if not isinstance(val,str):
        raise ValueError("{} must be of type str".format(val))
    return "Is an instance."

print test("hello")
print test('hello')

try:
    print test(5)
except ValueError as err:
    print "Caught exception: {0}".format(err)


#----------------------------------------------------------------------#
# 
#----------------------------------------------------------------------#
