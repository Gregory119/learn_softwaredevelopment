#!/usr/bin/env python

# Import the functools.wraps decorator
from functools import wraps

def mean(first, second, *rest):
    print rest
    print type(rest) # Will be of type tuple.
    
    numbers = (first, second) + rest
    return sum(numbers)/len(numbers)

def floatArgsAndReturn(function):
    def wrapper(*args,**kwargs):
        """
        wrapper documentation string.
        """
        print "args before = ", args
        args = [float(arg) for arg in args]
        print "args after = ", args
        return float(function(*args, **kwargs))
    return wrapper

@floatArgsAndReturn
def meanDecorated(first, second, *rest):
    """
    meanDecorated documentation string.
    """
    return mean(first,second,*rest)

#----------------------------------------------------------------------#
# Correcting the decorated function __name__ and __doc__ attribute by using @functools.wraps(function)

def floatArgsAndReturnWithDoc(function):
    @functools.wraps(function)
    def wrapperWithDoc(*args,**kwargs):
        """
        wrapperWithDoc documentation string.
        """
        print "args before = ", args
        args = [float(arg) for arg in args]
        print "args after = ", args
        return float(function(*args, **kwargs))
    return wrapperWithDoc

@floatArgsAndReturnWithDoc
def meanDecWithDoc(first, second, *rest):
    """
    meanDecWithDoc documentation string.
    """
    return meanWithDoc(first,second,*rest)



print
print "----------------------------------------------------------------------"
print "START"
print "----------------------------------------------------------------------"
val = (2,3) + (6,)
print "val=",val

print "----------------------------------------------------------------------"
meanVal1 = mean(5,4,10,11)
print meanVal1

print "----------------------------------------------------------------------"
print "Interesting for loop"
arr = [1,2,3,4]
#print e for e in arr

print "----------------------------------------------------------------------"
print "Using a function decorator directly"
f = floatArgsAndReturn(mean)
print f(2,3,6) 

print "----------------------------------------------------------------------"
print "Using a defined function decorator"
print meanDecorated(2,3,6)

print "----------------------------------------------------------------------"
print "Function decorator __info__ and __doc__"
# Notice that this outputs 'wrapper' as the function name, when we
# actually want it to be 'meanDecorated'.
print meanDecorated.__name__ 

# Notice that this outputs the wrapper function documentation string,
# when we actually want the meanDecorated function documentation
# string.
print meanDecorated.__doc__

# These attributes are corrected by using the @functools.wraps
# decorator, with the following results.
print meanDecWithDoc.__name__
print meanDecWithDoc.__doc__

