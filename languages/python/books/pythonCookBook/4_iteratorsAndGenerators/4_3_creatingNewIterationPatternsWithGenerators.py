#!/usr/bin/env python

def frange(start, stop, increment):
    x = start
    while x < stop:
        yield x
        x += increment

print "----------------------------------------------------------------------"
# Create the generator and manually iterate each returned value by the yield statement.
frangeGen = frange(13.5,16.0,0.5)
print frangeGen.next()
print frangeGen.next()
print frangeGen.next()
print frangeGen.next()

print "----------------------------------------------------------------------"
# Iterate with a generator using a for loop.
for num in frange(13.5,16.0,0.5):
    print num

print "----------------------------------------------------------------------"
# Use a generator to print a count down

def countDown(start):
    while (start > 0):
        yield start
        start = start-1

countDownGen = countDown(10)

print "Calling next.. gives the count at ", countDownGen.next()

# Notice how this will continue the iteration from the last call.
for count in countDownGen:
    print "The count is ", count

print "----------------------------------------------------------------------"
def printFunc(var):
    print "Print variable ", var

def testFunc():
    yield printFunc(4)
    yield printFunc(6)

tester1 = testFunc()
tester1.next()
tester1.next()

tester2 = testFunc()
for test in tester2:
    if test == None:
        print "It's None"
    pass # Nothing to do here because test will be None, but next() has been called.

# Run in a loop and check if an exception is thrown, indicating that
# the iteration has ended.
testerLoop = testFunc()
while True:
    try:
        testerLoop.next()
    except StopIteration:
        print "No more iterators to process.."
        break

print "----------------------------------------------------------------------"

def getName():
    return "Harry"
    
class TestClass:
    # This is a class (static) variable.
    forkLength = 5

    def __init__(self):
        self.name = "Gary"
        self.msg = "first msg"
    
    def test1(self):
        self.msg = yield "Bla"
        self.name = yield getName()

    def test2(self):
        self.name = getName()

        
testClass1 = TestClass()
testClass2 = TestClass()
print "testClass1.forkLength = ", testClass1.forkLength
print "testClass2.forkLength = ", testClass2.forkLength

testGenFunc1 = testClass1.test1()
print "testClass1.msg = ", testClass1.msg
print "testGenFunc1.next() = ",testGenFunc1.next() # This yields and returns 'Bla', without assigning anything to self.msg
print "testClass1.msg = ", testClass1.msg

# Send a value to the last yield, which is then assigned to self.msg. The program immediately continues and then yields after the call of getName(), returning the result of getName().
val = testGenFunc1.send("New message") # The returned value is the result of the getName() call.
print "testGenFunc1.send('New message') = ", val 
print "testClass1.msg = ", testClass1.msg
print "testClass1.name = ", testClass1.name

try:
    testGenFunc1.send("New name")
except StopIteration:
    pass

print "testClass1.name = ", testClass1.name
