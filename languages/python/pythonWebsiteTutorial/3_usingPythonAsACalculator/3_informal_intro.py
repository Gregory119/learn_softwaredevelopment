#!/usr/bin/env python

# push C-c C-c to execute this module within emacs

print("Informal Introduction to Python")

# add comments with the hash symbol

# a comment cannot appear in a string literal
text = "# This is not a comment because it's within quotes. Here the hash is just a hash."
print(text)

print("Using Python as a calculator")
print(2+2)
print(50-5*6)
print((50-0.6*8)/2)
print(8/5.0)

# integers have type int, and decimals have type float.
# implicit conversion occurs for operators
print("Floor division is performed with two integers")
print(5/2)

print("explicit floor divison can be done with //")
print(5.0//2.0)

print("The modulus operator % is available for remainders")
print(11%2)

print("operator ** is used for powers.")
print(3**2)
# assigment
var = 5
print(var)
print ""

# in interactive mode, the last printed expression is assigned to the variable _.

print("complex numbers (j or J)")
print(5-2j)
print(5-2J)
print ""

print("strings can be in '' or "" and \ can escape a quote")
str = 'new name is \'Greg\''
print(str)
str_2 = "new name is 'Greg'" # different internal quotes don't need to be escaped
print(str_2)
print ""

print("A raw string can be used to ignore the \ as a special character by placing a r infront.")
dir = 'C:\new\dir'
print(dir)
raw_dir = r'C:\new\dir'
print(raw_dir)
print ""

# Using the \ character to stop automatic insertion of new line in a multi-line print
print """\
\
Multi
     Line
     Print"""
print ""

print "Repeat and concatenate strings."
print "one "*3
print "first"+"second"
text = ('automatically concatenate'
' strings over multiple lines '
'using the \' character and brackets ()')
print(text)
print("another auto" "concatenation")
print("auto-concatenate a variable and string with +")
text = "hello"
print(text+" add on")
print ""

print "String indexing"
word = "hello"
print word[0]
print word[-1]
print ""

print "String slicing"
print word[0:2] # includes characters positions in the range [0,2)
print word[2:5]
print "String slices have useful defaults."
print word[3:] # from index 3 to end
print word[:2] # from start to index before 2
print word[-2:] # from second to last to end
print ""

print "String elements are immutable (they cannot be changed)"
word = "new word hello"
print word
# word[0] = 'f' # this results in an error
print ""

print "Get the string length with the len() function"
print len(word)
# Python supports unicode characters if desired
print ""

print "A list is a compound data type used to hold multiple data values. It can contain different data types, but it is typically used to hold one."
nums = [1, 3, 5, 8, 9]
print nums
print ""

print "All sequence types can be sliced => strings, lists, .."
print nums[0:3]
print "All slice operations perform a shallow copy to create a new list."
print ""

print "Lists supporst concatenation"
print nums+[1,8,7]
print ""

print "Unlike strings, lists are mutable."
nums[:3] = [7,4,6]
print nums
print ""

print ".append adds elements to the end"
nums.append(3**3)
print nums
print ""

words = ['a', 'b', 'c']
print "len(words) = ", len(words)
print ""

print "Nest listing"
a = [1,2,3]
b = [4,5,6]
c = ['a','b','v']
d = [a,b,c]
print d
print ""

print "Fibonacci Sequence"
a, b = 0, 1
while b < 10: # don't forget the colon!
    print b
    a,b = b,a+b # right hand operator expressions are evaluated first, starting from the left

# other comparison operators: >, <=, >=, !=

print "Fibonacci Sequence"
a, b = 0, 1
while b < 10:
    print b, # a trailing comma avoids the newline after the output
    a,b = b,a+b

print ""
print "Print out numbers on one line"
print 5,4,3,6, # need a trailing comma here to avoid newline after output
print 3,7,8,9
