#!/usr/bin/env python

# Push to the stack with append(), and pop from the queue with pop().
stack = [5,3,8,9,11,2]
print(stack)
stack.append(44)
stack.append(23)
print(stack)
stack.pop()
print(stack)
stack.pop()
print(stack)
stack.pop()
print(stack)
stack.pop()
print(stack)
