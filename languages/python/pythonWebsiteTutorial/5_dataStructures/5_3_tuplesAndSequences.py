#!/usr/bin/env python

# Tuples are immutable and usually contain a hetrogenous sequence of elements that are accessed via unpacking or indexing. Lists are mutable, and their elements are usually homogeneous and are accessed by iterating over the list.

print [4.0,5,'hh']

# Basic tuple
t = 1234, 'haha', 5.0, 'bla' # This statement is an example of tuple packing.
print t
print t[0]

# Nested tuples
u = t, 'haha'
print u

v = t, (5,6.0,'lala')
print v

# Tuples are immutable
#t[0] = 10 # This fails

# Tuples can contain mutable objects
d = ([1,2,3],'haha',5.0,[4,3,6])
print d

# Empty tuple
empty = ()
print empty

# Tuple with one element
oneElement = 'one', # <-- Note the trailing comma
print oneElement

# Sequence unpacking
a, b, var, e = t
print "a =",a
print "b =",b
print "var =",var
print "e =",e
