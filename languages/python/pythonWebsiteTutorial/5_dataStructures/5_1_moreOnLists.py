#!/usr/bin/env python

fruits = ['orange', 'pear', 'banana', 'peach', 'pineapple', 'peach', 'apple', 'orange']
print(fruits)
print(fruits.count('orange')) # 2
print(fruits.index('peach')) # 3
print(fruits.index('peach',4)) # 5
fruits.reverse()
print(fruits)
fruits.append('naartjie')
print(fruits)
fruits.sort()
print(fruits)
fruits.pop()
print(fruits)
