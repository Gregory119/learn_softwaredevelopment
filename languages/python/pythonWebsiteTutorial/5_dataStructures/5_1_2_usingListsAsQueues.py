#!/usr/bin/env python

from collections import deque

queue = deque(['Mom','Dad','Gran','Kyle','Joe'])
print(queue)
queue.append('Gary')
queue.append('John')
print(queue)
print(queue.popleft())
print(queue)
print(queue.popleft())
print(queue)
print(queue.popleft())
print(queue)
