#!/usr/bin/env python

print "=================================================="
print "Extracted from section 3.1.3 of the tutorial."
print "=================================================="
print ""

print "A list is a compound data type used to hold multiple data values. It can contain different data types, but it is typically used to hold one."
nums = [1, 3, 5, 8, 9]
print nums
print ""

print "All sequence types can be sliced => strings, lists, .."
print nums[0:3]
print "All slice operations perform a shallow copy to create a new list (the new copy internally holds references to the original elements)."
print ""

print "Example of a shallow copy."
ages=[1,4,6,7]
ages2=ages[1:]
print id(ages)==id(ages2) # This should be false because the objects are different.
print id(ages[3])==id(ages2[2]) # This should be true because of a shallow copy.
print ""

print "Lists support concatenation"
print nums+[1,8,7]
print ""

print "Unlike strings, lists are mutable."
nums[:3] = [7,4,6]
print nums
print ""

print ".append adds elements to the end"
nums.append(3**3)
print nums
print ""

print "Adding and clearing slices."
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
print letters
# replace some values
letters[2:5] = ['C', 'D', 'E']
print letters
# now remove them
letters[2:5] = []
print letters
# clear the list by replacing all the elements with an empty list
letters[:] = []
print letters
print ""

words = ['a', 'b', 'c']
print "len(words) = ", len(words)
print ""

print "Nest listing"
a = [1,2,3]
b = [4,5,6]
c = ['a','b','v']
d = [a,b,c]
print d
print ""

print "Fibonacci Sequence"
a, b = 0, 1
while b < 10: # don't forget the colon!
    print b
    a,b = b,a+b # right hand operator expressions are evaluated first, starting from the left

# other comparison operators: >, <=, >=, !=

print "Fibonacci Sequence"
a, b = 0, 1
while b < 10:
    print b, # a trailing comma avoids the newline after the output
    a,b = b,a+b

print ""
print "Print out numbers on one line"
print 5,4,3,6, # need a trailing comma here to avoid newline after output
print 3,7,8,9
