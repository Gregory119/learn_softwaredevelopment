#!/usr/bin/env python

# push C-c C-c to execute this module within emacs

# --- if statement ---
# if statements are used in place of switch-case statements in other languages, although there are workarounds to create the same control flow.
# x = int(raw_input("Please enter an integer: ")) # uncomment this to get input from the user
x = 4
if x < 0:
    print "number is less than zero"
elif x == 0:
    print "number is zero"
else:
    print "number is greater than zero"
print("")

# --- for statement ---
# iterates over the items of any sequence
words = ["haha","wordrwo","test"]
for w in words:
    print "The word "+w+" has a length of ",len(w)
print("")

# to modify the sequence being iterated over, it is recommended to iterate over a copy
i = 0
for w in words[:]:
    if w == "test":
        words.insert(i,"worked")
    i=i+1
print words
print("")

# --- range function ---
# this can be used for indices
print(range(15)) # 0 to 14
print(range(4,12,2))
print(range(4,-6,-2))

for i in range(len(words)):
    print words[i-1]
print("")

# --- break, continue, else with loops ---
# A break jumps out or exits the inner most loop (for/while).
# An else of a for loop executes after the last iteration; even when no iterations occur.
# The else does not execute if a break is called within the loop.
for i in range(5,5):
    print "test"
else:
    print "else"
print("")

# determine the prime numbers less than ten
for i in range(2,10):
    for j in range(2,i):
        if (i%j == 0):
            print i,"is divisible by",j
            break # current i is not a prime number, so move to next number
    else:
        # i is not divisible by any number less than it, other than one
        # thus it is a prime number
        print i,"is a prime number"
print("")

# An else of a while loop executes when the condition is/becomes false.
i = 0
while i < 5:
    print i
    i = i+1
else:
    print "i =",i
print("")


# The continue keyword is used to skip to the next loop iteration
for i in range(10):
    if (i == 5):
        print "skipping",i
        continue
    
    if (i%2 == 0): # if an even number
        print i,"is an even number"
print("")

# --- pass statement ---
# Used as a place-holder that does nothing to satisfy syntax requirements
# conditional body
i = 0
while i > 0:
    pass

# minimal class
class MyEmptyClass:
    pass

# function
def myFunc(*args):
    pass # STILL TO DO

# --- Defining Functions ---
# The execution of a function introduces a new symbol table used for
# the local variables of the function. More precisely, all variable
# assignments in a function store the value in the local symbol table;
# whereas variable references first look in the local symbol table,
# then in the local symbol tables of enclosing functions, then in the
# global symbol table, and finally in the table of built-in
# names. Thus, global variables and variables of enclosing functions
# cannot be directly assigned a value within a function (unless, for
# global variables, named in a global statement, or, for variables of
# enclosing functions, named in a nonlocal statement), although they
# may be referenced.

def fibonacci(n):
    """ Print a Fibonacci series up to n """ # This is a documentation string used for automatic documentation.
    a, b = 0, 1
    while a < n:
        print a,
        a,b = b,a+b
    print("")

fibonacci(20)

# Function parameters are passed by value, but are references.
# Functions can be assigned (reminds me of function pointers)
fib = fibonacci
fib(8)

# Returning a list from a function.
def retFib(n):
    """ Returns a list of the Fibonacci sequence up to n """
    result = []
    a, b = 0, 1
    while a < n:
        result.append(a) # more efficient than result = result + [a]
        a, b = b,a+b
    return result

nums = retFib(100)
print(nums)
print("")

# --- Default Function Parameter Values ---
print "Default Function Parameter Values"
def ask_ok(prompt, retries=4, reminder='Please try again!'):
    while True:
        ok = input(prompt) # Get input from standard input, but print
                           # the contents of the variable prompt
                           # first. Types will automatically be
                           # deduced, so strings should be in quotes.
        if ok in ('y', 'ye', 'yes'): # in keyword: tests whether or
                                     # not a sequence contains a
                                     # certain value
            return True
        if ok in ('n', 'no', 'nop', 'nope'):
            return False
        retries = retries - 1
        if retries < 0:
            raise ValueError('invalid user response')
        print(reminder)

# NB: The default values are evaluated at the point of function
# definition in the defining scope. If the default is a mutable object
# (list, dictionary, class object) then the object can be shared
# between function calls like (reminds me of a local static variable
# in C++):
def f(a, L=[]):
    if L==[]:
        print("L==[]")
    L.append(a)
    return L

print(f(3))
print(f(6))
print(f(1))

T = [0,1,2,3,4]
print(f(3,T))

print(f(2)) # This will still use the modified default, even though
            # the previous function call did not use a default.
print("")

# f() Re-written to not share the default between calls (to be more like C++ behaviour):
def f(a, L=None): # the keyword 'None'
    if L is None: # the keyword 'is'
        print("L is None")
        L = []
        
    if L==[]:
        print("L==[]") # this can be used to check for an empty list
    
    L.append(a)
    return L

print(f(1))
print(f(9))
print(f(8))
print("")

print("Function Keyword Arguments")
def parrot(voltage, state='a stiff', action='voom', type='Norwegian Blue'):
    print("-- This parrot wouldn't", action, ' ')
    print("if you put", voltage, "volts through it.")
    print("-- Lovely plumage, the", type)
    print("-- It's", state, "!")

parrot(1000)                                          # 1 positional argument
print("")
parrot(voltage=1000)                                  # 1 keyword argument
print("")
parrot(voltage=1000000, action='VOOOOOM')             # 2 keyword arguments
print("")
parrot(action='VOOOOOM', voltage=1000000)             # 2 keyword arguments
print("")
parrot('a million', 'bereft of life', 'jump')         # 3 positional arguments
print("")
parrot('a thousand', state='pushing up the daisies')  # 1 positional, 1 keyword
print("")

# The following will not work
#parrot()                     # required argument missing
#parrot(voltage=5.0, 'dead')  # non-keyword argument after a keyword argument
#parrot(110, voltage=220)     # duplicate value for the same argument
#parrot(actor='John Cleese')  # unknown keyword argument

def cheeseshop(kind, *arguments, **keywords):
    print("-- Do you have any", kind, "?")
    print("-- I'm sorry, we're all out of", kind)
    for arg in arguments:
        print(arg)
    print("-" * 40)
    for kw in keywords:
        print(kw, ":", keywords[kw])

cheeseshop("Limburger", "It's very runny, sir.",
           "It's really very, VERY runny, sir.",
           shopkeeper="Michael Palin",
           client="John Cleese",
           sketch="Cheese Shop Sketch")
print("")

print "Arbitrary Function Argument List"
# This reminds me of variadic function parameters in C++

# The last argument is the *args parameter
def printArgs(test, *args):
    print test
    print args

# keyword-only type formal arguments after the *args parameter (not supported in python 2.7)
#def printArgs2(*args, word="/"): 
#    return word.join(args)

printArgs("hel","to","the")
#printArgs2("this","is","the","other","version")

# See the rest of the online tutorial.
