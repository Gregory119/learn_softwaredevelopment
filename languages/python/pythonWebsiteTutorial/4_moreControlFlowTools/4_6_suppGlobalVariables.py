#!/usr/bin/env python

# References:
# https://www.python-course.eu/python3_global_vs_local_variables.php

# While in many or most other programming languages variables are
# treated as global if not otherwise declared, Python deals with
# variables the other way around. They are local, if not otherwise
# declared. This is to enforce good programming practise.

# Read the section "Global and Local Variables in Functions".

print ("Automatically use a global scoped variable in a function's local scope.")

# The following function does not have the variable s declared and
# defined in its local symbol table.
def func():
    print(s)

s = "Hello Mr String"
func() # This will print out the global variable.
print ("")


print ("Defining and declaring a local function variable will cause the variable to appear in the local symbol table of the function. This variable will then be printed out.")

def func2():
    s = "Seriously?"
    print(s)

s = "Oh yeah!"
func2() # This will print out the local function variable.
print("")


print ("Trying to print out the global from within the function before defining the local function variable will generate an error, ensuring that the local variable name should be used.")
def func3():
    #print(s) # Uncomment this to generate the error.
    s = "What the?"
    print(s)

s = "YummY"
func3()
print("")


print ("Mark a local variable as global so that it can be accessed within the function.")
def func4():
    global g_words
    print (g_words) # Prints the global variable.
    g_words = "OK Sir!" # Modifies the global variable.
    print (g_words)

g_words = "Take that Fucker!!"
func4()
print g_words # This will be "OK Sir!"
print ("")


print ("The following example shows a wild combination of local and global variables and function parameters:")
def foo(x, y):
    global a
    a = 42
    x,y = y,x
    b = 33
    b = 17
    c = 100
    print(a,b,x,y)

a, b, x, y = 1, 15, 3,4 
foo(17, 4)
print(a, b, x, y)
print("")


print("Global variables in nested functions.")
def blobOut():
    x = 42 # This enclosed function variable cannot be modified by
           # using the global keyword in the nested function.
    def blobIn():
        global x
        x = 43 # This will not modify the x in the enclosed function
               # scope. Rather, it will create and modify an x variable
               # in the module scope.
    print ("Before calling blobIn(): x = {}".format(x)) # Will be 42
    blobIn()
    print ("After calling blobIn(): x = {}".format(x)) # Will be 42

blobOut()
print("x in main module is: {}".format(x)) # Will be 43
