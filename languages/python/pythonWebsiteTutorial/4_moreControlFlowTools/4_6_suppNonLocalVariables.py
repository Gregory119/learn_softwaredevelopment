#!/usr/bin/env python3

# References:
# https://www.python-course.eu/python3_global_vs_local_variables.php

# Note: Nonlocal variables are a python 3 feature.

print("Global variables in nested functions.")
def blobOut():
    x = 42 # This enclosed function variable cannot be modified by
           # using the global keyword in the nested function.
    def blobIn():
        global x
        x = 43 # This will not modify the x in the enclosed function
               # scope. Rather, it will create and modify an x variable
               # in the module scope.
    print ("Before calling blobIn(): x = {}".format(x)) # Will be 42
    blobIn()
    print ("After calling blobIn(): x = {}".format(x)) # Will be 42

blobOut()
print("x in main module is: {}".format(x)) # Will be 43
print("")


print("Modify enclosed function variable inside a nested function.")
def funcOut():
    x = 42 # This will be modified by the nested function because of
           # the use of the nonlocal keyword. Uncomment this line to
           # generate an error saying that there is no binding for
           # nonlocal variable x.
    def funcIn():
        nonlocal x
        x = 43
    print ("Before calling funcIn(): x = {}".format(x)) # Will be 42
    funcIn()
    print ("After calling funcIn(): x = {}".format(x)) # Will be 43

x = 4
funcOut()
print("x in main is {}".format(x)) # Will be 4
print("")


print("A nonlocal variable must be defined if the keyword is to be used.")
def bla():
    #nonlocal g # Uncomment this to generate an error. The nonlocal
               # keyword cannot be used to modify a variable in the
               # global scope.
    print("Hi")

g = 11
bla()
