% $Header$

\documentclass{beamer}

% This file is a solution template for:

% - Giving a talk on some subject.  - The talk is between 15min and 45min long.
% - Style is ornate.



% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under the terms
% of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified for your own
% needs. For this reason, if you use this file as a template and not
% specifically distribute it as part of a another package/program, I grant the
% extra permission to freely copy and modify this file as you see fit and even
% to delete this copyright notice.

% ======================================================================%
% image packages
% ======================================================================%
% (uncomment only if not included by default in memoir)
\usepackage{graphicx}
\DeclareGraphicsExtensions{.pdf,.png,.jpg} % image extensions in order of preference
\graphicspath{ {./images/} }

% ======================================================================%
% code block highlighting
% ======================================================================%
\usepackage{minted}

\mode<presentation> { \usetheme{Berkeley}
  % or ...
  \setbeamercovered{transparent}
  % or whatever (possibly just delete it)
}

\usepackage[english]{babel}
% or whatever

\usepackage[latin1]{inputenc}
% or whatever

\usepackage{times} \usepackage[T1]{fontenc}
% Or whatever. Note that the encoding and the font should match. If T1 does not
% look nice, try deleting the line with the fontenc.


\title[Short Paper Title] % (optional, use only with long paper titles)
{Computer Systems: A Programmer's Perspective}

\subtitle {Chapter 2} % (optional)

\author[Jones] % (optional, use only with lots of authors)
{G.Jones}
% - Use the \inst{?} command only if the authors have different affiliation.

\institute[Universities] % (optional, but mostly needed)
{
  Department of SelfStudy\\
  University of SelfStudy }
% - Use the \inst command only if there are several affiliations.  - Keep it
% simple, no one is interested in your street address.

\date[Short Occasion] % (optional)
{13 Jan 2020}

\subject{Talks}
% This is only inserted into the PDF information catalog. Can be left out.



% If you have a file called "university-logo-filename.xxx", where xxx is a
% graphic format that can be processed by latex or pdflatex, resp., then you can
% add a logo as follows:

% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}



% Delete this, if you do not want the table of contents to pop up at the
% beginning of each subsection:
\AtBeginSection[] {
  \begin{frame}<beamer>{Outline}
    \tableofcontents[currentsection]
  \end{frame}
}


% If you wish to uncover everything in a step-wise fashion, uncomment the
% following command:

% \beamerdefaultoverlayspecification{<+->}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
  % You might wish to add the option [pausesections]
\end{frame}


% Since this a solution template for a generic talk, very little can be said
% about how it should be structured. However, the talk length of between 15min
% and 45min and the theme suggest that you stick to the following rules:

% - Exactly two or three sections (other than the summary).  - At *most* three
% subsections per section.  - Talk about 30s to 2min per frame. So there should
% be between about 15 and 30 frames, all told.

\section{2.1 Information Storage}

\begin{frame}{Information Storage}
  \begin{itemize}
  \item Most computers use blocks of eight bits, or \underline{\textbf{bytes}},
    as the smallest addressable unit of memory.
  \item A machine level program views memory as a very large array of bytes,
    referred to as \underline{\textbf{virtual memory}}.
  \item Every byte of memory is identified by a unique number, known as its
    \textbf{address}, and the set of all possible addresses is known as the
    \underline{\textbf{virtual address space}}.
  \item The value of a pointer in C \textemdash whether it points to an integer,
    a structure, or some other program object \textemdash is the \textbf{virtual
      address of the first byte} of some block of storage.
  \item C compiler associates type information with each pointer to generate
    appropriate machine-level code (eg. size info).
  \end{itemize}
\end{frame}


\begin{frame}{2.1.1 Hexadecimal Notation}
  % - A title should summarize the slide in an understandable fashion
  % for anyone how does not follow everything on the slide itself.

  \begin{itemize}
  \item Bit patterns are typically shown as Base-16 (0 to F) numbers.
  \item Any byte can be represented as 0x00 to 0xFF (Or 0X..).
  \item A to F can be written in lowercase or a mix.
  \item Trick for mental conversions to decimal: memorize binary equivalents of
    0xA (0x1010), 0xC (0x1100), and 0xF (0x1111). B, D, and E can be converted
    relative to the first three.
  \item Binary to Hex: Group bits of 4 (might need zero padding to the left).
  \item Dec to Hex: (mental) Dec to Binary to Hex. (calc) Repeatedly divide
    decimal x by 16. Convert remainder of each division to hex.
  \end{itemize}
  
\end{frame}


\begin{frame}{2.1.2 Words}
  \begin{itemize}
  \item Every computer has a \underline{\textbf{word size}} (w), indicating the
    nominal size of integer and pointer data.
  \item Virtual address space is determined by word size. Addresses will range
    from 0 to $(2^w - 1)$.
  \item 32\textendash bit machines have $2^{32} = 2^{30} \cdot 2^{2} = 4 GB$ of
    addresses.
  \item 64\textendash bit machines have $2^{64} = 2^{60} \cdot 2^4 = 16 EB$ of
    addresses.
  \item
    $2^{10} = 1 KB, 2^{20} = 1 MB, 2^{30} = 1 GB, 2^{40} = 1 TB, 2^{50} = 1 PB$
    (\textbf{Petabyte}), $2^{60} = 1 EB$ (\textbf{Exabyte}), $2^{70} = 1 ZB$
    (\textbf{Zetabyte}), $2^{80} = 1 YB$ (\textbf{Yottabyte}).
    
  \end{itemize}
\end{frame}

\begin{frame}{2.1.3 Data Sizes}
  \begin{figure}
    \includegraphics[width=\textwidth]{c-data-sizes}
  \end{figure}
\end{frame}

\begin{frame}{2.1.3 Data Sizes (cont.)}
  \begin{figure}
    \includegraphics[width=0.60\textwidth]{cpp-data-sizes}
    \includegraphics[width=0.39\textwidth]{cpp-machine-types}
  \end{figure}
\end{frame}

\begin{frame}{2.1.3 Data Sizes (cont.)}
  \begin{itemize}
  \item The exact number depends on both the \textbf{machine} and the
    \textbf{compiler}.
  \item char* (a pointer) uses the \textbf{full word size} of the machine.
  \item Programmers should strive to make their programs \textbf{portable}
    across different machines and compilers. Eg. Make the program insensitive to
    the exact sizes of the different data types.
  \item Many programmers assume that a program object declared as type int can
    be used to store a pointer (\textbf{problem} on 64-bit).
  \end{itemize}
\end{frame}
\begin{frame}{2.1.4 Addressing and Byte Ordering}
  \begin{itemize}
  \item For program objects that span multiple bytes, we must establish the
    \textbf{address} of the object and the \textbf{order of the bytes} in memory
  \item A multi-byte object is stored as a \textbf{contiguous} sequence of
    bytes, with the address of the object given by the \textbf{smallest address}
    of the bytes used.
  \item Eg. A variable x of type int has address 0x100. The 4 bytes of x would
    be stored in memory locations 0x100, 0x101, 0x102, and 0x103.
  \item \underline{\textbf{Little endian}} - least significant byte comes first
    (at the lower address).
  \item \underline{\textbf{Big endian}} - most significant byte comes first (at
    the lower address).
  \item Processors are either one (typical) or both
    (\underline{\textbf{bi-endian}}).
  \end{itemize}
\end{frame}

\begin{frame}{2.1.4 Addressing and Byte Ordering (cont.)}

  \begin{itemize}
  \item Suppose the variable x of type int and at address 0x100 has a
    hexadecimal value of 0x01234567.
  \end{itemize}

  \begin{figure}
    \includegraphics[width=0.5\textwidth]{endianess}
  \end{figure}
  
  \begin{itemize}
  \item Typically byte ordering is invisible; programs compiled for either class
    of machine give identical results.
  \item Byte ordering is an \textbf{issue} for \textbf{network communication}
    (sending and receiving machines must convert to/from the network standard)
    and \textbf{viewing byte sequences of data} (machine-level code or printing
    bytes of objects).
  \item See Figure 2.4 (pg 42), 2.5, and 2.6 for an example of printing object
    bytes.
  \item Use \textbf{sizeof(..)} for portability.
  \end{itemize}
  
\end{frame}

\begin{frame}{2.1.5 Representing Strings}
  \begin{itemize}
  \item A string in C is encoded by an array of characters terminated by the
    null (having value 0) character.
  \item Each character is represented by some standard encoding, with the most
    common being ASCII.
  \item show\_bytes("12345", 6) gives 31 32 33 34 35 00 across any system using
    ASCII (more platform independent than binary).
  \item The unicode base encoding (eg. UTF\textendash7, UTF\textendash8,
    UTF\textendash16, UTF\textendash32) uses multiple bytes to represent
    characters in other languages.
  \item UTF\textendash8 is compatible with ASCII (first 128 characters) and uses
    up to four bytes.
  \end{itemize}
\end{frame}

\begin{frame}{2.1.6 Representing Code}
  \begin{figure}
    \includegraphics[width=0.55\textwidth]{platform-test-types}
  \end{figure}

  \begin{itemize}
  \item The same C code as machine-level code (assuming same compiler):
  \end{itemize}
  \begin{figure}
    \includegraphics[width=0.55\textwidth]{platform-machine-code}
  \end{figure}
  \begin{itemize}
  \item Different machine types use different and incompatible instructions and
    encodings.
  \item Binary code is seldom portable across different combinations of machine
    and operating system (even OSs have differences in their coding conventions
    - not binary compatible).
  \end{itemize}
\end{frame}

\begin{frame}{2.1.7 Introduction to Boolean Algebra}
  \begin{figure}
    \includegraphics[width=0.8\textwidth]{operations-on-boolean-algebra}
  \end{figure}
  \begin{itemize}
  \item $\neg P \Rightarrow$ NOT, $P \wedge Q \Rightarrow P$ AND $Q$,
    $P \vee Q \Rightarrow P$ OR $Q$, $P \oplus Q \Rightarrow P$ XOR $Q$
  \item Boolean algebra has many of the same properties as arithmetic over
    integers.
  \item For example, just as multiplication distributes over addition, written
    $a \cdot (b + c) = (a \cdot b) + (a \cdot c)$, Boolean operation \textbf{\&
      distributes over |}, written
    $a \,\&\, (b \,|\, c) = (a \,\&\, b) \,|\, (a \,\&\, c)$.
  \end{itemize}
\end{frame}

\begin{frame}{2.1.7 Introduction to Boolean Algebra (cont.)}
  \begin{itemize}
  \item In addition, Boolean operation \textbf{| distributes over \&}, and so we
    can write $a \,|\, (b \,\&\, c) = (a \,|\, b) \,\&\, (a \,|\, c)$, whereas
    we cannot say that $a + (b \cdot c) = (a + b) \cdot (a + c)$ holds for all
    integers.
  \item Operations \textasciicircum, \&, and \textasciitilde on bit vectors of
    length w, we get a mathematical form known as a \underline{\textbf{Boolean
        ring}}, which have many properties of integer arithmetic.
  \item Eg. Every integer x has an \textbf{additive inverse} $-x$, such that
    $x + -x = 0$. A similar property holds for Boolean rings, where
    \textbf{\textasciicircum\, is the "addition"} operation, but in this case
    \textbf{each element} is its own additive inverse.
  \item Specifically, $a \textasciicircum a = 0$ for any value $a$, where we use
    0 here to represent a \textbf{bit vector of all zeros}.
  \end{itemize}
\end{frame}

\begin{frame}{2.1.7 Introduction to Boolean Algebra (cont.)}
  \begin{itemize}
  \item This holds for single bits since
    $0 \textasciicircum 0 = 1 \textasciicircum 1 = 0$ and extends to bit
    vectors. Even holds with rearranged terms
    $(a \textasciicircum b) \textasciicircum a = b$.
  \item Bit vectors can be used to represent finite sets.
  \item Any subset $A \subseteq \{0, 1, . . . , w - 1\}$ can be encoded with a
    bit vector $[a_{w - 1}, . . . , a_1, a_0]$, where $a_i = 1$ iff
    $i \subseteq A$.
  \item Eg. Bit vector $a = [01101001]$ encodes the set $A = \{0, 3, 5, 6\}$.
  \item With this way of encoding sets, Boolean operations | and \& correspond
    to \textbf{set union} and \textbf{intersection}, respectively, and
    \textbf{\textasciitilde} corresponds to \textbf{set complement}.
  \item Practice problem 2.10 (pg 51) shows how to swap integers using
    \textbf{\textasciicircum}.
  \end{itemize}
\end{frame}

\begin{frame}{2.1.8 Bit-Level Operations in C}
  \begin{figure}
    \includegraphics[width=0.6\textwidth]{c-bit-operations}
  \end{figure}
  \begin{itemize}
  \item The best way to determine the effect of a bit-level expression is to
    expand the hexadecimal arguments to their binary representations, perform
    the operations in binary, and then convert back to hexadecimal.
  \item Bit-level operations are used for \textbf{masking}. A \textbf{mask} is a
    bit pattern that indicates a selected set of bits within a word.
  \item The bit-level operation \textbf{x \& 0xFF} yields a value consisting of
    the least significant byte of x, but with all other bytes set to 0.
  \item The expression \textbf{\textasciitilde0} is a \textbf{portable} way of
    yielding a mask of all ones.
  \item See Practice problem 2.12 and 2.13.
  \end{itemize}
\end{frame}

\begin{frame}{2.1.9 Logical Operations in C}
  \begin{itemize}
  \item C also provides a set of \textbf{logical operators} ||, \&\&, and !.
  \item The logical operations treat any \textbf{nonzero argument} as
    representing True and argument 0 as representing False.
  \item Logical operators do not evaluate their second argument if the result of
    the expression can be determined by evaluating the first argument.
  \item Eg. a \&\& 5/a will never cause a division by zero, and p \&\& *p++ will
    never cause the de-referencing of a null pointer.
  \end{itemize}
  \begin{figure}
    \includegraphics[width=0.3\textwidth]{c-logical-operations}
  \end{figure}
\end{frame}

\begin{frame}{2.1.10 Shift Operations in C}
  \begin{itemize}
  \item x <{}< k shifts x to the \textbf{left by k bits}, dropping k significant
    bits and filling k \textbf{zeros} in the right end.
  \item Shift operations associate from \textbf{left to right}, so x <{}< j <{}<
    k is equivalent to (x <{}< j) <{}< k
  \item Right shift operation x >{}> k is either \textbf{logical} or
    \textbf{arithmetic}.
  \item A \textbf{logical right shift} fills the left end with k zeros, an
    arithmetic right shift fills the left end with k repetitions of the
    \textbf{most significant bit}.
  \end{itemize}
  \begin{figure}
    \includegraphics[width=0.6\textwidth]{c-shift-operations}
  \end{figure}
\end{frame}

\begin{frame}{2.1.10 Shift Operations in C (cont.)}
  \begin{itemize}
  \item The C right shifts on unsigned variables must be logical, but
    \textbf{logical or arithmetic} could be used for \textbf{signed} variables.
  \item C++ also uses logical for positive signed variables \textendash\, see
    \href{https://en.cppreference.com/w/cpp/language/operator_arithmetic}{\color{blue}{\underline{this}}}
    for details.
  \item Almost all compiler/machine combinations use arithmetic right shifts for
    signed data, and many programmers assume this to be the case.
  \item C does not define how to handle shifts of $k \geq w$, but machines
    typically shift by $k$ mod $w \Rightarrow$ rule: \textbf{keep shifts less
      than the word size}.
  \item Use parentheses in expressions with multiple shifts to avoid precedence
    problems.
  \end{itemize}
\end{frame}

\section{2.2 Integer Representations}

\begin{frame}{2.2.1 Integer Representations}
  \begin{figure}
    \includegraphics[width=0.9\textwidth]{typical-int-range-32-bit-machine}
  \end{figure}
  \begin{itemize}
  \item Notice that the maximum and minimum values are asymmetric.
  \end{itemize}
\end{frame}

\begin{frame}{2.2.1 Integer Representations (cont.)}
  \begin{figure}
    \includegraphics[width=0.9\textwidth]{typical-int-range-64-bit-machine}
  \end{figure}
\end{frame}

\begin{frame}{2.2.1 Integer Representations (cont.)}
  \begin{figure}
    \includegraphics[width=0.9\textwidth]{min-c-int-range-png}
  \end{figure}
  \begin{itemize}
  \item We see that they require only a symmetric range of positive and negative
    numbers.
  \end{itemize}
\end{frame}

\begin{frame}{2.2.2 Unsigned Encodings}
  \begin{itemize}
  \item $\vec{x} = [x_{w-1}, x_{w-2}, . . . , x_0]$ denotes a bit vector with
    $w$ bits.
  \item The unsigned interpretation of $\vec{x}$ is given by the
    binary-to-unsigned equation
  \end{itemize}
  \begin{equation}
    {B2U}_w(\vec{x})=\sum_{i=0}^{w-1} x_i 2^i \tag{2.1}
  \end{equation}
  \begin{itemize}
  \item $B2U_w([00...0]) = \textbf{UMin} = 0$.
  \item $B2U_w([11...1]) = \textbf{UMax} = \sum_{i=0}^{w-1} 2^i = 2^w - 1$.
  \item $B2U_w : \{0, 1\}^w \rightarrow \{0, 1,..., 2^w - 1\}$ is a
    \textbf{bijection}.
  \end{itemize}
  \begin{figure}
    \includegraphics[width=0.5\textwidth]{unsigned-example}
  \end{figure}
\end{frame}

\begin{frame}{2.2.3 Two's-Complement Encodings}
  \begin{itemize}
  \item This is the most common representation of signed numbers given by the
    binary-to-two's-complement equation
  \end{itemize}
  \begin{equation}
    {B2T}_w(\vec{x})=-x_{w-1} 2^{w-1} + \sum_{i=0}^{w-2} x_i 2^i \tag{2.3}
  \end{equation}
  \begin{itemize}
  \item The MSB (Most Significant Bit) $x_{w-1}$, also called the \textbf{signed
      bit}, has negative weight $-2^{w-1}$.
  \item If the MSB is 1, the value is negative. If the MSB is 0, the value is
    positive.
  \end{itemize}
  \begin{figure}
    \includegraphics[width=0.5\textwidth]{twos-complement-example}
  \end{figure}
\end{frame}

\begin{frame}{2.2.3 Two's-Complement Encodings (cont.)}
  \begin{itemize}
  \item $B2T_w(\vec{x}=[10...0]) = \textbf{TMin} = -2^{w-1}$.
  \item
    $B2T_w(\vec{x}=[01...1]) = \textbf{TMax} = \sum_{i=0}^{w-2}2^i = 2^{w-1}-1$.
  \item $B2T_w : \{0, 1\}^w \rightarrow \{-2^{w-1},...,2^{w-1}-1\}$ is a
    \textbf{bijection}.
  \item This range is asymmetric: $|TMin| = |TMax| + 1$ (source of bugs).
  \end{itemize}
  \begin{figure}
    \includegraphics[width=0.75\textwidth]{important-numbers}
  \end{figure}
\end{frame}

\begin{frame}{2.2.3 Two's-Complement Encodings (cont.)}
  \begin{itemize}
  \item Zero is represented with a sign bit of 0, causing \textbf{one less
      positive number} representation.
  \item Modern machines typically use two's complement, but older ones have used
    ones' complement.
  \item Strictly portable code will \textbf{not assume} a specific signed number
    encoding or non-guaranteed value range.
  \item Fixed size data types should be used for portability (see stdint.h).
  \end{itemize}
\end{frame}

\begin{frame}{2.2.3 Two's-Complement Encodings (cont.)}
  \begin{itemize}
  \item \textbf{Ones' Complement} has a sign bit weight of $-(2^{w-1}-1)$ and
    gives (binary-to-ones'-complement)
  \end{itemize}
  
  \begin{equation}
    B20_w(\vec{x}) = - x_{w-1} (2^{w-1} - 1) + \sum_{i=0}^{w-2} x_i 2^i
  \end{equation}
  
  \begin{itemize}
  \item $B2O_w([10...0]) = \textbf{OMin} = -(2^{w-1} - 1)$
  \item $B2O_w([01...1]) = \textbf{OMax} = 2^{w-1} - 1$
  \item -0 is represented by $[11...1]$.
  \item +0 by $[00...0]$ (zero for 2's complement).
  \item +1 is represented by $[00...1]$ (same for 2's complement).
  \item -1 is represented by $[11...10]$ ($[11...1]$ for 2's complement).
  \end{itemize}
\end{frame}

\begin{frame}{2.2.3 Two's-Complement Encodings (cont.)}
  \begin{itemize}
  \item The MSB for \textbf{Sign-Magnitude} only determines the sign (it has no
    weight) of the number represented by the remaining bits.
  \end{itemize}
  \begin{equation}
    \textit{binary-to-sign-magnitude:}\,\, B2S_w(\vec{x}) = x_{w-1} (-1) \sum_{i=0}^{w-2} x_i 2^i
  \end{equation}

  \begin{itemize}
  \item $B2S_w([11...1]) = \textbf{SMin} = -(2^{w-1}-1)$
  \item $B2S_w([01...1]) = \textbf{SMax} = 2^{w-1}-1$
  \item +0 is represented by $[00...0]$.
  \item -0 is represented by $[10...0]$.
  \item +1 is represented by $[00...1]$.
  \item -1 is represented by $[10...1]$.
  \end{itemize}
\end{frame}

\begin{frame}{2.2.3 Two's-Complement Encodings (cont.)}
  \begin{figure}
    \includegraphics[width=0.6\textwidth]{twos-complement-representations}
  \end{figure}
 
  \begin{itemize}
  \item Do Practice Problem 2.18.
  \end{itemize}
\end{frame}

\begin{frame}{2.2.4 Conversion Between Signed and Unsigned}
  \begin{itemize}
  \item Based on a bit level perspective rather than a numeric one.
  \item Casting between numbers of the same word size keeps the bit values
    identical but changes how these bits are interpreted.
  \item The letter 'u' or 'U' can be used to specify that a number should be
    interpreted as an unsigned. eg. \mintinline{c}{unsigned a = 5u;}
  \item Since both $B2U_w(\vec{x})$ and $B2T_w(\vec{x})$ are bijections for bit
    pattern $\vec{x}$, they are invertible. Let $U2B_w(u)=B2U_w^{-1}(\vec{x})$,
    where $u$ is unsigned, and $T2B_w(s)=B2T_w^{-1}(\vec{x})$, where $s$ is
    signed.
  \end{itemize}
\end{frame}

\begin{frame}{2.2.4 Conversion Between Signed and Unsigned}
  \begin{itemize}
  \item Derivation of the function $T2U_w(x)$. We know $T2U_w(x)=B2U_w(T2B_w(x))$.
  \item Using (2.1) and (2.3): $B2U_w(\vec{x})-B2T_w(\vec{x})=\sum_{i=0}^{w-1} x_i 2^i-(-x_{w-1} 2^{w-1} + \sum_{i=0}^{w-2} x_i 2^i)$
  \item The first and second terms cancel giving $B2U_w(\vec{x})-B2T_w(\vec{x})=x_{w-1} 2^{w-1}$
  \item Letting $\vec{x}=T2B_w(x)$ in the last step gives $T2U_w(x)=B2U_w(T2B_w(x))=x_{w-1} 2^{w-1}+x$.
  \item Finally, with bit $x_{w-1}$ determining the sign of $x$ we have
  \end{itemize}
  \begin{equation} \label{eq:T2U}
      T2U_w(x)=
      \begin{cases}
        x+2^w, & x< 0 \\
        x, & x\geq 0
      \end{cases}
    \end{equation}
\end{frame}

\begin{frame}{2.2.4 Conversion Between Signed and Unsigned Cont.}
  \begin{itemize}
  \item The following figure visually represents equation \ref{eq:T2U}.
  \item Negative numbers represented with two's complement are converted to
    unsigned numbers by adding $2^w$, whereas positive numbers remain
    unchanged.
  \end{itemize}
  \begin{figure}
    \includegraphics[width=\textwidth]{two's-complement-to-unsigned}
  \end{figure}
\end{frame}

\begin{frame}{2.2.4 Conversion Between Signed and Unsigned Cont.}
  \begin{itemize}
  \item The function for converting an unsigned number to a two's complement
    number can be derived in a similar way (see pg 68) giving
  \end{itemize}
  \begin{equation}
    U2T_w(u)=
    \begin{cases}
      u, u< 2^{w-1} \\
      u-2^w, u\geq 2^w
    \end{cases}
  \end{equation}
  \begin{itemize}
  \item Unsigned numbers $\geq 2^{w-1}$ are converted to a
    two's complement number by subtracting $2^w$, whereas unsigned values $<2^{w-1}$ remain unchanged.
  \end{itemize}
  \begin{figure}
    \includegraphics[width=0.9\textwidth]{unsigned-to-two's-complement}
  \end{figure}
\end{frame}

\begin{frame}{2.2.5 Signed vs. Unsigned in C}
  \begin{itemize}
  \item C does not specify a particular representation of signed numbers
    although \textbf{most are two's complement}.
  \item Numbers that do not include 'u' or 'U' are considered \textbf{signed by default}
    (eg. 2345, 0xAB31).
  \item C performs unsigned <-> signed conversion using the functions $U2T_w$
    and $T2U_w$ (underlying bit representation does not change) where $w$ is the
    number of bits for the data type.
  \item Casting can be \textbf{explicit} or \textbf{implicit}.
  \item \textbf{\textit{printf()}} does not use type information and so a signed
    number can be printed as an unsigned and vice-versa.
  \item When an \textbf{operation} is performed between two operands where the
    \textbf{one is signed and the other is unsigned}, the signed number will be
    implicitly cast to an unsigned type followed by performing the operations as
    though the numbers are both non-negative.
  \end{itemize}
\end{frame}

\begin{frame}{2.2.5 Signed vs. Unsigned in C}
  \begin{itemize}
  \item The last point causes unexpected results for relational operators
    ($<,\leq,>,\geq$) between signed and unsigned numbers, but not much
    difference for arithmetic operations.
  \item eg. $-1 < 0U$ => $4294967295U < 0U$ => false (recall
    $T2U_w(-1)=UMax_w$). See practice problem 2.21.
  \end{itemize}
  \begin{figure}
    \includegraphics[width=0.9\textwidth]{effects-of-c-promotion-rules}
  \end{figure}
\end{frame}

\defverbatim[colored]\codeRelativeConversionOrder{
\begin{minted}{c}
short sx = -12345; /* -12345 */
unsigned uy = sx; /* Mystery! */
\end{minted}
}

\begin{frame}{2.2.6 Expanding the Bit Representation of a Number}
  \begin{itemize}
  \item Converting an integer to use a larger integer data type is always
    possible, however converting to a smaller integer data type is not always
    possible (there could be overflow - discussed later).
  \item \textbf{\textit{Zero extension}} is used to convert an unsigned number to a larger
    unsigned data type - new higher significant bits are set to zero.
  \item \textbf{\textit{Sign extension}} is used to convert a signed number to a larger
    signed data type - new higher significant bits are set to one.
  \item This causes the bit representation of a number as an unsigned and signed
    number are \textbf{not the same after a conversion to a larger data type}.
  \end{itemize}
\end{frame}

\begin{frame}{2.2.6 Expanding the Bit Representation of a Number Cont.}
  \begin{itemize}
  \item The \textbf{relative order of conversion} from one type to another matters. eg.
  \end{itemize}
  \codeRelativeConversionOrder
  \begin{itemize}
  \item The \textbf{size is changed first} followed by the signed to unsigned
    conversion. eg. \mintinline{c}{(unsigned)(int)sx = 4,294,954,951}
  \item Do practice problem 2.23.
  \end{itemize}
\end{frame}

\begin{frame}{2.2.7 Truncating Numbers}
  \begin{itemize}
  \item Converting an \mintinline{c}{int} to a smaller data type like \mintinline{c}{short}
    will cause truncation - the number of bits representing the number are
    reduced.
  \item Truncating a $w$-bit number $\vec{x}=[x_{w-1},x_{w-2},...,x_0]$ to a
    k-bit number involves dropping the higher order $w-k$ bits. This bit-level
    operation applies to signed and unsigned numbers.
  \item This operation can alter the value of a number - a form of overflow.
  \item For an unsigned number $x$ the result is equivalent to $x\,mod\,2^k$,
    which can be applied to equation 2.1 (see pg 75) to give
  \end{itemize}
  \begin{equation}
    B2U_k([x_{k-1},x_{k-2},...,x_0])=B2U_w([x_{k-1},x_{k-2},...,x_0])\,mod\,2^k \tag{2.9}
  \end{equation}
\end{frame}

\begin{frame}{2.2.7 Truncating Numbers Cont.}
  \begin{itemize}
  \item It can be shown that for a \textbf{signed} number $x$
    $B2T_w([x_{w-1},x_{w-2},...,x_0])\,mod\,2^k=B2U_k([x_{k-1},x_{k-2},...,x_0])$. In
    other words, $x\,mod\,2^k$ is equal to the unsigned number representing
    $[x_{k-1},x_{k-2},...,x_0]$.
  \item Signed number truncation is then given by
  \end{itemize}
  \begin{gather*}
      B2T_k([x_{k-1},x_{k-2},...,x_0]) \\
      = U2T_k(B2U_w([x_{w-1},x_{w-2},...,x_0])\,mod\,2^k) \\
      = U2T_k(B2U_k([x_{k-1},x_{k-2},...,x_0]))
      \tag{2.10}
  \end{gather*}
\end{frame}

\begin{frame}{2.2.8 Advice on Signed vs. Unsigned}
  Knowing the nuances with converting between signed and unsigned numbers,
  reasons for using unsigned numbers include:
  \begin{itemize}
  \item Unsigned values are useful for thinking of words as collection of bits
    with no numeric intepretation (useful for holding multiple boolean
    \textit{flags}).
  \item Addresses are naturally unsigned (useful for system programmers).
  \item Mathematical packages represent numbers by arrays of words for modular
    and multiprecision arithmetic.
  \end{itemize}
\end{frame}

\section{2.3 Integer Arithmetic}
\begin{frame}{2.3.1 Unsigned Addition}
  \begin{itemize}
  \item 
  \end{itemize}
\end{frame}

\begin{frame}{2.3.2 Two's-Complement Addition}
  \begin{itemize}
  \item 
  \end{itemize}
\end{frame}
\begin{frame}{2.3.3 Two's-Complement Negation}
  \begin{itemize}
  \item 
  \end{itemize}
\end{frame}
\begin{frame}{2.3.4 Unsigned Multiplication}
  \begin{itemize}
  \item 
  \end{itemize}
\end{frame}
\begin{frame}{2.3.5 Two's Complement Multiplication}
  \begin{itemize}
  \item 
  \end{itemize}
\end{frame}
\begin{frame}{2.3.6 Multiplying by Constants}
  \begin{itemize}
  \item 
  \end{itemize}
\end{frame}
\begin{frame}{2.3.7 Dividing by Powers of 2}
  \begin{itemize}
  \item 
  \end{itemize}
\end{frame}


\section{2.4 Floating Point}

\end{document}

%%% Local Variables:
%%% LaTeX-command: "latex -shell-escape"
%%% TeX-master: t
%%% End:
