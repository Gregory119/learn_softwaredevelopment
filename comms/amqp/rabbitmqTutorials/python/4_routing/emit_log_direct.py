#!/usr/bin/env python

# Description: Publish (send) messages to an exchange 'direct_logs'
# with type 'direct'. Consumers use direct matching routing keys to
# process messages.

import pika
import sys

# Create a connection to the 'local host'
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Create an exchange (if it doesn't already exist) called 'direct_logs'.
channel.exchange_declare(exchange='direct_logs', exchange_type='direct')

# Severity is the first argument, which is either 'info','warning', or 'error'.
severity = sys.argv[1] if len(sys.argv) > 1 else 'info'
# Other arguments are the message contents.
message = ' '.join(sys.argv[2:]) or "Hello World!"

# Send the message to the specified exchange.
channel.basic_publish(exchange='direct_logs',
                      # The message is sent to the exchange, without
                      # knowing the destination queue (in constrast to
                      # the first two tutorials.
                      routing_key=severity, 
                      body=message)
print("[x] Sent: {}:{}".format(severity,message))

# Make sure the network buffers are flushed and that the message is
# delivered to the RabbitMQ server by gently closing the connection.
connection.close()
