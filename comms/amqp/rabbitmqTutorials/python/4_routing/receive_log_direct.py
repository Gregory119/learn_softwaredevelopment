#!/usr/bin/env python

# Description: Receive messages from the 'direct_logs' exchange and
# print them. These messages have been published to the exchange, by
# some other producer, which broadcasts them. The queue in this
# program is bound to the 'direct_log' exchange with a routing key,
# which is used to only accept messages that have an exactly matching
# routing key.

import pika
import sys

# Create a connection to the 'local host'
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Create an exchange (if it doesn't already exist) called
# 'direct_logs' for direct filtering.
channel.exchange_declare(exchange='direct_logs', exchange_type='direct')

# Create a queue with a random name. The exlusive flag tells RabbitMq
# to delete the queue when the connection is closed.
result = channel.queue_declare('',exclusive=True)
queue_name = result.method.queue

severities = sys.argv[1:]
if not severities:
    sys.stderr.write("Usage: {} [info] [warning] [error]".format(sys.argv[0]))
    sys.exit(1)

# Bind the queue to all of the severities specified at the command line.
for severity in severities:
    channel.queue_bind(
        exchange='direct_logs', queue=queue_name, routing_key=severity)
    

# Create callback to handle each queue message.
def callback(ch, method, properties, body):
    print(" [x] Received: {}:{}".format(method.routing_key, body))

# Register this callback with RabbitMQ for the queue we generated earlier.
channel.basic_consume(queue=queue_name,
                      # Disable the manual message acknowledgements.
                      auto_ack=True,
                      on_message_callback=callback)

# Enter a never ending loop that waits for data and runs callbacks
# whenever necessary.
print(" [*] Waiting for messages. To exit press CTRL+C")
channel.start_consuming()
