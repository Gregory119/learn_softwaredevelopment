#!/usr/bin/env python

# Description: Receive messages from the queue and print them on the screen.

import pika

# Create a connection to the 'local host'
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Create a recipient queue if it doesn't exist already.
channel.queue_declare(queue='hello')

# Create callback to handle each queue message
def callback(ch, method, properties, body):
    print(" [x] Received: {}".format(body))

# Register this callback with RabbitMQ for the specified queue.  This
# command will only succeed if the queue we want to subscribe exists,
# which we have already made sure of with 'queue_declare'.
channel.basic_consume(queue='hello',
                      auto_ack=True,
                      on_message_callback=callback)

# Enter a never ending loop that waits for data and runs callbacks
# whenever necessary.
print(" [*] Waiting for messages. To exit press CTRL+C")
channel.start_consuming()
