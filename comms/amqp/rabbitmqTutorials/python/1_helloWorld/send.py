#!/usr/bin/env python

# Description: Send messages to a RabbitMQ queue.

import pika

# Create a connection to the 'local host'
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Create a recipient queue if it doesn't exist already.
channel.queue_declare(queue='hello')

# Send the message through the default exchange to the specific queue.
channel.basic_publish(exchange='', # Use the default exchange. Allows
                                   # specifying the destination queue
                                   # in the routing key.
                      routing_key='hello',
                      body='Hello World!')
print("[x] Sent 'Hello World'")

# Make sure the network buffers are flushed and that the message is
# delivered to the RabbitMQ by gently closing the connection.
connection.close()
