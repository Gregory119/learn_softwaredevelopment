#!/usr/bin/env python

# Description: Publish (send) messages to an exchange 'topic_logs'
# with type 'topic'. Consumers use topic based routing keys to match
# process messages.

import pika
import sys

# Create a connection to the 'local host'
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Create an exchange (if it doesn't already exist) called 'topic_logs'.
channel.exchange_declare(exchange='topic_logs', exchange_type='topic')

# Routing key is in the format <facility>.<severity>
routing_key = sys.argv[1] if len(sys.argv) > 2 else 'anonymous.info'
# Other arguments are the message contents.
message = ' '.join(sys.argv[2:]) or "Hello World!"

# Send the message through the default exchange to the specific queue.
channel.basic_publish(exchange='topic_logs',
                      # The message is sent to the exchange, without
                      # knowing the destination queue (in constrast to
                      # the first two tutorials.
                      routing_key=routing_key, 
                      body=message)
print("[x] Sent: {}:{}".format(routing_key,message))

# Make sure the network buffers are flushed and that the message is
# delivered to the RabbitMQ server by gently closing the connection.
connection.close()
