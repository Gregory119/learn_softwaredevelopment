#!/usr/bin/env python

# Description: Receive messages from the 'log' exchange and print
# them. These messages have been published to the exchange by some
# other producer which broadcasts them. The queue in this program is
# bound to the 'log' exchange, which receives the broadcasted message.

import pika
import time

# Create a connection to the 'local host'
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Create an exchange (if it doesn't already exist) called 'logs' that
# will broadcast messages to queues bound to the exchange.
channel.exchange_declare(exchange='logs', exchange_type='fanout')

# Create a queue with a random name (we don't need to share it with other
# consumers). The exlusive flag tells RabbitMq to delete the queue when the
# connection is closed.
result = channel.queue_declare('',exclusive=True)
queue_name = result.method.queue

# Bind the queue to the exchange so that the exchange knows to send
# messages to the specified queue.
channel.queue_bind(exchange='logs',
                   # This gets the name of the queue we generated before.
                   queue=queue_name)

# Create callback to handle each queue message.
def callback(ch, method, properties, body):
    print(" [x] Received: {}".format(body))

# Register this callback with RabbitMQ for the queue we generated earlier.
channel.basic_consume(queue=queue_name,
                      # Disable the manual message acknowledgements.
                      auto_ack=True,
                      on_message_callback=callback)

# Enter a never ending loop that waits for data and runs callbacks
# whenever necessary.
print(" [*] Waiting for messages. To exit press CTRL+C")
channel.start_consuming()
