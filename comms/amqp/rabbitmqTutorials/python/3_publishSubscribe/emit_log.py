#!/usr/bin/env python

# Description: Publish (send) messages to an exchange 'logs' to be
# broadcasted to all the queues bound to the exchange.

import pika
import sys

# Create a connection to the 'local host'
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Create an exchange (if it doesn't already exist) called 'logs' that
# will broadcast messages to queues bound to the exchange.
channel.exchange_declare(exchange='logs', exchange_type='fanout')

# Create the message. If passed command line arguments, then these
# arguments will be combined into a single string, each separated by a
# space.
message = ' '.join(sys.argv[1:]) or "Hello World!"

# Send the message to the specified exchange.
channel.basic_publish(exchange='logs',
                      # The message is sent to the exchange, without
                      # knowing the destination queue (in constrast to
                      # the last two tutorials.
                      routing_key='', 
                      body=message)
print("[x] Sent: {}".format(message))

# Make sure the network buffers are flushed and that the message is
# delivered to the RabbitMQ server by gently closing the connection.
connection.close()
