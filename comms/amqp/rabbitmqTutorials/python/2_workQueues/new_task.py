#!/usr/bin/env python

# Description: Schedule tasks to the work queue (named 'task_queue') with messages.

import pika
import sys

# Create a connection to the 'local host'
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Create a recipient queue if it doesn't exist already.
# Tell RabbitMQ to not forget about the queue if RabbitMQ quits or
# crashes by using the durable parameter. We need to do something
# similar for the message later.
channel.queue_declare(queue='task_queue', durable=True)

# Create the message. If passed command line arguments, then these
# arguments will be combined into a single string, each separated by a
# space.
message = ' '.join(sys.argv[1:]) or "Hello World!"

# Send the message through the default exchange to the specific queue.
channel.basic_publish(exchange='', # Use the default exchange. Allows
                                   # specifying the destination queue
                                   # in the routing key.
                      routing_key='task_queue',
                      body=message,
                      properties=pika.BasicProperties(
                          # Make message persistent. Not a complete
                          # guarantee (there is a short time window
                          # when RabbitMQ has accepted a message and
                          # hasn't saved it yet, and the message might
                          # be temporarily saved to file cache without
                          # flushing with fsync), but it is better
                          # than nothing.
                          delivery_mode = 2 
                      ))
print("[x] Sent: {}".format(message))

# Make sure the network buffers are flushed and that the message is
# delivered to the RabbitMQ server by gently closing the connection.
connection.close()
