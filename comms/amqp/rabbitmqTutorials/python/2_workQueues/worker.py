#!/usr/bin/env python

# Description: Pop messages from the worker queue (named 'task_queue') and perform the task.

import pika
import time

# Create a connection to the 'local host'
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Create a recipient queue if it doesn't exist already.
# Tell RabbitMQ to not forget about the queue if RabbitMQ quits or
# crashes by using the durable parameter. We need to do something
# similar for the messages when they are published by the sender.
channel.queue_declare(queue='task_queue', durable=True)

# Create callback to handle each queue message. Every '.' in the
# received message will generate a sleep for 1 second.
def callback(ch, method, properties, body):
    print(" [x] Received: {}".format(body))

    # The b infront of the string will cast the string to a bytes type
    # in Python 3 and above.
    time.sleep(body.count(b'.')) 

    # Send the message acknowledgement of completion to the RabbitMQ
    # server.
    ch.basic_ack(delivery_tag = method.delivery_tag) 
    print(" [x] Done")

# Don't dispatch more than one message to a worker at a time. If a
# worker is busy processing a message, then dispatch it to the next
# worker that is not busy.
channel.basic_qos(prefetch_count=1)
    
# Register this callback with RabbitMQ for the specified queue.  This
# command will only succeed if the queue we want to subscribe exists,
# which we have already made sure of with 'queue_declare'.
channel.basic_consume(queue='task_queue',
                      # auto_ack=True, Without this flag the manual
                      # message acknowledgements are turned on (by
                      # default). If the worker's connection is
                      # closed, channel is closed, or TCP connection
                      # is lost, without an acknowledgement then
                      # RabbitMQ will redeliver the message.
                      on_message_callback=callback)

# Enter a never ending loop that waits for data and runs callbacks
# whenever necessary.
print(" [*] Waiting for messages. To exit press CTRL+C")
channel.start_consuming()
