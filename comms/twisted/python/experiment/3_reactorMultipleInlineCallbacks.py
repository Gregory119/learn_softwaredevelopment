#!/usr/bin/env python

from twisted.internet.defer import Deferred
from twisted.internet.defer import inlineCallbacks

def falldown(val):
    raise Exception('I fall down.')

def upagain():
    print("But I get up again.")
    reactor.stop()

def helloA(val):
    print("helloA: {}".format(val))
    return val+1

def helloB(val):
    print("helloB: {}".format(val))
    return val+2

def helloC(val):
    print("helloC: {}".format(val))
    return val+3

def printA(val):
    print("helloA")

def failedA(err):
    print("failedA with error: {}".format(repr(err)))

def failedB(err):
    print("failedB with error: {}".format(repr(err)))
    
# The following inline callback generator replicates the following
# deferred (without the error callback at the end). Adding the ErrBack
# is dealt with later:
# dA = Deferred()
# dA.addCallback(helloA)
# dA.addCallback(helloA)
# dA.addCallback(falldown)
# dA.addCallback(helloA)
# dA.addErrback(failedA)
@inlineCallbacks
def ACallback(val):
    res = yield helloA(val)
    res = yield helloA(res)
    res = yield falldown(res)
    res = yield helloA(res)

# dB = Deferred()
# dB.addCallback(helloB)
# dB.addCallback(helloB)
# dB.addCallback(helloB)
# dB.addErrback(failedB) This is accommodated later.
@inlineCallbacks
def BCallback(val):
    res = yield helloB(val)
    res = yield helloB(res)
    res = yield helloB(res)

# dC = Deferred()
# dC.addCallback(helloC)
# dC.addCallback(helloC)
# dC.addCallback(helloC)
@inlineCallbacks
def CCallback(val):
    res = yield helloC(val)
    res = yield helloC(res)
    res = yield helloC(res)

# The following function replicates the following:
# reactor.callWhenRunning(dA.callback,0)
# reactor.callWhenRunning(dB.callback,1)
# reactor.callWhenRunning(dC.callback,3)
@inlineCallbacks
def runCallbacks():
    # Returns a Deferred that is called when the generator ACallback() has
    # completely finished (or throws an exception)
    dA = ACallback(0)
    dA.addErrback(failedA)

    dB = BCallback(1)
    dB.addErrback(failedB)

    dC = CCallback(3)
    yield None
    
from twisted.internet import reactor

reactor.callWhenRunning(runCallbacks)

print("Starting the reactor.")
reactor.run()
