#!/usr/bin/env python

from twisted.internet.task import LoopingCall

def callbackHalfSec():
    print "Reached callbackHalfSec."

def callbackOneSec():
    print "Reached callbackOneSec."
    
loopCallHalfSec = LoopingCall(callbackHalfSec)
loopCallHalfSec.start(0.5)

loopCallOneSec = LoopingCall(callbackOneSec)
loopCallOneSec.start(1)

from twisted.internet import reactor

print "Starting the reactor."
reactor.run()
