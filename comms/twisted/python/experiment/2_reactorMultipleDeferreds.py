#!/usr/bin/env python

from twisted.internet.defer import Deferred

def falldown(val):
    raise Exception('I fall down.')

def upagain():
    print("But I get up again.")
    reactor.stop()

def helloA(val):
    print("helloA: {}".format(val))
    return val+1

def helloB(val):
    print("helloB: {}".format(val))
    return val+2

def helloC(val):
    print("helloC: {}".format(val))
    return val+3

def printA(val):
    print("helloA")

def failedA(err):
    print("failedA with error: {}".format(repr(err)))

def failedB(err):
    print("failedB with error: {}".format(repr(err)))
    

from twisted.internet import reactor
    
dA = Deferred()
dA.addCallback(helloA)
dA.addCallback(helloA)
dA.addCallback(falldown)
dA.addCallback(helloA)
dA.addErrback(failedA)

reactor.callWhenRunning(dA.callback,0)

dB = Deferred()
dB.addCallback(helloB)
dB.addCallback(helloB)
dB.addCallback(helloB)
dB.addErrback(failedB)

reactor.callWhenRunning(dB.callback,1)

dC = Deferred()
dC.addCallback(helloC)
dC.addCallback(helloC)
dC.addCallback(helloC)
    
reactor.callWhenRunning(dC.callback,3)

print("Starting the reactor.")
reactor.run()
