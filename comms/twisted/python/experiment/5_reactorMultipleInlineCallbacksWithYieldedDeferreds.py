#!/usr/bin/env python

# The generators are only paused if they return a Deferred, just as a
# chain of callbacks of a Deferred are paused if one of the callbacks
# in the chain returns a deferred. At this point the outer deferred
# adds a callback at the end of the returned (inner deferred). The
# returned deferred must be scheduled to be called at some other point
# in time, which will end up calling the callback added by the outer
# deferred, in order to continue with the rest of the callbacks in the
# outer deferred.

from twisted.internet.defer import Deferred
from twisted.internet.defer import inlineCallbacks

def falldown(val):
    raise Exception('I fall down.')

def upagain():
    print("But I get up again.")
    reactor.stop()

def helloA(val):
    print("helloA: {}".format(val))
    return val+1

def helloB(val):
    print("helloB: {}".format(val))
    return val+2

def helloC(val):
    print("helloC: {}".format(val))
    return val+3

def printA(val):
    print("helloA")

def failedA(err):
    print("failedA with error: {}".format(repr(err)))

def failedB(err):
    print("failedB with error: {}".format(repr(err)))

def failedC(err):
    print("failedC with error: {}".format(repr(err)))
    
@inlineCallbacks
def ACallback(val):
    d = Deferred()
    d.addCallback(helloA)
    d.addCallback(helloA)
    d.addErrback(failedA)

    from twisted.internet import reactor
    reactor.callLater(3,d.callback,val)

    # This generator, created by @inclineCallbacks, will pause when yielding a
    # deferred. When this deferred completes at some other point in time
    # (hopefully it does), then the final result of the deferred will be placed
    # back into ACallback at the yield point, and the generator will continue to
    # be called by the reactor.
    res = yield d
    res = yield helloA(res)

    try:
        res = yield falldown(res)
    except Exception:
        print("Caught exception.")

    res = yield falldown(res)
    res = yield helloA(res)

@inlineCallbacks
def BCallback(val):
    res = yield helloB(val)
    res = yield helloB(res)
    res = yield helloB(res)

@inlineCallbacks
def CCallback(val):
    res = yield helloC(val)
    res = yield helloC(res)

    d = Deferred()
    d.addCallback(helloC)
    d.addCallback(helloC)

    from twisted.internet import reactor
    reactor.callLater(2,d.callback,res)
    
    res = yield d
    res = yield helloC(res)

def runCallbacks():
    # Returns a Deferred that is called when the generator ACallback() has
    # completely finished (or throws an exception - unless the exception is
    # caught inside the inline callback function)
    dA = ACallback(0)
    dA.addErrback(failedA)

    dB = BCallback(1)
    dB.addErrback(failedB)

    dC = CCallback(3)
    dC.addErrback(failedC)
    
from twisted.internet import reactor

reactor.callWhenRunning(runCallbacks)

print("Starting the reactor.")
reactor.run()
