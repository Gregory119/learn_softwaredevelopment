#!/usr/bin/env python

def falldown():
    raise Exception('I fall down.')

def upagain():
    print("But I get up again.")
    reactor.stop()

def hello(val):
    print("hello: {}".format(val))
    
from twisted.internet import reactor

reactor.callWhenRunning(falldown)
reactor.callWhenRunning(upagain)
reactor.callWhenRunning(hello, 2)
reactor.callWhenRunning(falldown)
reactor.callWhenRunning(hello)
reactor.callWhenRunning(hello, 10)

print("Starting the reactor.")
reactor.run()
