#!/usr/bin/env python

import sys

from twisted.internet.defer import Deferred

def gotPoem(poem):
    print poem
    from twisted.internet import reactor
    reactor.stop()

def poemFailed(err):
    print >>sys.stderr, 'poem download failed'
    print >>sys.stderr, 'I am terribly sorry'
    print >>sys.stderr, 'try again later?'
    from twisted.internet import reactor
    reactor.stop()

d = Deferred()

d.addCallbacks(gotPoem, poemFailed)

from twisted.internet import reactor

# Use Deferred to hold a chain of callbacks. Pass the function to
# start the chain of calls to the reactor. The reactor will call the
# 'callback' function of the chain, with the given string as a
# parameter, when the reactor has started running.
reactor.callWhenRunning(d.callback, 'Another short poem')

# Both gotPoem() and poemFailed() stop the reactor. This will be
# refactorred in the next code unit.

reactor.run()
