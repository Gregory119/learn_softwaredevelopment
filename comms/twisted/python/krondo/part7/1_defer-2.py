#!/usr/bin/env python

from twisted.internet.defer import Deferred
from twisted.python.failure import Failure

def gotPoem(res):
    print("Your poem is served.")
    print(res)

def poemFailed(err):
    print("No poetry for you.")

d = Deferred()

# Add a callback/errback pair to the chain
d.addCallbacks(gotPoem, poemFailed)

# Fire the chain with an error result.
d.errback(Failure(Exception("I have failed.")))

print("Finished.")
