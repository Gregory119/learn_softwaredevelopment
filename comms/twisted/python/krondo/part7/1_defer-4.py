#!/usr/bin/env python

from twisted.internet.defer import Deferred

def out(s): print(s)

d = Deferred()
d.addCallbacks(out, out)
d.callback('First result')
d.callback('Second result')

# A Deferred will not let us fire the normal results callbacks a
# second time, no matter what.

print("Finished.")
