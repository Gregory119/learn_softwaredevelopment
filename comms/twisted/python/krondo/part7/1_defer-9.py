#!/usr/bin/env python

# Tutorial at: http://krondo.com/an-interlude-deferred/

import sys

from twisted.internet.defer import Deferred

def gotPoem(poem):
    print poem

def poemFailed(err):
    print >>sys.stderr, 'poem download failed'
    print >>sys.stderr, 'I am terribly sorry'
    print >>sys.stderr, 'try again later?'
    from twisted.internet import reactor
    reactor.stop()

def poemDone(_):
    from twisted.internet import reactor
    reactor.stop()
    
d = Deferred()

d.addCallbacks(gotPoem, poemFailed)

# poemDone will be called after either gotPoem or poemFailed is
# called.  Note: There's a subtlety in the way this deferred would
# execute its errback chain. This is discussed in a futuer part.
d.addBoth(poemDone)

from twisted.internet import reactor

# Use Deferred to hold a chain of callbacks. Pass the function to
# start the chain of calls to the reactor. The reactor will call the
# 'callback' function of the chain, with the given string as a
# parameter, when the reactor has started running.
reactor.callWhenRunning(d.callback, 'Another short poem')

reactor.run()
