#!/usr/bin/env python

from twisted.internet.defer import Deferred

def gotPoem(res):
    print("Your poem is served.")
    print(res)

def poemFailed(err):
    print("No poetry for you.")

d = Deferred()

# Add a callback/errback pair to the chain
d.addCallbacks(gotPoem, poemFailed)

# Fire the chain with a normal result.
d.callback("This poem is short.")

print("Finished.")
