#!/usr/bin/env python

# Tutorial at: http://krondo.com/just-another-way-to-spell-callback/

def my_generator():
    print 'my_generator'
    yield 1
    print 'my_generator'
    yield 2
    print 'my_generator'
    yield 3

def my_other_generator():
    print 'my_other_generator'
    yield 10
    print 'my_other_generator'
    yield 20
    print 'my_other_generator'
    yield 30
    print 'my_other_generator'
    yield 40

gens = [my_generator(), my_other_generator()]

while gens:
    for g in gens[:]:
        try:
            n = g.next()
        except StopIteration:
            print 'StopIteration'
            gens.remove(g)
        else:
            print n
