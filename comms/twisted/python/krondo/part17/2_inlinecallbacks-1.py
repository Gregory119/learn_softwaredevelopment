#!/usr/bin/env python

# Tutorial at: http://krondo.com/just-another-way-to-spell-callback/

from twisted.internet.defer import inlineCallbacks, Deferred

@inlineCallbacks
def myCallbacks():
    from twisted.internet import reactor

    print 'first callback'
    result = yield 1 # yielded values that aren't deferred come right back

    print 'second callback got', result
    d = Deferred()
    reactor.callLater(5, d.callback, 2)
    result = yield d # yielded deferred will pause the generator

    print 'third callback got', result # The result of the deferred

    d = Deferred()
    reactor.callLater(5, d.errback, Exception(3))

    try:
        yield d
    except Exception, e:
        result = e

    print 'fourth callback got', repr(result) # the exception from the deferred

    reactor.stop()

from twisted.internet import reactor
reactor.callWhenRunning(myCallbacks)
reactor.run()
