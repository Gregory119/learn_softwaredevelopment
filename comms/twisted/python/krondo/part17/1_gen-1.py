#!/usr/bin/env python

# Tutorial at: http://krondo.com/just-another-way-to-spell-callback/

def myGenerator():
    print 'starting up'
    yield 1
    print 'working'
    yield 2
    print 'still working'
    yield 3
    print 'done'

for n in myGenerator():
    print n
