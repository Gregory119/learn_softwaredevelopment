#!/usr/bin/env python

# Tutorial at: http://krondo.com/just-another-way-to-spell-callback/

# The generator is just an object for getting successive values.

def myGenerator():
    print 'starting up'
    yield 1
    print 'working'
    yield 2
    print 'still working'
    yield 3
    print 'done'

gen = myGenerator()

while True:
    try:
        n = gen.next()
    except StopIteration:
        break
    else:
        print n

# We can think of the while loop as the reactor, and the generator as
# a series of callbacks separated by yield statements, with the
# interesting fact that all the callbacks share the same local
# variable namespace, and the namespace persisits from one callback to
# the next.
