#!/usr/bin/env python

# Tutorial at: http://krondo.com/just-another-way-to-spell-callback/

class Malfunction(Exception):
    pass

def my_generator():
    print 'starting up'

    val = yield 1
    print('got: {}'.format(val))

    val = yield 2
    print('got: {}'.format(val))

    try:
        yield 3
    except Malfunction:
        print 'Malfunction!'

    yield 4

    print 'done'

gen = my_generator()

print gen.next() # start the generator
print gen.send(10) # send the value 10
print gen.send(20) # send the value 20
print gen.throw(Malfunction()) # raise and exception inside the generator

try:
    gen.next()
except StopIteration:
    pass
