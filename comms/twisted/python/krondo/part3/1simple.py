#!/usr/bin/env python

# If you want to use a particular reactor type (eg. pollreactor), it
# should be installed here, before the normal reactor import line is
# called:
# from twisted.internet import pollreactor
# pollreactor.install()

def funcSayHello():
    print("Why hello there missy!")

from twisted.internet import reactor

reactor.callWhenRunning(funcSayHello)

print("Starting the reactor..")
reactor.run()
