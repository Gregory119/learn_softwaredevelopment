#!/usr/bin/env python

class Countdown(object):
    counter = 5

    def count(self):
        if self.counter == 0:
            reactor.stop()
        else:
            print("{}...".format(self.counter))
            self.counter -= 1
            reactor.callLater(1,self.count)

from twisted.internet import reactor

reactor.callWhenRunning(Countdown().count)

print("Start")
reactor.run()
print("Stop!")
