// Client side C/C++ program to demonstrate Socket programming
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>

#include <unistd.h>
#include <arpa/inet.h>

#include <string.h>

#define SERVER_PORT 8080

int main(int argc, char const *argv[])
{
  struct sockaddr_in address;

  int clientSocket = 0;
  if ((clientSocket = socket(AF_INET,
                             SOCK_STREAM,
                             0)) < 0)
    {
      printf("\n Socket creation error \n");
      return -1;
    }

  struct sockaddr_in serverAddr;
  memset(&serverAddr, 0, sizeof(serverAddr)); // Good practice
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(SERVER_PORT);

  /* Convert IPv4 and IPv6 addresses from text to binary form. The
     binary form is a in_addr (IPv4), or in6_addr (IPv6) structure
     that is output to the third parameter. See
     http://man7.org/linux/man-pages/man3/inet_pton.3.html*/
  if ( inet_pton(AF_INET, // Use IPv4
                 "127.0.0.1", // Dotted, decimal format of the network address to be converted.
                 &serverAddr.sin_addr) <= 0)
    {
      printf("\nInvalid address/ Address not supported \n");
      return -1;
    }

  /* Connect the client socket to the server address.*/
  if (connect(clientSocket,
              (struct sockaddr*)&serverAddr,
              sizeof(serverAddr)) < 0)
    {
      /*
        If we want to try again, then we must close the socket, create
        a new one and then call connect() again.
       */

      printf("\nConnection Failed \n");
      return -1;
    }

  /*
    At this point we can use read() and write(), or the socket
    specific send() and recv() calls.

    If the peer socket is closed, then reading all the buffered data
    on the local socket will end in an EOF. Writing to the local
    socket will results in SIGPIPE signal and an error EPIPE. This
    error can be checked before writing. See section 56.5.4, pg 1159
    of "The Linux Programming Interface".
   */

  char hello[] = "Hello from client";
  write ( clientSocket , hello , strlen(hello) );
  printf("Hello message sent\n");

  char buffer[1024] = {0};
  int valread;
  valread = read ( clientSocket , buffer, 1024 );
  printf("%s\n",buffer );
  return 0;
}
