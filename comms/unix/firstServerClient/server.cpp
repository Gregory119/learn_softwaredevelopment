/*
  Server side C/C++ program to demonstrate Socket programming
  Extracted from https://www.geeksforgeeks.org/socket-programming-cc/
 */

/*
  Questions

  1) If the server address is IPv4, does that guarantee that the client
  must connect with IPv4?
 */

#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>

#include <iostream>

#define PORT 8080

int main(int argc, char const *argv[])
{
  // Creating socket file descriptor
  int server_fd; // File descriptor of the created socket.
  int domain = AF_INET; // IPv4 Internet Protocol
  int type = SOCK_STREAM; // Socket stream
  int protocol = 0; /* Normally only a single protocol exists to
                     * support a particular socket type within a
                     * given protocol family, in which case
                     * protocol=0. See top of pg 1151 of "The Linux
                     * Programming Interface" and
                     * http://man7.org/linux/man-pages/man2/socket.2.html.*/
  if ((server_fd = socket(domain, SOCK_STREAM, protocol)) < 0)
    {
      /* On success, a file descriptor for the new socket is
       * returned. On error, -1 is returned, and errno is set
       * appropriately.*/
      perror("socket failed");
      exit(EXIT_FAILURE);
    }

  // Set socket options
  int opt = 1; // Set to non-zero to enable a boolean option. See
               // https://linux.die.net/man/2/setsockopt.
  if ( setsockopt(server_fd,
                  SOL_SOCKET, // Manipulate options at the sockets API level
                  SO_REUSEADDR | /* Allow binding to a local address more
                                    than once (some other process may
                                    have already binded to
                                    it). See https://linux.die.net/man/7/socket */
                  SO_REUSEPORT, /* Permits multiple AF_INET or
                                   AF_INET6 sockets to be bound to an
                                   identical socket address. */
                  &opt,
                  sizeof(opt)) < 0 )
    {
      perror("setsockopt");
      exit(EXIT_FAILURE);
    }

  // Forcefully attaching socket to the loopback address and port 8080
  struct sockaddr_in serverAddress; // See structure at
                                    // https://linux.die.net/man/7/ip
  serverAddress.sin_family = AF_INET; // Domain for IPv4 communication. Always
                                      // set to this for sockaddr_in.
  serverAddress.sin_addr.s_addr = INADDR_ANY; // The socket will be bound to ALL
                                              // local interfaces. See
                                              // https://linux.die.net/man/7/ip
  serverAddress.sin_port = htons( PORT ); // Port in network byte order.

  // Cast to the generic socket address type.
  struct sockaddr* gen_sockaddr = (struct sockaddr*)&serverAddress;

  if (bind(server_fd,
           gen_sockaddr,
           sizeof(serverAddress)) < 0)
    {
      perror("bind failed");
      exit(EXIT_FAILURE);
    }

  // After a call to listen, the socket is considered passive.
  int backlog = 3; /* The max number of pending connections that will
                      be accepted after the call to accept(). This is
                      silently limited by the value in
                      /proc/sys/net/core/somaxconn at runtime. It was
                      128 when I checked.

                      A call to connect() by the client before the
                      server has called accept() will cause the client
                      connection to pend in a queue until it has been
                      accepted with accept().*/
  if (listen(server_fd,
             backlog) < 0)
    {
      perror("listen");
      exit(EXIT_FAILURE);
    }

  /* Accept a connection. Block until a connection request arrives.
   Accepting a connection on a passive socket is known as a passive
   open. The server socket remains open to be used to accept
   additional future connect requests. The accept call creates a new
   socket that is connected to the peer socket.

   The client address parameters are outputs.
  */
  struct sockaddr clientAddress; /* The type of this socket will
                                    depend on the socket domain. */
  int clientAddrLen = sizeof ( clientAddress ); /* Must be initialized
                                                   to the size of the
                                                   structure passed in
                                                   to accept(). This
                                                   will be overwritten
                                                   with the size of
                                                   data written to the
                                                   client address
                                                   structure.*/
  int clientSocket = accept(server_fd,
                            (struct sockaddr*)&clientAddress,
                            (socklen_t*)&clientAddrLen);
  if ( clientSocket < 0)
    {
      perror("accept");
      exit(EXIT_FAILURE);
    }
  /* If we don't care about the client address, then we can use NULL
   and 0 in place of the &clientAddress and &clientAddrLen parameters,
   respectively. We could retrieve the client address details later
   using getpeername().*/

  // Let's check the client socket domain.
  switch ( clientAddress.sa_family )
    {
    case AF_INET:
      std::cout << "Client socket domain is IPv4." << std::endl;
      break;

    default:
      std::cout << "Unhandled client socket domain." << std::endl;
      break;
    }

  int valread;
  char buffer[1024] = {0};
  char hello[] = "Hello from server";

  // Read from the socket connected to the client (peer) socket.
  valread = read( clientSocket , buffer, 1024);
  printf("%s\n",buffer );

  send(clientSocket , hello , strlen(hello) , 0 );
  printf("Hello message sent\n");
  return 0;
}
