#include <vector>
#include <iostream>
#include <set>

using namespace std;

void minimumBribes(vector<int> q)
{
    unsigned count_total = 0;
    int count_prev_greater_nums = 0;
    set<int> prev;
    for (size_t i=0; i<q.size(); ++i)
    {
        if (q[i]-1-static_cast<int>(i) > 2) // shift left from original pos
        {
            cout << "Too chaotic " << endl;
            return;
        }
        set<int>::iterator first_greater = prev.upper_bound(q[i]); // log(i)
        if (first_greater == prev.end())
        {
          prev.insert(q[i]); // log(i)
          continue;
        }
        count_prev_greater_nums = std::distance(first_greater,prev.end()); // no. of accepted bribes by q[i]
        count_total += count_prev_greater_nums;
        prev.insert(q[i]); // log(i)
    }
    cout << count_total << endl;
}

//----------------------------------------------------------------------//
int main(int argc, char* argv[])
{
  vector<int> vec = {2,1,5,3,4};
  minimumBribes(vec);

  return 0;
}





