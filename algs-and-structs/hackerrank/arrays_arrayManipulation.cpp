#include <vector>
#include <iostream>
#include <set>
#include <algorithm>
#include <unordered_map>

using namespace std;

long manipArray(int n, const vector<vector<int>>& queries)
{
  vector<long> sums;
  sums.resize(n);
  // sums
  for (int i=0; i<queries.size(); ++i)
    {
      for (int j=queries[i][0]-1; j<=queries[i][1]-1; ++j)
        {
          sums[j]+=queries[i].back();
        }
      for (const auto& sum : sums)
        {
          cout << sum << " ";
        }
      cout << endl;
    }
  cout << endl;
    
  // find max
  long max = 0;
  for (const auto& sum : sums)
    {
      cout << sum << " ";
      if (sum > max)
        {
          max = sum;
        }
    }
  cout << endl;
  return max;
}

//----------------------------------------------------------------------//
long manipArray2(int n, vector<vector<int>>& queries)
{
  // sort rows by descending k value (the add value)
  sort(queries.begin(),queries.end(),
       [](const vector<int>& a, const vector<int>b){
         return a.back() > b.back();
       });
    
  // get the absolute max sums
  std::vector<long> abs_max_sums; // ascending order (starts with the smallest)
  abs_max_sums.reserve(queries.size()+1); // one size large to start with a zero
  abs_max_sums.push_back(0);
  for (size_t i = 0; i<queries.size(); ++i)
    {
      abs_max_sums.push_back(abs_max_sums[i]+queries[i].back());
    }
  cout << "max_sums: ";
  for (size_t i=0; i<queries.size()+1; ++i)
    {
      cout << abs_max_sums[i] << " ";
    }
  cout << endl << endl;
    
  // create the map to hold the sums
  unordered_map<int,long> sums; // key=index, value=current cumulative sum

  // max values for each state after addition
  long max = 0;
  long second_max = 0;
  long k_max_sum = 0;

  for (size_t i = 0; i<queries.size(); ++i)
    {
      // add the k of the query to the appropriate indices
      for (size_t j=queries[i][0]-1; j<=queries[i][1]-1; ++j)
        {
          if (sums.find(j) != sums.end()) // element is not empty
            {
              sums[j] += queries[i].back();
            }
          else
            {
              sums[j] = queries[i].back();
            }
        }
      // if the max number is ahead of the second largest number by more than the max cumulative sum of all the other k values
      // find max, and second max
      for (auto it=sums.begin();it!=sums.end();++it)
        {
          cout << it->second << " ";
          if (it->second > max)
            {
              second_max = max;
              max = it->second;
              k_max_sum = std::distance(sums.begin(),it);
            }
        }
      cout << endl << endl;

      if (max-second_max > abs_max_sums[queries.size()-1-i])
        {
          // the max number will end up being the final max so some up the other k values for this index
          for (size_t l=i+1; l<queries.size(); ++l)
            {
              if (k_max_sum >= queries[l][0] &&
                  k_max_sum <= queries[l][1])
                {
                  max += queries[l].back();
                }
            }
        }
    }
  return max;
}

//----------------------------------------------------------------------//
int main(int argc, char* argv[])
{
  vector<vector<int>> vec = {{2,3,603},
                             {1,1,286},
                             {4,4,882}};

  cout << manipArray2(4,vec) << endl;

  return 0;
}
