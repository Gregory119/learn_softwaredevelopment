#include <iostream>
#include <functional>
#include <cassert>
#include <cmath>
#include <vector>

//----------------------------------------------------------------------//
int lastDigitOfFibSlow(long long n)
{
  if (n < 2)
    {
      return n;
    }
  
  int prev = 0;
  int fib = 1;

  for (int i=2; i<=n; ++i)
    {
      int temp_fib = fib;
      fib = fib + prev;
      prev = temp_fib;
    }

  return fib % 10;
}

//----------------------------------------------------------------------//
// 0 <= n,m <= 45
int lastDigitOfFibSumIntervalSlow(long long n, long long m)
{
  if (n == m)
    {
      return lastDigitOfFibSlow(n);
    }

  int dn = lastDigitOfFibSlow(n+2);
  int dm = lastDigitOfFibSlow(m+1);

  if (dn < dm)
    {
      return dn+10-dm;
    }
  
  return dn-dm;
}

//----------------------------------------------------------------------//
// 1 <= i <= 10^18
// 2 <= m <= 1000
int modulusFibFast(long long i, int m)
{
  // Find size of Pisano period, pi(m) for m
  // If m = 1000 => size of Pisano period is 1500 => need to calculate the 1500th Fibonacci number.
  // We can calculate the entire fibonacci modulus m sequence by only using the results of fibonacci modulus m
  std::vector<int> fibs_mod_m;
  fibs_mod_m.push_back(0);
  fibs_mod_m.push_back(1);
  
  while (1)
    {
      int last_index = fibs_mod_m.size()-1;
      fibs_mod_m.push_back((fibs_mod_m[last_index]+fibs_mod_m[last_index-1]) % m);

      // Check for start of new period by looking for 0,1,1
      last_index = fibs_mod_m.size()-1;
      if (fibs_mod_m.size() > 3 &&
	  fibs_mod_m[last_index] == 1 &&
	  fibs_mod_m[last_index-1] == 1 &&
	  fibs_mod_m[last_index-2] == 0)
	{
	  fibs_mod_m.erase(fibs_mod_m.end()-3,fibs_mod_m.end());
	  break;
	}
    }
  
  
  // Fib(i) modulus m = Fib(i % pi(m)) % m
  // So use "i % pi(m)" as an index into the first period of Fib(i) modulus m.
  return fibs_mod_m[i % fibs_mod_m.size()];
}

//----------------------------------------------------------------------//
// 1 <= n,m <= 10^18
int lastDigitOfFibSumIntervalFast(long long n, long long m)
{
  if (n == m)
    {
      return modulusFibFast(n, 10);
    }

  int dn = modulusFibFast(n+2, 10);
  int dm = modulusFibFast(m+1, 10);

  if (dn < dm)
    {
      return dn+10-dm;
    }
  
  return dn-dm;
}

//----------------------------------------------------------------------//
void test(std::function<int(long long, long long)> sol)
{
  //======================================================================
  // Edge Cases
  //======================================================================
  long num1 = 0;
  long num2 = 0;
  assert(sol(num1,num2) == 0);

  num1 = 1;
  num2 = 0;
  assert(sol(num1,num2) == 1);

  //======================================================================
  // Simple Cases
  //======================================================================
  num1 = 1;
  num2 = 0;
  assert(sol(num1,num2) == 1);

  num1 = 3; // fib sum = 4
  num2 = 1; // fib sum at 0 = 0
  assert(sol(num1,num2) == 4);

  num1 = 6; // fib sum = 20
  num2 = 3; // fib sum at 2 = 2
  // 20 - 2 = 18
  // last digit is 8
  assert(sol(num1,num2) == 8);

  num1 = 6; // fib sum = 20
  num2 = 6; // fib sum at 5 = 12
  // 20 - 12 = 8
  // last digit is 8
  assert(sol(num1,num2) == 8);  
}

//----------------------------------------------------------------------//
int main() {
  long long n = 0;
  long long m = 0;
  std::cin >> m >> n;

  //test(lastDigitOfFibSumIntervalSlow);
  //test(lastDigitOfFibSumIntervalFast);
  
  std::cout << lastDigitOfFibSumIntervalFast(n,m) << std::endl;
  return 0;
}
