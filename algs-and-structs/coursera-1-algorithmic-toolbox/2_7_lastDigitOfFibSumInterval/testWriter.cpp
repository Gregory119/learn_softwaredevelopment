#include <algorithm>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <random>
#include <vector>

//----------------------------------------------------------------------//
void writeTestDataToFile(const std::string& test_name,
			 long long num1,
			 long long num2)
{
  static int test_number = 0;
  ++test_number;

  std::ostringstream filename;
  filename << "input_data_test"
	   << std::setfill('0')
	   << std::setw(5)
	   << test_number
	   << "_"
	   << test_name
	   << ".txt";

  std::ofstream test_data_file(filename.str());
  test_data_file << num1 << " " << num2 << std::endl;
}

//----------------------------------------------------------------------//
void writeStressTest(unsigned iterations)
{
  std::default_random_engine rand_engine;
  //std::uniform_int_distribution<long long> uni_dist_fib(1,1000000000000000000); // This will take forever with the naive solution.
  std::uniform_int_distribution<long long> uni_dist_fib1(1,45);

  for (unsigned i=0; i<iterations; ++i)
    {
      // Randomly generate data.
      long long num1 = uni_dist_fib1(rand_engine);
      std::uniform_int_distribution<long long> uni_dist_fib2(num1,45);
      long long num2 = uni_dist_fib2(rand_engine);

      writeTestDataToFile("stress",num1,num2);
    }
}

//----------------------------------------------------------------------//
int main(int argc, char* argv[])
{
  //======================================================================
  // Edge Cases
  //======================================================================
  long long num1 = 45;
  long long num2 = 45;
  writeTestDataToFile("edge",num1,num2);

  num1 = 0;
  num2 = 0;
  writeTestDataToFile("edge",num1,num2);

  num1 = 0;
  num2 = 1;
  writeTestDataToFile("edge",num1,num2);

  //======================================================================
  // Simple Cases
  //======================================================================
  num2 = 3; // fib sum = 4
  num1 = 1; // fib sum at 0 = 0
  writeTestDataToFile("simple",num1,num2);

  num2 = 6; // fib sum = 20
  num1 = 3; // fib sum at 2 = 2
  // 20 - 2 = 18
  // last digit is 8
  writeTestDataToFile("simple",num1,num2);

  num1 = 6; // fib sum = 20
  num2 = 6; // fib sum at 5 = 12
  // 20 - 12 = 8
  // last digit is 8
  writeTestDataToFile("simple",num1,num2);
  
  //======================================================================
  // Stress Test
  //======================================================================
  writeStressTest(15);
  
  return 0;
}
