#!/usr/bin/env bash

test_dir=.
test_writer=testWriter.exec
solution=solution.exec
test_evaluator=testEvaluator.exec

../testSolution.sh $test_dir $test_writer $solution $test_evaluator
