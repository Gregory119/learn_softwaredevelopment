#include <iostream>
#include <unordered_set>
#include <vector>
#include <numeric>

using std::vector;

int sum ( const std::unordered_set<int>& s, const vector<int> & A ) {
  int ret = 0;
  for ( auto i : s ) {
    ret += A[i];
  }
  return ret;
}

void findSumSetsRecursive ( int targetSum, int i, const vector<int> &A, std::unordered_set<int> &currSet, std::vector<std::unordered_set<int>> &possibleSets )
{
  int setSum = sum ( currSet, A );
  if ( setSum == targetSum ) {
    possibleSets.push_back ( currSet );
    // std::cout << "possible set: ";
    // for ( const auto& i : currSet ) {
    //   std::cout << " " << i << ", ";
    // }
    // std::cout << std::endl;
    return;
  } else if ( setSum > targetSum ) {
    return;
  }
  
  if ( i >= A.size() ) {
    return;
  }

  currSet.insert ( i );
  findSumSetsRecursive ( targetSum, i + 1, A, currSet, possibleSets );
  currSet.erase ( i );
  findSumSetsRecursive ( targetSum, i + 1, A, currSet, possibleSets );
}

int findSumSetsIt ( int targetSum, const vector<int> &A, int numSets, const std::unordered_set<int> &ignoreSet )
{
  if ( numSets == 0 ) {
    if ( ignoreSet.size() == A.size() ) {
      //std::cout << "num sets: " << numSets << ", ignore set size: " << ignoreSet.size() << std::endl;
      return 1;
    } else {
      return 0;
    }
  }
  
  vector<std::unordered_set<int>> prevSets;
  prevSets.push_back ( {} ); // start with an empty set

  for ( std::size_t i = 0; i < A.size(); ++i ) {
    if ( ignoreSet.find(i) != ignoreSet.end() ) {
      continue;
    }
    vector<std::unordered_set<int>> currSets;
    for ( auto setIt = prevSets.begin(); setIt != prevSets.end(); ++setIt) {
      // copy set
      std::unordered_set<int> currSet ( *setIt );
      // insert curr element index
      currSet.insert ( i );
      // if sum matches then check for other sets
      int setSum = sum ( currSet, A );
      if ( setSum == targetSum ) {
	// std::cout << "possible set: ";
	// for ( const auto& i : currSet ) {
	//   std::cout << " " << i << ", ";
	// }
	// std::cout << std::endl;

	// find possible subsets
	std::unordered_set<int> newIgnoreSet { ignoreSet };
	newIgnoreSet.insert ( currSet.begin(), currSet.end() );
	if ( findSumSetsIt ( targetSum, A, numSets - 1, newIgnoreSet ) ){
	  return 1;
	}
      } else if ( setSum > targetSum ) {
	// no need to keep if already over the sum
	continue;
      } else {
	// else move to current sets that will be considered in the next iteration
	currSets.push_back ( std::move ( currSet ) );
      }
    }
    // move current sets to previous sets
    prevSets.insert ( prevSets.end(), currSets.begin(), currSets.end() );
  }
  return 0;
}

int partition3 ( vector<int> &A )
{
    // get total sum divided by three
    int sum = 0;
    for ( auto &a : A ) {
        sum += a;
    }
    int W = sum / 3;
    // check if it is a multiple of 3
    if ( W * 3 != sum ) {
        return false;
    }

    std::vector<std::unordered_set<int>> possibleSets;
    std::unordered_set<int> ignoreSet;
    int ret = findSumSetsIt ( W, A, 3, ignoreSet );
    //const int ret = find3UniqueSets ( possibleSets, A.size() );
    
    
    // If there is only one way to create 3 sets then there will be at
    // least 3 possible sets. Actually this is wrong (there needs to
    // be a check for finding at 3 unique ones). An example of having
    // 3 subsets that do not include all elements is 2 1 1 1 4.
    return ret;
}

int main()
{
  int n;
    std::cin >> n;
    vector<int> A ( n );
    for ( size_t i = 0; i < A.size(); ++i ) {
        std::cin >> A [i];
    }
    std::cout << partition3 ( A ) << '\n';
}
