#include <iostream>
#include <vector>

using std::vector;

int optimal_weight ( int W, const vector<int> &w )
{
    // The subproblem is as follows:

    // Assume that value(w) is the largest possible value for weight w with n
    // options. Then either this does or doesn't include the nth option. Then
    // value(w,i) = max { value(w-wi,i-1) + vi, value(w, i-1) }, where i goes
    // from 1 to n. For the gold bar case the weight and the value are equal
    // because we want to optimize the weight.

    // rows index is types/options
    // column index is current capacity

    std::vector<std::vector<int>> values;
    values.push_back ( std::vector<int> ( W + 1, 0 ) );
    for ( std::size_t t = 1; t <= w.size(); ++t ) {
        values.push_back ( std::vector<int> ( W + 1, 0 ) );
        for ( int c = 1; c <= W; ++c ) {
            values [t][c] = values [t - 1][c];
            int wi = w [t - 1];
            int vi = wi;  // value is equal to weight
            if ( c < wi ) {
                continue;
            }
            int Vinc { values [t - 1][c - wi] + vi };
            if ( Vinc > values [t][c] ) {
                values [t][c] = Vinc;
            }
        }
    }

    return values [w.size()][W];
}

int main()
{
    int n, W;
    std::cin >> W >> n;
    vector<int> w ( n );
    for ( int i = 0; i < n; i++ ) {
        std::cin >> w [i];
    }
    std::cout << optimal_weight ( W, w ) << '\n';
}
