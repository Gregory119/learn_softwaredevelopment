#!/usr/bin/python3

def fibonacciFast(n):
    
    if n == 0:
        return 0

    res = 1
    if n == 1:
        return res

    prev = 0
    for i in range(2,n+1):
        temp = res
        res = res + prev
        prev = temp

    return res


n = int ( input() )
print("{}".format(fibonacciFast ( n )))
