#include <algorithm>
#include <random>
#include <vector>

#include <gtest/gtest.h>

int getFibLastDigitNaive ( int n )
{
    if ( n <= 1 )
        return n;

    int previous = 0;
    int current = 1;

    for ( int i = 0; i < n - 1; ++i ) {
        int tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
    }

    return current % 10;
}

short getFibLastDigitFast ( int n )
{
    if ( n <= 1 )
        return n;

    short prev_digit = 0;
    short curr_digit = 1;

    for ( int i = 0; i < n - 1; ++i ) {
        short tmp_prev_digit = prev_digit;
        prev_digit = curr_digit;
        curr_digit = ( tmp_prev_digit + curr_digit ) % 10;
    }

    return curr_digit;
}

TEST ( SuiteFibLastDigit, Edges )
{
    int n { 0 };
    EXPECT_EQ ( getFibLastDigitNaive ( n ), getFibLastDigitFast ( n ) );

    n = 45;
    EXPECT_EQ ( getFibLastDigitNaive ( n ), getFibLastDigitFast ( n ) );
}

TEST ( SuiteFibLastDigit, Simple )
{
    int n { 1 };
    EXPECT_EQ ( getFibLastDigitNaive ( n ), getFibLastDigitFast ( n ) );

    n = 2;
    EXPECT_EQ ( getFibLastDigitNaive ( n ), getFibLastDigitFast ( n ) );
}

TEST ( SuiteFibLastDigit, Stress )
{
    std::default_random_engine rand_engine;
    std::uniform_int_distribution<> dist ( 3, 45 );

    for ( std::size_t i = 0; i < 30; ++i ) {
        const int n { dist ( rand_engine ) };
        ASSERT_EQ ( getFibLastDigitNaive ( n ), getFibLastDigitFast ( n ) );
    }
}
