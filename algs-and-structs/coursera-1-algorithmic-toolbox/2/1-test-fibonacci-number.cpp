#include <random>
#include <vector>

#include <gtest/gtest.h>

int fibonacciSlow ( int n )
{
    if ( n <= 1 )
        return n;

    return fibonacciSlow ( n - 1 ) + fibonacciSlow ( n - 2 );
}

int fibonacciFast ( int n )
{
    // 0 <= n <= 45
    std::vector<int> fibs;
    fibs.reserve ( n + 1 );
    fibs.push_back ( 0 );
    fibs.push_back ( 1 );

    for ( std::size_t i = 2; i < n + 1; ++i ) {
        fibs.push_back ( fibs [i - 1] + fibs [i - 2] );
    }

    return fibs [n];
}

TEST ( SuiteFibonacci, Edges )
{
    int n { 0 };
    EXPECT_EQ ( fibonacciSlow ( n ), fibonacciFast ( n ) );

    n = 45;
    EXPECT_EQ ( fibonacciSlow ( n ), fibonacciFast ( n ) );
}

TEST ( SuiteFibonacci, Simple )
{
    int n { 1 };
    EXPECT_EQ ( fibonacciSlow ( n ), fibonacciFast ( n ) );

    n = 2;
    EXPECT_EQ ( fibonacciSlow ( n ), fibonacciFast ( n ) );
}

TEST ( SuiteFibonacci, Stress )
{
    std::size_t iterations { 35 };

    // fill data with random numbers within constraints
    std::default_random_engine rand_engine;
    std::uniform_int_distribution<int> uni_dist ( 0, iterations );

    for ( std::size_t i = 0; i < iterations; ++i ) {
        int n { uni_dist ( rand_engine ) };
        ASSERT_EQ ( fibonacciSlow ( n ), fibonacciFast ( n ) );
    }
}
