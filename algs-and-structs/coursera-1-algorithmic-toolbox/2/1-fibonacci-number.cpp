#include <cassert>
#include <iostream>
#include <vector>

int fibonacciFast ( int n )
{
    // 0 <= n <= 45
    std::vector<int> fibs;
    fibs.reserve ( n + 1 );
    fibs.push_back ( 0 );
    fibs.push_back ( 1 );

    for ( int i = 2; i < n + 1; ++i ) {
        fibs.push_back ( fibs [i - 1] + fibs [i - 2] );
    }

    return fibs [n];
}

int main()
{
    int n = 0;
    std::cin >> n;

    std::cout << fibonacciFast ( n ) << std::endl;
    return 0;
}
