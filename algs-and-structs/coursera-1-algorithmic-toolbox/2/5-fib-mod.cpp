#include <iostream>
#include <vector>

// 1 <= i <= 10^18
// 2 <= m <= 1000
int fibModFast ( long long i, int m )
{
    // Find size of Pisano period, pi(m) for m
    // If m = 1000 => size of Pisano period is 1500 => need to calculate the
    // 1500th Fibonacci number. We can calculate the entire fibonacci modulus m
    // sequence by only using the results of fibonacci modulus m
    std::vector<int> fibs_mod_m;
    fibs_mod_m.push_back ( 0 );
    fibs_mod_m.push_back ( 1 );

    while ( 1 ) {
        int last_index = fibs_mod_m.size() - 1;
        fibs_mod_m.push_back (
            ( fibs_mod_m [last_index] + fibs_mod_m [last_index - 1] ) % m );

        // Check for start of new period by looking for 0,1,1
        last_index = fibs_mod_m.size() - 1;
        if ( fibs_mod_m.size() > 3 && fibs_mod_m [last_index] == 1
             && fibs_mod_m [last_index - 1] == 1
             && fibs_mod_m [last_index - 2] == 0 ) {
            fibs_mod_m.erase ( fibs_mod_m.end() - 3, fibs_mod_m.end() );
            break;
        }
    }

    // Fib(i) modulus m = Fib(i % pi(m)) % m
    // So use "i % pi(m)" as an index into the first period of Fib(i) modulus m.
    return fibs_mod_m [i % fibs_mod_m.size()];
}

int main()
{
    long long a = 0;
    int b = 0;
    std::cin >> a >> b;

    std::cout << fibModFast ( a, b ) << std::endl;
    return 0;
}
