#include <random>
#include <vector>

#include <gtest/gtest.h>

// 0 <= n <= 45
int fibFast ( int n )
{
    // Memoization implementation
    int prev = 0;
    int fib = 1;

    for ( std::size_t i = 2; i <= n; ++i ) {
        int fib_temp = fib;
        fib = fib + prev;
        prev = fib_temp;
    }

    return fib;
}

int fibModSlow ( int i, int m )
{
    int fib = fibFast ( i );
    return fib % m;
}

// 1 <= i <= 10^18
// 2 <= m <= 1000
int fibModFast ( long long i, int m )
{
    // Find size of Pisano period, pi(m) for m
    // If m = 1000 => size of Pisano period is 1500 => need to calculate the
    // 1500th Fibonacci number. We can calculate the entire fibonacci modulus m
    // sequence by only using the results of fibonacci modulus m
    std::vector<int> fibs_mod_m;
    fibs_mod_m.push_back ( 0 );
    fibs_mod_m.push_back ( 1 );

    while ( 1 ) {
        int last_index = fibs_mod_m.size() - 1;
        fibs_mod_m.push_back (
            ( fibs_mod_m [last_index] + fibs_mod_m [last_index - 1] ) % m );

        // Check for start of new period by looking for 0,1,1
        last_index = fibs_mod_m.size() - 1;
        if ( fibs_mod_m.size() > 3 && fibs_mod_m [last_index] == 1
             && fibs_mod_m [last_index - 1] == 1
             && fibs_mod_m [last_index - 2] == 0 ) {
            fibs_mod_m.erase ( fibs_mod_m.end() - 3, fibs_mod_m.end() );
            break;
        }
    }

    // Fib(i) modulus m = Fib(i % pi(m)) % m
    // So use "i % pi(m)" as an index into the first period of Fib(i) modulus m.
    return fibs_mod_m [i % fibs_mod_m.size()];
}

TEST ( SuiteFibMod, Edges )
{
    long long num1 = 45;
    int num2 = 1000;
    EXPECT_EQ ( 170, fibModFast ( num1, num2 ) );
    
    num1 = 45;
    num2 = 2;
    EXPECT_EQ ( 0, fibModFast ( num1, num2 ) );

    num1 = 2;
    num2 = 1000;
    EXPECT_EQ ( 1, fibModFast ( num1, num2 ) );

    num1 = 1;
    num2 = 2;
    EXPECT_EQ ( 1, fibModFast ( num1, num2 ) );
}

TEST ( SuiteFibMode, Simple )
{
    int num1 = 4;
    int num2 = 3;
    EXPECT_EQ ( 0, fibModFast ( num1, num2 ) );

    num1 = 5;
    num2 = 3;
    EXPECT_EQ ( 2, fibModFast ( num1, num2 ) );
}

TEST ( SuiteFibMode, Stress )
{
    std::default_random_engine randEng;
    // std::uniform_int_distribution<long long>
    // distFib(1,1000000000000000000); // This will take forever with the
    // naive solution.
    std::uniform_int_distribution<long long> distFib ( 1, 45 );
    std::uniform_int_distribution<int> distMod ( 2, 1000 );

    for ( unsigned i = 0; i < 15; ++i ) {
        // Randomly generate data.
        long long num1 = distFib ( randEng );
        int num2 = distMod ( randEng );

        ASSERT_EQ ( fibModSlow ( num1, num2 ), fibModFast ( num1, num2 ) );
    }
}
