#!/usr/bin/python3

def getFibLastDigitFast(n):
    if n < 2:
        return n

    prev = 0
    res = 1
    for i in range(2, n+1):
        tmp = res
        res = (res + prev) % 10
        prev = tmp

    return res

n = int ( input() )
print("{}".format(getFibLastDigitFast(n)))
