#include <iostream>

int getGcdFastIter ( int a, int b )
{
    while ( b != 0 ) {
        int a_temp = a;
        a = b;
        b = a_temp % b;
    }
    return a;
}

long long getLcmFast ( int a, int b )
{
    return static_cast<long long> ( a / getGcdFastIter ( a, b ) ) * b;
}

int main()
{
    int a, b;
    std::cin >> a >> b;

    std::cout << getLcmFast ( a, b ) << std::endl;
    return 0;
}
