#include <random>

#include <gtest/gtest.h>

long long getLcmNaive ( int a, int b )
{
    // Starting from the max of a and b,
    //   Find multiples of this max up to a*b
    //   For each multiple, check if the minimum of a and b is a factor => found
    //   least common multiple
    int max = 0;
    int min = 0;
    if ( a > b ) {
        max = a;
        min = b;
    } else {
        max = b;
        min = a;
    }

    long long max_multi = max;
    long long max_lcm = static_cast<long long> ( a ) * b;
    while ( max_multi < max_lcm ) {
        if ( max_multi % min == 0 ) {
            return max_multi;
        }
        max_multi += max;
    }

    return max_lcm;
}

int getGcdFastIter ( int a, int b )
{
    while ( b != 0 ) {
        int a_temp = a;
        a = b;
        b = a_temp % b;
    }
    return a;
}

long long getLcmFast ( int a, int b )
{
    return static_cast<long long> ( a / getGcdFastIter ( a, b ) ) * b;
}

TEST ( SuiteLcm, Edges )
{
    int num1 = 2 * 1000 * 1000 * 1000;
    int num2 = 2 * 1000 * 1000 * 1000;
    EXPECT_EQ ( getLcmNaive ( num1, num2 ), getLcmFast ( num1, num2 ) );

    num1 = 1;
    num2 = 2 * 1000 * 1000 * 1000;
    EXPECT_EQ ( getLcmNaive ( num1, num2 ), getLcmFast ( num1, num2 ) );

    num1 = 2 * 1000 * 1000 * 1000;
    num2 = 1;
    EXPECT_EQ ( getLcmNaive ( num1, num2 ), getLcmFast ( num1, num2 ) );

    num1 = 1;
    num2 = 1;
    EXPECT_EQ ( getLcmNaive ( num1, num2 ), getLcmFast ( num1, num2 ) );
}

TEST ( SuiteLcm, Simple )
{
    int num1 = 4;
    int num2 = 3;
    EXPECT_EQ ( 12, getLcmFast ( num1, num2 ) );

    num1 = 12;
    num2 = 4;
    EXPECT_EQ ( 12, getLcmFast ( num1, num2 ) );

    num1 = 10;
    num2 = 20;
    EXPECT_EQ ( 20, getLcmFast ( num1, num2 ) );
}

TEST ( SuiteLcm, Stress )
{
    std::default_random_engine randEng;
    std::uniform_int_distribution<> dist ( 1, 10000000 );

    for ( unsigned i = 0; i < 15; ++i ) {
        int num1 = dist ( randEng );
        int num2 = dist ( randEng );
        ASSERT_EQ ( getLcmNaive ( num1, num2 ), getLcmFast ( num1, num2 ) );
    }
}
