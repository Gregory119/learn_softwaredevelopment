#include <iostream>

short getFibLastDigitFast ( int n )
{
    if ( n <= 1 )
        return n;

    short prev_digit = 0;
    short curr_digit = 1;

    for ( int i = 0; i < n - 1; ++i ) {
        short tmp_prev_digit = prev_digit;
        prev_digit = curr_digit;
        curr_digit = ( tmp_prev_digit + curr_digit ) % 10;
    }

    return curr_digit;
}

int main()
{
    int n;
    std::cin >> n;
    int c = getFibLastDigitFast ( n );
    std::cout << c << '\n';
}
