#include <algorithm>
#include <random>

#include <gtest/gtest.h>

int getGcdNaive ( int a, int b )
{
    int current_gcd = 1;
    for ( int d = 2; d <= a && d <= b; d++ ) {
        if ( a % d == 0 && b % d == 0 ) {
            if ( d > current_gcd ) {
                current_gcd = d;
            }
        }
    }
    return current_gcd;
}

int getGcdFast ( int a, int b )
{
    if ( b == 0 ) {
        return a;
    }

    return getGcdFast ( b, a % b );
}

int getGcdFastIter ( int a, int b )
{
    while ( b != 0 ) {
        int a_temp = a;
        a = b;
        b = a_temp % b;
    }
    return a;
}

TEST ( SuiteGcd, Edges )
{
    int num1 = 2 * 1000 * 1000 * 1000;
    int num2 = 2 * 1000 * 1000 * 1000;
    EXPECT_EQ ( getGcdNaive ( num1, num2 ), getGcdFastIter ( num1, num2 ) );

    num1 = 1;
    num2 = 2 * 1000 * 1000 * 1000;
    EXPECT_EQ ( 1, getGcdFastIter ( num1, num2 ) );

    num1 = 2 * 1000 * 1000 * 1000;
    num2 = 1;
    EXPECT_EQ ( 1, getGcdFastIter ( num1, num2 ) );

    num1 = 1;
    num2 = 1;
    EXPECT_EQ ( 1, getGcdFastIter ( num1, num2 ) );
}

TEST ( SuiteGcd, Simple )
{
    int num1 = 4;
    int num2 = 3;
    EXPECT_EQ ( 1, getGcdFastIter ( num1, num2 ) );

    num1 = 12;
    num2 = 4;
    EXPECT_EQ ( 4, getGcdFastIter ( num1, num2 ) );

    num1 = 10;
    num2 = 20;
    EXPECT_EQ ( 10, getGcdFastIter ( num1, num2 ) );
}

TEST ( SuiteGcd, Stress )
{
    std::default_random_engine randEng;
    std::uniform_int_distribution<> dist ( 1, 2000000000 );

    for ( std::size_t i = 0; i < 7; ++i ) {
        const int num1 { dist ( randEng ) };
        const int num2 { dist ( randEng ) };
        ASSERT_EQ ( getGcdNaive ( num1, num2 ), getGcdFastIter ( num1, num2 ) );
    }
}
