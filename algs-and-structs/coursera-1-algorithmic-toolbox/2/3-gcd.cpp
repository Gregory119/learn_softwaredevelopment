#include <cassert>
#include <functional>
#include <iostream>

int getGcdFastIter ( int a, int b )
{
    while ( b != 0 ) {
        int a_temp = a;
        a = b;
        b = a_temp % b;
    }
    return a;
}

int main()
{
    int a, b;
    std::cin >> a >> b;

    std::cout << getGcdFastIter ( a, b ) << std::endl;
    return 0;
}
