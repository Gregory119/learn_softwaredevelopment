#!/usr/bin/env python3

import sys

def maxPairwiseProduct ( data ):
    # find first max
    iMax1 = 0
    max1 = 0
    i = 0
    for d in data:
        if d > max1:
            max1 = d
            iMax1 = i
        i += 1
        
    # find second max
    iMax2 = 0
    max2 = 0
    i = 0
    for d in data:
        if (d > max2) and (i != iMax1):
            max2 = d
            iMax2 = i
        i += 1

    return max1 * max2

n = input()
dataStr = input()
data = [int(i) for i in dataStr.split()]
print("{}".format(maxPairwiseProduct ( data )))
