#include <iostream>

#include <algorithm>
#include <vector>

long long maxPairwiseProduct ( std::vector<int> A )
{
    // A has n elements.
    // 2 <= n <= 2*10^5 => not empty with at least two elements < 2^31-1 (int)
    // The elements are a_1, a_2,...,a_n
    // 0 <= a_1, a_2,...,a_n <= 2*10^5
    // => max output could be 4*10^10 > 2^32-1 (unsigned int)
    // 4*10^10 > 2^63-1 (long long or int64_t)
    // The elements of the product pair can have different indices with the same
    // element value, but cannot have the same index.

    long long first_max = A [0];
    long long second_max = A [1];
    if ( second_max > first_max ) {
        std::swap ( first_max, second_max );
    }

    for ( std::size_t i = 2; i < A.size(); ++i ) {
        if ( A [i] >= first_max ) {
            second_max = first_max;
            first_max = A [i];
        } else if ( A [i] > second_max ) {
            second_max = A [i];
        }
    }

    return first_max * second_max;
}

int main()
{
    // parse num elements
    unsigned n { 0 };
    std::cin >> n;

    // parse elements
    std::vector<int> data;
    data.reserve ( n );
    for ( unsigned i = 0; i < n; ++i ) {
        int num { 0 };
        std::cin >> num;
        data.push_back ( num );
    }

    std::cout << maxPairwiseProduct ( data ) << std::endl;
    
    return 0;
};
