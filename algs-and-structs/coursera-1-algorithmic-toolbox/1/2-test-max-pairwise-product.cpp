#include <random>

#include <gtest/gtest.h>

#include <algorithm>
#include <vector>

static const int g_min_elements { 2 };
static const int g_max_elements { 2 * 100000 };
static const int g_element_max { 2 * 100000 };
static const int g_element_min { 0 };

long long correctMaxPairwiseProduct ( std::vector<int> A )
{
    std::sort ( A.begin(), A.end() );
    return A [A.size() - 1] * static_cast<long long> ( A [A.size() - 2] );
}

long long maxPairwiseProduct ( std::vector<int> A )
{
    // A has n elements.
    // 2 <= n <= 2*10^5 => not empty with at least two elements < 2^31-1 (int)
    // The elements are a_1, a_2,...,a_n
    // 0 <= a_1, a_2,...,a_n <= 2*10^5
    // => max output could be 4*10^10 > 2^32-1 (unsigned int)
    // 4*10^10 > 2^63-1 (long long or int64_t)
    // The elements of the product pair can have different indices with the same
    // element value, but cannot have the same index.

    long long first_max = A [0];
    long long second_max = A [1];
    if ( second_max > first_max ) {
        std::swap ( first_max, second_max );
    }

    for ( std::size_t i = 2; i < A.size(); ++i ) {
        if ( A [i] >= first_max ) {
            second_max = first_max;
            first_max = A [i];
        } else if ( A [i] > second_max ) {
            second_max = A [i];
        }
    }

    return first_max * second_max;
}

template <class Ret, class T>
void expectFuncsEq ( std::function<Ret ( T )> leftFunc,
                     std::function<Ret ( T )> rightFunc,
                     const std::vector<T> &inputs )
{
    for ( const auto &input : inputs ) {
        EXPECT_EQ ( leftFunc ( input ), rightFunc ( input ) );
    }
}

TEST ( SuiteMaxPairwiseProduct, Edges )
{
    std::vector<std::vector<int>> data {
        { 1, 2 },    { 2, 1 },
        { 2, 2 },    { 2, 2, 2 },
        { 0, 0, 0 }, { 1, 0, 2, 5, g_element_max, 0, g_element_max }
    };
    expectFuncsEq<long long, std::vector<int>> ( correctMaxPairwiseProduct,
                                                 maxPairwiseProduct,
                                                 data );
}

TEST ( SuiteMaxPairwiseProduct, Simple )
{
    std::vector<std::vector<int>> data { { 0, 2, 1 },
                                         { 2, 1, 2 },
                                         { 2, 1, 2, 4 },
                                         { 2, 1, 4, 2, 4 } };
    expectFuncsEq<long long, std::vector<int>> ( correctMaxPairwiseProduct,
                                                 maxPairwiseProduct,
                                                 data );
}

TEST ( SuiteMaxPairwiseProduct, MaxNumberOfElements )
{
    std::vector<std::vector<int>> data;
    std::vector<int> data1;
    data.reserve ( g_max_elements );

    // fill data with random numbers within constraints
    std::default_random_engine rand_engine;
    std::uniform_int_distribution<int> uni_dist ( g_element_min,
                                                  g_element_max );

    for ( int i = 0; i < g_max_elements; ++i ) {
        data1.push_back ( uni_dist ( rand_engine ) );
    }
    data.push_back ( std::move ( data1 ) );

    expectFuncsEq<long long, std::vector<int>> ( correctMaxPairwiseProduct,
                                                 maxPairwiseProduct,
                                                 data );
}

TEST ( SuiteMaxPairwiseProduct, Stress )
{
    // Stress test should be deterministic (output must be unique to
    // input). Random data should be psuedorandom so that the same "random"
    // input data will be generated (deterministic - same output for same
    // input). Psuedorandom means that each element in the range of
    // possibilities has the same likelyhood/probability of appearing, and each
    // of these numbers appears in a repeatable sequence.

    std::size_t iterations { 100 };

    std::default_random_engine rand_engine;
    std::uniform_int_distribution<int> uni_dist_num_elements ( g_min_elements,
                                                               g_max_elements );
    std::uniform_int_distribution<int> uni_dist_element ( g_element_min,
                                                          g_element_max );

    std::size_t num_elements { 0 };

    std::vector<std::vector<int>> data;
    for ( std::size_t i = 0; i < iterations; ++i ) {
        // Randomly generate length of data.
        num_elements = uni_dist_num_elements ( rand_engine );
        std::vector<int> data1;
        data1.reserve ( num_elements );

        // Fill data with random numbers within constraints.
        for ( int j = 0; j < num_elements; ++j ) {
            data1.push_back ( uni_dist_element ( rand_engine ) );
        }

        data.push_back ( std::move ( data1 ) );
    }

    expectFuncsEq<long long, std::vector<int>> ( correctMaxPairwiseProduct,
                                                 maxPairwiseProduct,
                                                 data );
}
