#include <algorithm>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <random>
#include <vector>

//----------------------------------------------------------------------//
void writeTestDataToFile(const std::string& test_name,
			 long long num1)
{
  static int test_number = 0;
  ++test_number;

  std::ostringstream filename;
  filename << "input_data_test"
	   << std::setfill('0')
	   << std::setw(5)
	   << test_number
	   << "_"
	   << test_name
	   << ".txt";

  std::ofstream test_data_file(filename.str());
  test_data_file << num1 << std::endl;
}

//----------------------------------------------------------------------//
void writeStressTest(unsigned iterations)
{
  std::default_random_engine rand_engine;
  //std::uniform_int_distribution<long long> uni_dist_fib(1,1000000000000000000); // This will take forever with the naive solution.
  std::uniform_int_distribution<long long> uni_dist_fib1(0,45);

  for (unsigned i=0; i<iterations; ++i)
    {
      // Randomly generate data.
      long long num1 = uni_dist_fib1(rand_engine);

      writeTestDataToFile("stress",num1);
    }
}

//----------------------------------------------------------------------//
int main(int argc, char* argv[])
{
  //======================================================================
  // Edge Cases
  //======================================================================
  long num = 0;
  writeTestDataToFile("edge",num);

  num = 1;
  writeTestDataToFile("edge",num);

  //======================================================================
  // Simple Cases
  //======================================================================
  num = 2;
  writeTestDataToFile("simple",num);

  num = 3;
  writeTestDataToFile("simple",num);

  num = 4;
  writeTestDataToFile("simple",num);

  num = 5;
  writeTestDataToFile("simple",num);

  num = 6;
  writeTestDataToFile("simple",num);
  
  //======================================================================
  // Stress Test
  //======================================================================
  writeStressTest(15);
  
  return 0;
}
