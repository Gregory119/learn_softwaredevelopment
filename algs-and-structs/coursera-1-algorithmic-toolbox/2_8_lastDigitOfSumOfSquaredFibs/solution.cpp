#include <iostream>
#include <functional>
#include <cassert>
#include <cmath>
#include <vector>

//----------------------------------------------------------------------//
// 0 <= n <= 30
int lastDigitOfSumOfSquaredFibsSlow(long long n)
{
  if (n <= 1)
    return n;

  long long previous = 0;
  long long current  = 1;
  long long sum      = 1;

  for (long long i = 0; i < n - 1; ++i) {
    long long tmp_previous = previous;
    previous = current;
    current = tmp_previous + current;
    sum += current * current;
  }

  return sum % 10;
}

//----------------------------------------------------------------------//
// 1 <= n <= 10^18
int lastDigitOfSumOfSquaredFibsFast(long long n)
{
  // Find size of Pisano period, pi(m) for m
  // If m = 1000 => size of Pisano period is 1500 => need to calculate the 1500th Fibonacci number.
  // We can calculate the entire fibonacci modulus m sequence by only using the results of fibonacci modulus m
  long long m = 10;
  std::vector<int> fibs_mod_m;
  fibs_mod_m.push_back(0);
  fibs_mod_m.push_back(1);
  
  while (1)
    {
      int last_index = fibs_mod_m.size()-1;
      fibs_mod_m.push_back((fibs_mod_m[last_index]+fibs_mod_m[last_index-1]) % m);

      // Check for start of new period by looking for 0,1,1
      last_index = fibs_mod_m.size()-1;
      if (fibs_mod_m.size() > 3 &&
	  fibs_mod_m[last_index] == 1 &&
	  fibs_mod_m[last_index-1] == 1 &&
	  fibs_mod_m[last_index-2] == 0)
	{
	  fibs_mod_m.erase(fibs_mod_m.end()-3,fibs_mod_m.end());
	  break;
	}
    }
  
  
  // Fib(i) modulus m = Fib(i % pi(m)) % m
  // So use "i % pi(m)" as an index into the first period of Fib(i) modulus m.
  return (fibs_mod_m[(n+1) % fibs_mod_m.size()] * fibs_mod_m[n % fibs_mod_m.size()]) % m;
}

//----------------------------------------------------------------------//
void test(std::function<int(long long)> sol)
{
  //======================================================================
  // Edge Cases
  //======================================================================
  long num = 0;
  assert(sol(num) == 0);

  num = 1;
  assert(sol(num) == 1);

  //======================================================================
  // Simple Cases
  //======================================================================
  num = 2;
  assert(sol(num) == 2);

  num = 3;
  assert(sol(num) == 6);

  num = 4;
  assert(sol(num) == 5); // sum of 15 => last digit of 5

  num = 5;
  assert(sol(num) == 0); // sum of 40 => last digit of 0

  num = 6;
  assert(sol(num) == 4); // sum of 104 => last digit of 4
}

//----------------------------------------------------------------------//
int main() {
  long long n = 0;
  std::cin >> n;

  //test(lastDigitOfSumOfSquaredFibsSlow);
  //test(lastDigitOfSumOfSquaredFibsFast);
  
  std::cout << lastDigitOfSumOfSquaredFibsFast(n) << std::endl;
  return 0;
}
