#include <algorithm>
#include <iostream>
#include <regex>
#include <vector>

//----------------------------------------------------------------------//
// 0 <= n,m <= 45
int slowCorrectSolution(long long n)
{
  if (n <= 1)
    return n;

  long long previous = 0;
  long long current  = 1;
  long long sum      = 1;

  for (long long i = 0; i < n - 1; ++i) {
    long long tmp_previous = previous;
    previous = current;
    current = tmp_previous + current;
    sum += current * current;
  }

  return sum % 10;
}

//----------------------------------------------------------------------//
double getExecutionTimeSec(const std::string& times)
{
  std::smatch matches;
  std::regex reg("real=([0-9]+\\.[0-9]+).*sys=([0-9]+\\.[0-9]+).*user=([0-9]+\\.[0-9]+).*"); // Capture the different decimal point times.
  std::regex_search(times,matches,reg);
  //matches.str(1) - real
  //matches.str(2) - system
  //matches.str(3) - user

  double time_sec = 0.0;
  double system_sec = std::stod(matches.str(2));
  double user_sec = std::stod(matches.str(3));
  time_sec = system_sec+user_sec;
  
  return time_sec;
}

//----------------------------------------------------------------------//
int main(int argc, char* argv[])
{
  // Parameter is the minimum time.
  if (argc != 2)
    {
      std::cout << "Please enter the minimum time in seconds (decimal point) as a parameter." << std::endl;
      return 0;
    }

  double min_time_sec = 0;

  try {
    min_time_sec = std::stod(argv[argc-1]);
  }
  catch (...)
    {
      std::cout << "The minimum time is not valid." << std::endl;
      return 0;
    }
  
  // Parse input from solution.
  // Format: input data, solution data, processing time
  long long num1 = 0;
  std::cin >> num1;

  // Solution output
  int result = 0;
  std::cin >> result;

  // Processing time (in decimal point seconds)
  // real=.. sys=.. user=..
  std::string times;
  while (times.empty())
    {
      std::getline(std::cin,times);
    }
  double time_sec =0.0;
  try
    {
      time_sec = getExecutionTimeSec(times);
    }
  catch (...)
    {
      std::cout << "Failed to extract execution time." << std::endl;
      return 0;
    }

  // Check correctness of calculated output
  auto correct = slowCorrectSolution(num1);

  std::cout << "data = " << num1 << std::endl;
	  
  // Stop on a failure or after the iteration limit.
  std::cout << "correct result = " << correct << std::endl
	    << "solution result = " << result << std::endl
	    << "minimum time [sec] = " << min_time_sec << std::endl
	    << "solution time [sec] = " << time_sec << std::endl;
  
  if (result != correct || time_sec > min_time_sec)
    {
      std::cout << "FAIL" << std::endl;
    }
  else
    {
      std::cout << "PASS" << std::endl;
    }
  std::cout << std::endl;
  
  return 0;
}
