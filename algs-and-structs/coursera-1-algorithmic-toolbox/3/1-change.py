#!/usr/bin/python3

def getChange ( m ):
    coinTypes = [10, 5, 1]
    count = 0
    for coinT in coinTypes:
        q = int ( m / coinT )
        m -= q * coinT
        count += q

    return count

m = int ( input() )
print ( "{}".format(getChange(m)))
