#!/usr/bin/python3

import sys

def minRefills ( dist, tank, stops ):
    refills = 0
    currDist = 0
    nextStopI = 0
    while dist > currDist:
        move = 0
        remainingDist = dist - currDist
        if remainingDist <= tank:
            move = remainingDist
        else:
            distToStopFn = lambda : stops[nextStopI] - currDist
            distToStop = distToStopFn()
            while distToStop <= tank:
                move = distToStop

                if nextStopI == len(stops) - 1:
                    break

                nextStopI += 1
                distToStop = distToStopFn()

            if move > 0:
                refills += 1

        if move == 0:
            return -1

        currDist += move

    return refills

def main():
    dist = int(sys.stdin.readline())
    tank = int(sys.stdin.readline())
    sys.stdin.readline() # ignore number of stops
    stops = [int(x) for x in sys.stdin.readline().split()]
    print("{}".format(minRefills(dist, tank, stops)))
        

if __name__ == "__main__":
    main()
