#include <random>
#include <set>

#include <gtest/gtest.h>

int getChangeNaive ( int m )
{
    // time complexity: O ( m^3 )
    // for each multiplier
    //   from 0 to m
    //     if sum == m && numCoins < minCoins:
    //       minCoins = numCoins
    // return minCoins

    int minCount { m * 3 };
    for ( int i = 0; i <= m; ++i ) {
        for ( int j = 0; j <= m; ++j ) {
            for ( int k = 0; k <= m; ++k ) {
                int sum { i * 10 + j * 5 + k * 1 };
                int count { i + j + k };
                if ( ( sum == m ) && ( count < minCount ) ) {
                    minCount = count;
                }
            }
        }
    }

    return minCount;
}

int getChange ( int m )
{
    // time complexity: O ( 1 )
    // For each coin type
    //   - Divide by current coin type and use quotient as count for type (safe
    //   move)
    //   - Increment total count

    std::set<int, std::greater<int>> coinTypes { 10, 5, 1 };
    int count { 0 };
    for ( const auto &coinT : coinTypes ) {
        int q { m / coinT };
        m -= q * coinT;
        count += q;
    }

    return count;
}

TEST ( SuiteChange, Edges )
{
    EXPECT_EQ ( 0, getChange ( 0 ) );
    EXPECT_EQ ( 100, getChange ( 1000 ) );
}

TEST ( SuiteChange, Simple )
{
    EXPECT_EQ ( 1, getChange ( 1 ) );

    // 2 * coin 1
    EXPECT_EQ ( 2, getChange ( 2 ) );

    // 3 * coin 1
    EXPECT_EQ ( 3, getChange ( 3 ) );

    // 4 * coin 1
    EXPECT_EQ ( 4, getChange ( 4 ) );

    // 1 * coin 5
    EXPECT_EQ ( 1, getChange ( 5 ) );

    // 1 * coin 5 + 1 * coin 1
    EXPECT_EQ ( 2, getChange ( 6 ) );

    // 1 * coin 5 + 2 * coin 1
    EXPECT_EQ ( 3, getChange ( 7 ) );

    // 1 * coin 5 + 3 * coin 1
    EXPECT_EQ ( 4, getChange ( 8 ) );

    // 1 * coin 5 + 4 * coin 1
    EXPECT_EQ ( 5, getChange ( 9 ) );

    // 1 * coin 10
    EXPECT_EQ ( 1, getChange ( 10 ) );

    // 1 * coin 10 + 1 * coin 1
    EXPECT_EQ ( 2, getChange ( 11 ) );
}

TEST ( SuiteChange, Stress )
{
    std::default_random_engine eng;
    std::uniform_int_distribution<> dist ( 1, 100 );

    for ( std::size_t i = 0; i < 15; ++i ) {
        int m { dist ( eng ) };
        ASSERT_EQ ( getChangeNaive ( m ), getChange ( m ) ) << "m: " << m;
    }
}
