#include <algorithm>
#include <iostream>
#include <vector>

struct WeightVal
{
    int weight;
    int value;
    double fracVal;
};

double getOptimalVal ( int capacity,
                       const std::vector<int> &weights,
                       const std::vector<int> &values )
{
    // find fractional values (value/weight)
    // order highest to lowest
    // for each fractional value
    //   take up to max weight or the remaining space
    //   add to total value

    std::vector<WeightVal> weightVals;
    for ( std::size_t i = 0; i < weights.size(); ++i ) {
        weightVals.push_back ( WeightVal {
            weights.at ( i ),
            values.at ( i ),
            static_cast<double> ( values.at ( i ) ) / weights.at ( i ) } );
    }

    std::sort ( weightVals.begin(),
                weightVals.end(),
                [] ( const WeightVal &lhs, const WeightVal &rhs ) {
                    return lhs.fracVal > rhs.fracVal;
                } );

    double value = 0.0;
    for ( const auto &weightVal : weightVals ) {
        if ( capacity == 0 ) {
            break;
        }

        int w { std::min ( capacity, weightVal.weight ) };
        capacity -= w;
        value += weightVal.fracVal * w;
    }

    return value;
}

int main()
{
    int n;
    int capacity;
    std::cin >> n >> capacity;
    std::vector<int> values ( n );
    std::vector<int> weights ( n );
    for ( int i = 0; i < n; i++ ) {
        std::cin >> values [i] >> weights [i];
    }

    double optimal_value = getOptimalVal ( capacity, weights, values );

    std::cout.precision ( 10 );
    std::cout << optimal_value << std::endl;
    return 0;
}
