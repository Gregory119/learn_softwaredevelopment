#include <algorithm>

#include <gtest/gtest.h>

struct WeightVal
{
    int weight;
    int value;
    double fracVal;
};

double getOptimalVal ( int capacity,
                       const std::vector<int> &weights,
                       const std::vector<int> &values )
{
    // find fractional values (value/weight)
    // order highest to lowest
    // for each fractional value
    //   take up to max weight or the remaining space
    //   add to total value

    std::vector<WeightVal> weightVals;
    for ( std::size_t i = 0; i < weights.size(); ++i ) {
        weightVals.push_back ( WeightVal {
            weights.at ( i ),
            values.at ( i ),
            static_cast<double> ( values.at ( i ) ) / weights.at ( i ) } );
    }

    std::sort ( weightVals.begin(),
                weightVals.end(),
                [] ( const WeightVal &lhs, const WeightVal &rhs ) {
                    return lhs.fracVal > rhs.fracVal;
                } );

    double value = 0.0;
    for ( const auto &weightVal : weightVals ) {
        if ( capacity == 0 ) {
            break;
        }

        int w { std::min ( capacity, weightVal.weight ) };
        capacity -= w;
        value += weightVal.fracVal * w;
    }

    return value;
}

TEST ( FracKnap, Edges )
{
    // zero capacity
    std::vector<int> weights { 1, 2, 3 };
    std::vector<int> values { 15, 10, 3 };
    int capacity { 0 };
    EXPECT_FLOAT_EQ ( 0, getOptimalVal ( capacity, weights, values ) );

    // single value-weight
    weights = { 2 };
    values = { 15 };
    capacity = 60;
    EXPECT_FLOAT_EQ ( 15, getOptimalVal ( capacity, weights, values ) );

    // capacity smaller than any weight
    weights = { 2, 3, 4 };
    values = { 15, 10, 3 };
    capacity = 1;
    EXPECT_FLOAT_EQ ( 7.5, getOptimalVal ( capacity, weights, values ) );

    // values of only zero
    weights = { 2, 3, 4 };
    values = { 0, 0, 0 };
    capacity = 10;
    EXPECT_FLOAT_EQ ( 0, getOptimalVal ( capacity, weights, values ) );

    // all equal weights
    weights = { 5, 5, 5 };
    values = { 20, 10, 5 };
    capacity = 10;
    EXPECT_FLOAT_EQ ( 30, getOptimalVal ( capacity, weights, values ) );

    // min and max weight
    weights = { 1000000, 9, 1 };
    values = { 20, 10, 5 };
    capacity = 10;
    EXPECT_FLOAT_EQ ( 15, getOptimalVal ( capacity, weights, values ) );
}

TEST ( FracKnap, Simple )
{
    // weights less than capacity
    std::vector<int> weights { 1, 2, 3 };
    std::vector<int> values { 15, 10, 3 };
    int capacity { 15 };
    EXPECT_FLOAT_EQ ( 28, getOptimalVal ( capacity, weights, values ) );

    // weights more than capacity
    weights = { 5, 10, 15 };
    values = { 15, 10, 5 };
    capacity = 15;
    EXPECT_FLOAT_EQ ( 25, getOptimalVal ( capacity, weights, values ) );
}
