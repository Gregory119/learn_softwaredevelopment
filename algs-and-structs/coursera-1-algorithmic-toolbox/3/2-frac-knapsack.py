#!/usr/bin/python3

import sys

class WeightVal:
    def __init__(self, weight, value):
        self.weight = int ( weight )
        self.value = int ( value )
        self.fracVal = self.value / self.weight
              
def getOptimalValue(capacity, weights, values):
    weightVals=[]
    for val, weight in zip ( values, weights ):
        weightVals.append ( WeightVal ( weight, val ) )

    sorted ( weightVals, key = lambda x: x.fracVal, reverse = True )
    
    value = 0.
    for weightVal in weightVals:
        if capacity == 0:
            break

        w = min ( capacity, weightVal.weight )
        capacity -= w
        value += weightVal.fracVal * w
    
    return value

def main():
    n, capacity = [int(word) for word in sys.stdin.readline().split()]

    values=[]
    weights=[]
    for i in range(0,n):
        val, weight = [int(w) for w in sys.stdin.readline().split()]
        values.append ( val )
        weights.append ( weight )
        
    opt_value = getOptimalValue(capacity, weights, values)
    print("{:.10f}".format(opt_value))    

if __name__ == "__main__":
    main()
