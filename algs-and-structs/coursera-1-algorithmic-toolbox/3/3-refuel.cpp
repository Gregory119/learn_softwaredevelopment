#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::max;
using std::vector;

int minRefills ( int dist, int tank, std::vector<int> &stops )
{
    // continue until the distance is zero
    //   - look for the furthest stop less than what's left in the tank, if no
    //   stops exist then it's impossible
    //   - remove stops behind vehicle
    //   - substract moved distance from remaining dist

    int refills { 0 };
    int currDist { 0 };
    std::size_t nextStopI { 0 };
    while ( dist > currDist ) {
        int move { 0 };
        int remainingDist { dist - currDist };
        if ( remainingDist <= tank ) {
            move = remainingDist;
        } else {
            auto distToStopFn
                = [&]() { return stops.at ( nextStopI ) - currDist; };
            int distToStop { distToStopFn() };
            while ( distToStop <= tank ) {
                move = distToStop;

                if ( nextStopI == stops.size() - 1 ) {
                    // no more stops left so use it
                    break;
                }

                ++nextStopI;
                distToStop = distToStopFn();
            }

            if ( move > 0 ) {
                refills++;
            }
        }

        if ( move == 0 ) {
            // found no stops within tank distance => impossible
            return -1;
        }

        currDist += move;
    }

    return refills;
}

int main()
{
    int d = 0;
    cin >> d;
    int m = 0;
    cin >> m;
    int n = 0;
    cin >> n;

    vector<int> stops ( n );
    for ( size_t i = 0; i < n; ++i ) {
        cin >> stops.at ( i );
    }

    cout << minRefills ( d, m, stops ) << "\n";

    return 0;
}
