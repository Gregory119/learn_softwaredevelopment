#include <iostream>
#include <set>

int getChange ( int m )
{
    // time complexity: O ( 1 )
    // For each coin type
    //   - Divide by current coin type and use quotient as count for type (safe
    //   move)
    //   - Increment total count

    std::set<int, std::greater<int>> coinTypes { 10, 5, 1 };
    int count { 0 };
    for ( const auto &coinT : coinTypes ) {
        int q { m / coinT };
        m -= q * coinT;
        count += q;
    }

    return count;
}

int main()
{
    int m;
    std::cin >> m;
    std::cout << getChange ( m ) << '\n';
}
