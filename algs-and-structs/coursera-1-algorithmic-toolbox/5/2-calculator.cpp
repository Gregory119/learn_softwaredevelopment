#include <algorithm>
#include <array>
#include <iostream>
#include <vector>

using std::vector;

vector<int> optimal_sequence ( int n )
{
    std::vector<int> prevNums;
    prevNums.reserve ( n );
    prevNums.push_back ( -1 );
    std::vector<int> minOps;
    minOps.reserve ( n );
    minOps.push_back ( 0 );
    for ( std::size_t i = 1; i < minOps.capacity(); ++i ) {
        std::array<int, 3> prevOps { std::numeric_limits<int>::max(),  // x3
                                     std::numeric_limits<int>::max(),  // x2
                                     minOps [i - 1] };                 // +1

        int num = i + 1;
        if ( num % 3 == 0 ) {
            prevOps [0] = minOps [num / 3 - 1];
        }
        if ( num % 2 == 0 ) {
            prevOps [1] = minOps [num / 2 - 1];
        }
        auto minIt = std::min_element ( prevOps.begin(), prevOps.end() );
        minOps.push_back ( *minIt + 1 );
        if ( minIt == &prevOps [0] ) {
            prevNums.push_back ( num / 3 );
        } else if ( minIt == &prevOps [1] ) {
            prevNums.push_back ( num / 2 );
        } else {
            prevNums.push_back ( num - 1 );
        }
    }

    std::vector<int> sequence;
    sequence.push_back ( n );
    std::size_t i = minOps.size() - 1;
    while ( i > 0 ) {
        int prevNum = prevNums [i];
        sequence.push_back ( prevNum );
        i = prevNum - 1;
    }
    reverse ( sequence.begin(), sequence.end() );
    return sequence;
}

int main()
{
    int n;
    std::cin >> n;
    vector<int> sequence = optimal_sequence ( n );
    std::cout << sequence.size() - 1 << std::endl;
    for ( size_t i = 0; i < sequence.size(); ++i ) {
        std::cout << sequence [i] << " ";
    }
}
