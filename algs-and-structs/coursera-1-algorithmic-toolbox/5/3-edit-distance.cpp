#include <algorithm>
#include <array>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

using std::string;

int edit_distance ( const string &str1, const string &str2 )
{
    // create an E two dim array to hold the edit distance
    // for each array element
    //   find cost of each possible previous operation and pick minimum

    // str1 characters aligned with rows
    // str2 characters aligned with columns
    std::vector<std::vector<int>> E;
    E.reserve ( str1.size() + 1 );

    for ( std::size_t i = 0; i < E.capacity(); ++i ) {
        std::vector<int> row;
        row.reserve ( str2.size() + 1 );
        for ( std::size_t j = 0; j < row.capacity(); ++j ) {
            if ( i == 0 ) {
                row.push_back ( j );
                continue;
            }

            if ( j == 0 ) {
                row.push_back ( i );
                continue;
            }

            // insert, delete, match / mismatch
            std::array<int, 3> costs { std::numeric_limits<int>::max(),
                                       std::numeric_limits<int>::max(),
                                       std::numeric_limits<int>::max() };

            costs [0] = *row.rbegin() + 1;
            costs [1] = E [i - 1][j] + 1;
            if ( str1 [i-1] == str2 [j-1] ) {
                costs [2] = E [i - 1][j - 1];
            } else {  // not equal
                costs [2] = E [i - 1][j - 1] + 1;
            }

            row.push_back ( *std::min_element ( costs.begin(), costs.end() ) );
        }
        E.push_back ( std::move ( row ) );
    }

    return E [str1.size()][str2.size()];
}

int main()
{
    string str1;
    string str2;
    std::cin >> str1 >> str2;
    std::cout << edit_distance ( str1, str2 ) << std::endl;
    return 0;
}
