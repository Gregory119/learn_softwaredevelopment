#include <algorithm>
#include <array>
#include <iostream>
#include <limits>
#include <vector>

int get_change ( int m )
{
    // create array of size m
    // for each array index (number)
    //   determine the minimum number of coins based on min number of the past
    //   three options

    if ( m <= 0 ) {
        return 0;
    }

    std::vector<int> minChange;
    minChange.reserve ( m + 1 );
    minChange.push_back ( 0 );
    for ( int i = 1; i <= m; ++i ) {
        std::array<int, 3> change { std::numeric_limits<int>::max(),
                                    std::numeric_limits<int>::max(),
                                    std::numeric_limits<int>::max() };
        change [0] = minChange [i - 1];
        if ( i >= 3 ) {
            change [1] = minChange [i - 3];
        }
        if ( i >= 4 ) {
            change [2] = minChange [i - 4];
        }
        minChange.push_back (
            1 + *std::min_element ( change.begin(), change.end() ) );
    }
    return minChange [m];
}

int main()
{
    int m;
    std::cin >> m;
    std::cout << get_change ( m ) << '\n';
}
