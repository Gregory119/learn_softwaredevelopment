#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>

using std::vector;

int binary_search ( const vector<int> &a, int x )
{
    int left = 0, right = ( int )a.size();
    int size = right - left;
    int mid = left + size / 2;
    int midVal = a [mid];

    // edge cases
    // empty vector
    // left value
    // right value
    // move past left
    // move past right

    while ( midVal != x ) {
        // if size == 0 and not found then return -1
        if ( size == 0 ) {
            return -1;
        }

        // if x is <= mid val, then search lower half
        // else then search upper half
        if ( x < midVal ) {
            right = mid;
        } else {
            left = mid + 1;
        }

        // find mid m
        size = right - left;
        if ( size < 0 ) {
            return -1;
        }
        mid = left + size / 2;
        midVal = a [mid];
    }

    return mid;
}

int linear_search ( const vector<int> &a, int x )
{
    for ( size_t i = 0; i < a.size(); ++i ) {
        if ( a [i] == x )
            return i;
    }
    return -1;
}

int main()
{
    int n;
    std::cin >> n;
    vector<int> a ( n );
    for ( size_t i = 0; i < a.size(); i++ ) {
        std::cin >> a [i];
    }
    int m;
    std::cin >> m;
    vector<int> b ( m );
    for ( int i = 0; i < m; ++i ) {
        std::cin >> b [i];
    }

    for ( int i = 0; i < m; ++i ) {
        // replace with the call to binary_search when implemented
        std::cout << linear_search ( a, b [i] ) << ' ';
    }
}
