#include <algorithm>
#include <iostream>
#include <vector>

using std::vector;

bool isMaj ( const vector<int> &a, int left, int right, int num )
{
    int count { 0 };
    int size = right - left;
    for ( int i = left; i < right; ++i ) {
        if ( a [i] == num ) {
            ++count;
            if ( count > size / 2 ) {
                return true;
            }
        }
    }
    return false;
}

// A better alg. would be to first sort all the elements and then iterate
// through them counting the frequency of the current element until either
// its greater than n/2 or the current element differs from the previous
// element. However, the assignment indicates that a divide and conquer
// alg. must be designed and implemented.
int get_majority_element ( vector<int> &a, int left, int right )
{
    if ( left == right )
        return -1;
    if ( left + 1 == right )
        return a [left];

    int mid = ( left + right ) / 2;

    // has lhs majority
    //   scan combined majority
    int maj = get_majority_element ( a, left, mid );
    if ( maj != -1 ) {
        if ( isMaj ( a, left, right, maj ) ) {
            return maj;
        }
    }

    // has rhs majority
    //   scan combined majority
    maj = get_majority_element ( a, mid, right );
    if ( maj != -1 ) {
        if ( isMaj ( a, left, right, maj ) ) {
            return maj;
        }
    }

    return -1;
}

int main()
{
    int n;
    std::cin >> n;
    vector<int> a ( n );
    for ( size_t i = 0; i < a.size(); ++i ) {
        std::cin >> a [i];
    }
    std::cout << ( get_majority_element ( a, 0, a.size() ) != -1 ) << '\n';
}
