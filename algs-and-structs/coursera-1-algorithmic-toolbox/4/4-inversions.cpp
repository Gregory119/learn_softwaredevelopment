#include <algorithm>
#include <iostream>
#include <vector>

using std::vector;

long long merge_inversions ( vector<int> &a,
                             size_t left,
                             size_t mid,
                             size_t right )
{
    vector<int> leftVec ( &a [left], &a [mid] );
    vector<int> rightVec ( &a [mid], &a [right] );

    long long inv { 0 };
    // - Start with pos of 'a' at left
    vector<int>::iterator itA { &a[left] };

    vector<int>::const_iterator itL { leftVec.begin() };
    vector<int>::const_iterator itR { rightVec.begin() };

    // - Move through each vector and insert into the current pos of 'a' the
    //   larger number. If value from left is inserted, then add to inversion
    //   right the number of values currently in the right vector. Opposite case
    //   if value from right is inserted.
    while ( ( itL != leftVec.end() ) && ( itR != rightVec.end() ) ) {
        if ( *itL > *itR ) {
            *itA = *itL;
            inv += rightVec.end() - itR;
            ++itL;
        } else {
            *itA = *itR;
            ++itR;
        }
        ++itA;
    }

    if ( ( itL == leftVec.end() ) && ( itA != a.end() ) ) {
        std::copy ( itR, rightVec.cend(), itA );
    } else if ( ( itR == rightVec.end() ) && ( itA != a.end() ) ) {
        std::copy ( itL, leftVec.cend(), itA );
    }

    return inv;
}

long long get_number_of_inversions (
    vector<int> &a,
    size_t left,
    size_t right )
{
    long long inv = 0;
    if ( right <= left + 1 )
        return inv;
    size_t ave = left + ( right - left ) / 2;

    inv += get_number_of_inversions ( a, left, ave );
    inv += get_number_of_inversions ( a, ave, right );
    // get number of inversions between the two vectors and add to total
    inv += merge_inversions ( a, left, ave, right );

    return inv;
}

int main()
{
    int n;
    std::cin >> n;
    vector<int> a ( n );
    for ( size_t i = 0; i < a.size(); i++ ) {
        std::cin >> a [i];
    }
    std::cout << get_number_of_inversions ( a, 0, a.size() ) << '\n';
}
