#include <algorithm>
#include <iostream>
#include <queue>
#include <vector>

using std::cin;
using std::cout;
using std::vector;

class JobQueue
{
private:
    std::size_t num_workers_;
    vector<int> jobs_;

    vector<int> assigned_workers_;
    vector<long long> start_times_;

    void WriteResponse() const
    {
        for ( int i = 0; i < jobs_.size(); ++i ) {
            cout << assigned_workers_ [i] << " " << start_times_ [i] << "\n";
        }
    }

    void ReadData()
    {
        int m;
        cin >> num_workers_ >> m;
        jobs_.resize ( m );
        for ( int i = 0; i < m; ++i )
            cin >> jobs_ [i];
    }

    void AssignJobs()
    {
        assigned_workers_.reserve ( jobs_.size() );
        start_times_.reserve ( jobs_.size() );

        struct Worker
        {
            bool operator> ( const Worker &rhs ) const
            {
                return ( m_NextFreeTime > rhs.m_NextFreeTime )
                       || ( ( m_NextFreeTime == rhs.m_NextFreeTime )
                            && ( m_Index > rhs.m_Index ) );
            }

            std::size_t m_Index;
            long long m_NextFreeTime;
        };

        // initialize workers
        std::priority_queue<Worker, std::vector<Worker>, std::greater<Worker>>
            workers;
        for ( std::size_t i = 0; i < num_workers_; ++i ) {
            workers.push ( { i, 0 } );
        }

        // for each job
        //   find worker with the earliest free time
        //   start time of job is free time of worker
        //   update free time of worker with job duration
        for ( auto jobDur : jobs_ ) {
            auto nextWorker = workers.top();
            workers.pop();
            assigned_workers_.push_back ( nextWorker.m_Index );
            start_times_.push_back ( nextWorker.m_NextFreeTime );
            nextWorker.m_NextFreeTime += jobDur;
            workers.push ( nextWorker );
        }
    }

public:
    void Solve()
    {
        ReadData();
        AssignJobs();
        WriteResponse();
    }
};

int main()
{
    std::ios_base::sync_with_stdio ( false );
    JobQueue job_queue;
    job_queue.Solve();
    return 0;
}
