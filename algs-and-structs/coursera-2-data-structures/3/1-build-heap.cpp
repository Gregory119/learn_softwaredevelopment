#include <algorithm>
#include <iostream>
#include <queue>
#include <vector>

using std::cin;
using std::cout;
using std::make_pair;
using std::pair;
using std::swap;
using std::vector;

class HeapBuilder
{
private:
    vector<int> data_;
    vector<pair<int, int> > swaps_;

    void WriteResponse() const
    {
        cout << swaps_.size() << "\n";
        for ( int i = 0; i < swaps_.size(); ++i ) {
            cout << swaps_ [i].first << " " << swaps_ [i].second << "\n";
        }
    }

    void ReadData()
    {
        int n;
        cin >> n;
        data_.resize ( n );
        for ( int i = 0; i < n; ++i )
            cin >> data_ [i];
    }

    void GenerateSwaps()
    {
        swaps_.clear();
        // The following naive implementation just sorts
        // the given sequence using selection sort algorithm
        // and saves the resulting sequence of swaps.
        // This turns the given array into a heap,
        // but in the worst case gives a quadratic number of swaps.
        //
        // TODO: replace by a more efficient implementation

        // for i from n/2-1 to 0:
        //   sift down
        for ( std::size_t i = data_.size() / 2; i < data_.size(); --i ) {
            SiftDown ( data_, i );
        }
    }

    void SiftDown ( vector<int> &data, std::size_t i )
    {
        // std::cout << "Sift Down. i: " << i << std::endl;
        // get left child
        // get right child
        // swap such that i contains the min element

        std::queue<std::size_t> nextI;
        nextI.push ( i );
        while ( !nextI.empty() ) {
            i = nextI.front();
            nextI.pop();
            std::size_t leftChildI { 2 * i + 1 };
            std::size_t rightChildI { 2 * i + 2 };

            std::size_t minI { i };
            try {
                int &leftChild = data.at ( leftChildI );
                if ( leftChild < data [minI] ) {
                    minI = leftChildI;
                    // std::cout << "left child: " << leftChild
                    //           << ", index: " << leftChildI << std::endl;
                }
            } catch ( ... ) {
            }

            try {
                int &rightChild = data.at ( rightChildI );
                if ( rightChild < data [minI] ) {
                    minI = rightChildI;
                    // std::cout << "right child: " << rightChild
                    //           << ", index: " << rightChildI << std::endl;
                }
            } catch ( ... ) {
            }

            if ( minI != i ) {
                swaps_.push_back ( std::pair<int, int> ( i, minI ) );
                std::swap ( data [i], data [minI] );
                nextI.push ( minI );
            }
        }
    }

public:
    void Solve()
    {
        ReadData();
        GenerateSwaps();
        WriteResponse();
    }
};

int main()
{
    std::ios_base::sync_with_stdio ( false );
    HeapBuilder heap_builder;
    heap_builder.Solve();
    return 0;
}
