#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using std::cin;
using std::string;
using std::vector;

struct Query
{
    string type, name;
    int number;
};

vector<Query> read_queries()
{
    int n;
    cin >> n;
    vector<Query> queries ( n );
    for ( int i = 0; i < n; ++i ) {
        cin >> queries [i].type;
        if ( queries [i].type == "add" )
            cin >> queries [i].number >> queries [i].name;
        else
            cin >> queries [i].number;
    }
    return queries;
}

void write_responses ( const vector<string> &result )
{
    for ( size_t i = 0; i < result.size(); ++i )
        std::cout << result [i] << "\n";
}

vector<string> process_queries ( const vector<Query> &queries )
{
    vector<string> result;
    // Keep list of all existing (i.e. not deleted yet) contacts.
    std::unordered_map<int, std::string> contacts;
    for ( const auto &query : queries ) {
        if ( query.type == "add" ) {
            // if we already have contact with such number,
            // we should rewrite contact's name
            contacts [query.number] = query.name;
        } else if ( query.type == "del" ) {
            contacts.erase ( query.number );
        } else {
            const auto foundIt = contacts.find ( query.number );
            if ( foundIt == contacts.end() ) {
                result.push_back ( "not found" );
                continue;
            }

            result.push_back ( foundIt->second );
        }
    }
    return result;
}

int main()
{
    write_responses ( process_queries ( read_queries() ) );
    return 0;
}
