#include <algorithm>
#include <iostream>
#include <list>
#include <string>
#include <vector>

using std::cin;
using std::string;
using std::vector;

struct Query
{
    string type, s;
    size_t ind;
};

class QueryProcessor
{
    int m_BucketCount;
    // store all strings in one vector
    vector<std::list<std::string>> m_HashMap;
    size_t hash_func ( const string &s ) const
    {
        static const size_t multiplier = 263;
        static const size_t prime = 1000000007;
        unsigned long long hash = 0;
        for ( int i = static_cast<int> ( s.size() ) - 1; i >= 0; --i )
            hash = ( hash * multiplier + s [i] ) % prime;
        return hash % m_BucketCount;
    }

public:
    explicit QueryProcessor ( int bucket_count )
        : m_BucketCount ( bucket_count )
    {
        m_HashMap.resize ( m_BucketCount );
    }

    Query readQuery() const
    {
        Query query;
        cin >> query.type;
        if ( query.type != "check" )
            cin >> query.s;
        else
            cin >> query.ind;
        return query;
    }

    void writeSearchResult ( bool was_found ) const
    {
        std::cout << ( was_found ? "yes" : "no" ) << std::endl;
    }

    void processQuery ( const Query &query )
    {
        if ( query.type == "check" ) {
            const auto &list = m_HashMap.at ( query.ind );
            for ( const auto &str : list ) {
                std::cout << str << " ";
            }
            std::cout << std::endl;
        } else {
            auto &list = m_HashMap.at ( hash_func ( query.s ) );
                const auto foundIt
                    = std::find ( list.begin(), list.end(), query.s );
            if ( query.type == "find" ) {
                writeSearchResult ( foundIt != list.end() );
            } else if ( query.type == "add" ) {
                if ( foundIt == list.end() ) {
                    list.push_front ( query.s );
                }
            } else if ( query.type == "del" ) {
                if ( foundIt != list.end() ) {
                    list.erase ( foundIt );
                }
            }
        }
    }

    void processQueries()
    {
        int n;
        cin >> n;
        for ( int i = 0; i < n; ++i )
            processQuery ( readQuery() );
    }
};

int main()
{
    std::ios_base::sync_with_stdio ( false );
    int bucket_count;
    cin >> bucket_count;
    QueryProcessor proc ( bucket_count );
    proc.processQueries();
    return 0;
}
