#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using std::string;
using ull = unsigned long long;
using ill = int long long;

struct Data
{
    string pattern, text;
};

Data read_input()
{
    Data data;
    std::cin >> data.pattern >> data.text;
    return data;
}

void print_occurrences ( const std::vector<std::size_t> &output )
{
    //std::cout << "size: " << output.size() << std::endl;
    for ( size_t i = 0; i < output.size(); ++i )
        std::cout << output [i] << " ";

    //if ( !output.empty() )
    std::cout << std::endl;
}

ill mod ( ill lhs, ill rhs ) {
    ill ret { lhs % rhs };
    if ( ret < 0 ) {
        ret += rhs;
    }
    return ret;
}

ill polyHash ( const std::string &s, ill p, ill x )
{
    // s[i] * x mod p
    ill hash { 0 };
    for ( std::size_t i { s.size() - 1 }; i < s.size(); --i ) {
        // It's okay if s[i]*x overflows because it's going to have modulus p
        // applied anyway.
        hash = mod ( hash * x + s [i], p );
    }
    return hash;
}

std::vector<ill> precomputeHashes ( const std::string text,
                                    std::size_t patternLength,
                                    ill prime,
                                    ill x )
{
    std::size_t numSubStrs { text.size() - patternLength + 1 };
    std::vector<ill> hashes ( numSubStrs );

    // calculate hash for last substring in text
    auto rbeginIt = hashes.rbegin();
    const std::string subStr ( text.substr ( numSubStrs - 1 ) );
    //std::cout << "precomp subStr: " << subStr << std::endl;
    *rbeginIt = polyHash ( subStr, prime, x );

    // calculate other hashes
    ill y { 1 };
    for ( std::size_t i = 1; i <= patternLength; ++i ) {
        y = mod ( ( y * x ), prime );
    }
    for ( std::size_t i { numSubStrs - 2 }; i < numSubStrs - 1; --i ) {
        //std::cout << "precomp i: " << i << std::endl;
        ill t1 = mod ( x * hashes.at ( i + 1 ), prime );
        ill t2 = text.at ( i );
        ill t3 = mod ( y * text.at ( i + patternLength ), prime );
        // std::cout << "precomp i: " << i << ", t1: " << t1 << ", t2: " << t2
        //           << ", t3: " << t3 << std::endl;
        hashes [i] = mod ( ( t1 + t2 - t3 ), prime );
    }
    return hashes;
}

std::vector<ill> precomputeHashesSlow ( const std::string text,
                                        std::size_t patternLength,
                                        ill prime,
                                        ill x )
{
    std::size_t numSubStrs { text.size() - patternLength + 1 };
    std::vector<ill> hashes ( numSubStrs );

    //std::cout << "Slow hashes:" << std::endl;
    for ( std::size_t i { 0 }; i < numSubStrs; ++i ) {
        hashes [i] = polyHash ( text.substr ( i, patternLength ), prime, x );
        //std::cout << "i: " << i << ", hash: " << hashes.at ( i ) << std::endl;
    }
    return hashes;
}

std::vector<std::size_t> get_occurrences ( const Data &input )
{
    const string &s = input.pattern, t = input.text;

    // prime number that fits in an signed 64 bit number)
    const ill prime { 8533761193 };

    // choose random x value in [0, p - 1]
    // std::srand ( std::time (
    //     nullptr ) );  // use current time as seed for random generator
    // ill x = std::rand() % prime;
    ill x = 34;
    //std::cout << "prime: " << prime << ", x: " << x << std::endl;

    // Calculate hash of pattern.
    ill pHash { polyHash ( s, prime, x ) };

    // Create cache of hashes of substrings at each index in text.
    std::vector<ill> hashes { precomputeHashes ( t, s.size(), prime, x ) };
    //precomputeHashesSlow ( t, s.size(), prime, x );

    // For each index of the text:
    //   Lookup substring hash
    //   If hash matches pattern hash:
    //     Check if substring equals pattern

    
    std::vector<std::size_t> occurences;
    //std::cout << "pHash: " << pHash << std::endl;
    for ( std::size_t i { 0 }; i < t.size() - s.size() + 1; ++i ) {
        // std::cout << "get occur i: " << i << ", hash: " << hashes.at ( i )
        //           << std::endl;
        if ( ( pHash == hashes.at ( i ) )
             && ( s == t.substr ( i, s.size() ) ) ) {
            occurences.push_back ( i );
        }
    }
    return occurences;
}

int main()
{
    //    std::ios_base::sync_with_stdio ( false );
    print_occurrences ( get_occurrences ( read_input() ) );
    return 0;
}
