#include <algorithm>
#include <functional>
#include <iostream>
#include <queue>
#include <stack>
#include <unordered_map>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;

struct Node
{
    int key;
    int left;
    int right;

    Node()
        : key ( 0 )
        , left ( -1 )
        , right ( -1 )
    {}
    Node ( int key_, int left_, int right_ )
        : key ( key_ )
        , left ( left_ )
        , right ( right_ )
    {}
};

// node index to value
using HashMap = std::unordered_map<int, int>;

int findLimitInSubtree ( const vector<Node> &tree,
                         int rootI,
                         HashMap &map,
                         const std::function<bool ( int, int )> &comp )
{
    // std::cout << "findLimitInSubtree" << std::endl;
    enum class TreeDone
    {
        NONE,
        LEFT,
        BOTH
    };

    auto it = map.find ( rootI );
    if ( it != map.end() ) {
        return it->second;
    }

    std::stack<int> nodesI;
    nodesI.push ( rootI );
    TreeDone treeDone { TreeDone::NONE };
    while ( !nodesI.empty() ) {
        int nodeI { nodesI.top() };
        const Node &node { tree.at ( nodeI ) };

        if ( treeDone == TreeDone::NONE ) {
            if ( node.left > 0 ) {
                treeDone = TreeDone::NONE;
                nodesI.push ( node.left );
                continue;
            } else {
                treeDone = TreeDone::LEFT;
            }
        }

        if ( treeDone == TreeDone::LEFT ) {
            if ( node.right > 0 ) {
                treeDone = TreeDone::NONE;
                nodesI.push ( node.right );
                continue;
            } else {
                treeDone = TreeDone::BOTH;
            }
        }

        if ( treeDone == TreeDone::BOTH ) {
            treeDone = TreeDone::NONE;
            int currLimit { node.key };
            if ( node.left > 0 ) {
                int leftLimit { map.find ( node.left )->second };
                if ( comp ( leftLimit, currLimit ) ) {
                    currLimit = leftLimit;
                }
            }

            if ( node.right > 0 ) {
                int rightLimit { map.find ( node.right )->second };
                if ( comp ( rightLimit, currLimit ) ) {
                    currLimit = rightLimit;
                }
            }

            map [nodeI] = currLimit;

            nodesI.pop();
            if ( nodesI.empty() ) {
                continue;
            }

            if ( nodeI == tree.at ( nodesI.top() ).left ) {
                treeDone = TreeDone::LEFT;
            } else if ( nodeI == tree.at ( nodesI.top() ).right ) {
                treeDone = TreeDone::BOTH;
            }
        }
    }
    return map.at ( rootI );
}

bool isBinarySearchTree ( const vector<Node> &tree )
{
    // std::cout << "isBinarySearchTree" << std::endl;
    // If left node exist:
    //   find max value in left subtree
    //   assert max value is less than current node
    //   assert that left subtree is a binary search tree

    HashMap maxMap;
    HashMap minMap;

    std::queue<int> nodesI;
    nodesI.push ( 0 );
    while ( !nodesI.empty() ) {
        const Node &node { tree.at ( nodesI.front() ) };
        nodesI.pop();

        if ( node.left > 0 ) {
            int maxKey { findLimitInSubtree (
                tree,
                node.left,
                maxMap,
                [] ( int lhs, int rhs ) { return lhs > rhs; } ) };
            if ( maxKey >= node.key ) {
                return false;
            }
            nodesI.push ( node.left );
        }

        // Similarly for right node/subtree
        if ( node.right > 0 ) {
            int minKey { findLimitInSubtree (
                tree,
                node.right,
                minMap,
                [] ( int lhs, int rhs ) { return lhs < rhs; } ) };
            if ( minKey <= node.key ) {
                return false;
            }
            nodesI.push ( node.right );
        }
    }

    return true;
}

bool IsBinarySearchTree ( const vector<Node> &tree )
{
    if ( tree.empty() ) {
        return true;
    }

    return isBinarySearchTree ( tree );
}

int main()
{
    int nodes;
    cin >> nodes;
    vector<Node> tree;
    for ( int i = 0; i < nodes; ++i ) {
        int key, left, right;
        cin >> key >> left >> right;
        tree.push_back ( Node ( key, left, right ) );
    }
    if ( IsBinarySearchTree ( tree ) ) {
        cout << "CORRECT" << endl;
    } else {
        cout << "INCORRECT" << endl;
    }
    return 0;
}
