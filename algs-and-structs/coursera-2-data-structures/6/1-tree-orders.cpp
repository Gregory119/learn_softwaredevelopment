#include <algorithm>
#include <iostream>
#include <stack>
#include <vector>
#if defined( __unix__ ) || defined( __APPLE__ )
    #include <sys/resource.h>
#endif

using std::cin;
using std::cout;
using std::ios_base;
using std::vector;

class TreeOrders
{
    int n;
    vector<int> key;
    vector<int> left;
    vector<int> right;

public:
    void read()
    {
        cin >> n;
        key.resize ( n );
        left.resize ( n );
        right.resize ( n );
        for ( int i = 0; i < n; i++ ) {
            cin >> key [i] >> left [i] >> right [i];
        }
    }

    enum class Order
    {
        PRE,
        IN,
        POST
    };

    void GetOrderNaive ( Order order, int i, vector<int> &res )
    {
        int currKey { key.at ( i ) };

        if ( order == Order::PRE ) {
            res.push_back ( currKey );
        }

        // if has left node:
        //   inOrder on left node
        int iLeft { left.at ( i ) };
        if ( iLeft > 0 ) {
            GetOrder ( order, iLeft, res );
        }

        if ( order == Order::IN ) {
            res.push_back ( currKey );
        }

        // similarly for right node
        int iRight { right.at ( i ) };
        if ( iRight > 0 ) {
            GetOrder ( order, iRight, res );
        }

        if ( order == Order::POST ) {
            res.push_back ( currKey );
        }
    }

    void GetOrder ( Order order, int i, vector<int> &res )
    {
        enum class TreeDone
        {
            NONE,
            LEFT,
            BOTH
        };

        // keep stack of visited nodes
        std::stack<int> nodes;
        nodes.push ( i );
        TreeDone treeDone { TreeDone::NONE };
        while ( !nodes.empty() ) {
            if ( treeDone == TreeDone::BOTH ) {
                treeDone = TreeDone::NONE;
                if ( order == Order::POST ) {
                    res.push_back ( key.at ( nodes.top() ) );
                }

                int currI { nodes.top() };
                nodes.pop();

                if ( nodes.empty() ) {
                    continue;
                }

                if ( left.at ( nodes.top() ) == currI ) {
                    treeDone = TreeDone::LEFT;
                } else if ( right.at ( nodes.top() ) == currI ) {
                    treeDone = TreeDone::BOTH;
                }
            }

            if ( treeDone == TreeDone::NONE ) {
                if ( order == Order::PRE ) {
                    res.push_back ( key.at ( nodes.top() ) );
                }

                int leftI { left.at ( nodes.top() ) };
                if ( leftI > 0 ) {
                    nodes.push ( leftI );
                    treeDone = TreeDone::NONE;
                    continue;
                } else {
                    treeDone = TreeDone::LEFT;
                }
            }

            if ( treeDone == TreeDone::LEFT ) {
                if ( order == Order::IN ) {
                    res.push_back ( key.at ( nodes.top() ) );
                }

                int rightI { right.at ( nodes.top() ) };
                if ( rightI > 0 ) {
                    nodes.push ( rightI );
                    treeDone = TreeDone::NONE;
                    continue;
                } else {
                    treeDone = TreeDone::BOTH;
                }
            }
        }
    }

    vector<int> in_order()
    {
        vector<int> result;
        // Finish the implementation
        // You may need to add a new recursive method to do that

        GetOrder ( Order::IN, 0, result );

        return result;
    }

    vector<int> pre_order()
    {
        vector<int> result;
        // Finish the implementation
        // You may need to add a new recursive method to do that

        GetOrder ( Order::PRE, 0, result );

        return result;
    }

    vector<int> post_order()
    {
        vector<int> result;
        // Finish the implementation
        // You may need to add a new recursive method to do that

        GetOrder ( Order::POST, 0, result );
        return result;
    }
};

void print ( vector<int> a )
{
    for ( size_t i = 0; i < a.size(); i++ ) {
        if ( i > 0 ) {
            cout << ' ';
        }
        cout << a [i];
    }
    cout << '\n';
}

int main_with_large_stack_space()
{
    ios_base::sync_with_stdio ( 0 );
    TreeOrders t;
    t.read();
    print ( t.in_order() );
    print ( t.pre_order() );
    print ( t.post_order() );
    return 0;
}

int main ( int argc, char **argv )
{
#if defined( __unix__ ) || defined( __APPLE__ )
    // Allow larger stack space
    const rlim_t kStackSize = 16 * 1024 * 1024;  // min stack size = 16 MB
    struct rlimit rl;
    int result;

    result = getrlimit ( RLIMIT_STACK, &rl );
    if ( result == 0 ) {
        if ( rl.rlim_cur < kStackSize ) {
            rl.rlim_cur = kStackSize;
            result = setrlimit ( RLIMIT_STACK, &rl );
            if ( result != 0 ) {
                std::cerr << "setrlimit returned result = " << result
                          << std::endl;
            }
        }
    }
#endif

    return main_with_large_stack_space();
}
