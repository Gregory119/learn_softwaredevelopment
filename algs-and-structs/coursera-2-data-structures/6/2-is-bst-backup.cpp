#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;

struct Node
{
    int key;
    int left;
    int right;

    Node()
        : key ( 0 )
        , left ( -1 )
        , right ( -1 )
    {}
    Node ( int key_, int left_, int right_ )
        : key ( key_ )
        , left ( left_ )
        , right ( right_ )
    {}
};

// node index to value
using HashMap = std::unordered_map<int, int>;

int findMaxInSubtree ( const vector<Node> &tree, int rootI, HashMap &map )
{
    const Node &root { tree.at ( rootI ) };
    auto maxIt = map.find ( rootI );
    if ( maxIt != map.end() ) {
        return maxIt->second;
    }

    int max { root.key };
    if ( root.left > 0 ) {
        int maxL { findMaxInSubtree ( tree, root.left, map ) };
        if ( maxL > max ) {
            max = maxL;
        }
    }

    if ( root.right > 0 ) {
        int maxR { findMaxInSubtree ( tree, root.right, map ) };
        if ( maxR > max ) {
            max = maxR;
        }
    }

    map [rootI] = max;
    return max;
}

int findMinInSubtree ( const vector<Node> &tree, int rootI, HashMap &minMap )
{
    const Node &root { tree.at ( rootI ) };
    auto minIt = minMap.find ( rootI );
    if ( minIt != minMap.end() ) {
        return minIt->second;
    }

    int min { root.key };
    if ( root.left > 0 ) {
        int minL { findMinInSubtree ( tree, root.left, minMap ) };
        if ( minL < min ) {
            min = minL;
        }
    }

    if ( root.right > 0 ) {
        int minL { findMinInSubtree ( tree, root.right, minMap ) };
        if ( minL < min ) {
            min = minL;
        }
    }
    minMap [rootI] = min;
    return min;
}

bool isBinarySearchTree ( const vector<Node> &tree,
                          const Node &root,
                          HashMap &maxMap,
                          HashMap &minMap )
{
    // If left node exist:
    //   find max value in left subtree
    //   assert max value is less than current node
    //   assert that left subtree is a binary search tree

    if ( root.left > 0 ) {
        const Node &left = tree.at ( root.left );
        int maxKey { findMaxInSubtree ( tree, root.left, maxMap ) };
        if ( maxKey >= root.key ) {
            return false;
        }
        if ( !isBinarySearchTree ( tree, left, maxMap, minMap ) ) {
            return false;
        }
    }

    // Similarly for right node/subtree
    if ( root.right > 0 ) {
        const Node &right = tree.at ( root.right );
        int minKey { findMinInSubtree ( tree, root.right, minMap ) };
        if ( minKey <= root.key ) {
            return false;
        }
        if ( !isBinarySearchTree ( tree, right, maxMap, minMap ) ) {
            return false;
        }
    }

    return true;
}

bool IsBinarySearchTree ( const vector<Node> &tree )
{
    if ( tree.empty() ) {
        return true;
    }

    HashMap maxMap;
    HashMap minMap;
    
    return isBinarySearchTree ( tree, tree.at ( 0 ), maxMap, minMap );
}

int main()
{
    int nodes;
    cin >> nodes;
    vector<Node> tree;
    for ( int i = 0; i < nodes; ++i ) {
        int key, left, right;
        cin >> key >> left >> right;
        tree.push_back ( Node ( key, left, right ) );
    }
    if ( IsBinarySearchTree ( tree ) ) {
        cout << "CORRECT" << endl;
    } else {
        cout << "INCORRECT" << endl;
    }
    return 0;
}
