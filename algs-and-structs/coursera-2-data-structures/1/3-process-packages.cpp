#include <iostream>
#include <queue>
#include <vector>

struct Request
{
    Request ( int arrival_time, int process_time )
        : arrival_time ( arrival_time )
        , process_time ( process_time )
    {}

    int arrival_time;
    int process_time;
};

struct Response
{
    Response ( bool dropped, int start_time )
        : dropped ( dropped )
        , start_time ( start_time )
    {}

    bool dropped;
    int start_time;
};

class Buffer
{
public:
    Buffer ( std::size_t size )
        : size_ ( size )
        , finish_time_()
    {}

    Response Process ( const Request &request )
    {
        // find overlapping queued finish time:
        //   pop if not overlapping
        while ( !finish_time_.empty() ) {
            auto front = finish_time_.front();
            if ( front <= request.arrival_time ) {
                // no overlap
                finish_time_.pop();
                continue;
            }

            // overlap found
            break;
        }

        // if queue is empty:
        //   enqueue request end time
        //   return response start at request time
        if ( finish_time_.empty() ) {
            finish_time_.push ( request.arrival_time + request.process_time );
            return Response ( false, request.arrival_time );
        }

        // overlap found with front of queue
        // if queue size is max:
        //   drop request
        //   return response
        // push queue finish time plus current req duration
        // return response
        if ( finish_time_.size() == size_ ) {
            return Response ( true, request.arrival_time );
        }

        int startTime { finish_time_.back() };
        finish_time_.push ( startTime + request.process_time );
        return Response ( false, startTime );
    }

private:
    std::size_t size_;
    std::queue<int> finish_time_;
};

std::vector<Request> ReadRequests()
{
    std::vector<Request> requests;
    int count;
    std::cin >> count;
    requests.reserve ( count );
    for ( int i = 0; i < count; ++i ) {
        int arrival_time, process_time;
        std::cin >> arrival_time >> process_time;
        requests.emplace_back ( arrival_time, process_time );
    }
    return requests;
}

std::vector<Response> ProcessRequests ( const std::vector<Request> &requests,
                                        Buffer *buffer )
{
    std::vector<Response> responses;
    for ( std::size_t i = 0; i < requests.size(); ++i )
        responses.push_back ( buffer->Process ( requests [i] ) );
    return responses;
}

void PrintResponses ( const std::vector<Response> &responses )
{
    for ( std::size_t i = 0; i < responses.size(); ++i )
        std::cout << ( responses [i].dropped ? -1 : responses [i].start_time )
                  << std::endl;
}

int main()
{
    std::size_t size;
    std::cin >> size;
    std::vector<Request> requests = ReadRequests();

    Buffer buffer ( size );
    std::vector<Response> responses = ProcessRequests ( requests, &buffer );

    PrintResponses ( responses );
    return 0;
}
