#include <algorithm>
#include <iostream>
#include <queue>
#include <vector>
#if defined( __unix__ ) || defined( __APPLE__ )
    #include <sys/resource.h>
#endif

class Node;

class Node
{
public:
    int key;
    Node *parent;
    std::vector<Node *> children;

    Node()
    {
        this->parent = NULL;
    }

    void setParent ( Node *theParent )
    {
        parent = theParent;
        parent->children.push_back ( this );
    }
};

int main_with_large_stack_space()
{
    std::ios_base::sync_with_stdio ( 0 );
    int n;
    std::cin >> n;

    std::vector<Node> nodes;
    nodes.resize ( n );
    for ( int child_index = 0; child_index < n; child_index++ ) {
        int parent_index;
        std::cin >> parent_index;
        if ( parent_index >= 0 )
            nodes [child_index].setParent ( &nodes [parent_index] );
        nodes [child_index].key = child_index;
    }

    // Replace this code with a faster implementation
    int maxHeight { 1 };

    // breadth search first
    // hold queue of current level nodes
    // hold queue of next level nodes
    // hold level count
    // while curr queue is not empty:
    //   pop node from queue
    //   if it is a leaf node:
    //     max height = level count
    //   push child nodes onto next level queue
    //   if curr queue is empty:
    //     move next level queue into current queue
    //     increment level count
    std::queue<const Node *> currNodes;
    std::queue<const Node *> nextNodes;

    // push root node
    const Node* root { &nodes[0] };
    while ( root->parent != nullptr ) {
        //std::cout << "not root" << std::endl;
        root = root->parent;
    }
    currNodes.push ( root );

    while ( !currNodes.empty() ) {
        auto n = currNodes.front();
        currNodes.pop();
        //std::cout << "current node: " << n->key << std::endl;

        for ( const Node* child : n->children ) {
            //std::cout << "pushing child: " << child->key << std::endl;
            nextNodes.push ( child );
        }

        if ( currNodes.empty() && !nextNodes.empty() ) {
            currNodes = std::move ( nextNodes );
            nextNodes = std::queue<const Node*>();
            ++maxHeight;
            //std::cout << "moving to next level: " << maxHeight << std::endl;
        }
    }
        
    std::cout << maxHeight << std::endl;
    return 0;
}

int main ( int argc, char **argv )
{
#if defined( __unix__ ) || defined( __APPLE__ )
    // Allow larger stack space
    const rlim_t kStackSize = 16 * 1024 * 1024;  // min stack size = 16 MB
    struct rlimit rl;
    int result;

    result = getrlimit ( RLIMIT_STACK, &rl );
    if ( result == 0 ) {
        if ( rl.rlim_cur < kStackSize ) {
            rl.rlim_cur = kStackSize;
            result = setrlimit ( RLIMIT_STACK, &rl );
            if ( result != 0 ) {
                std::cerr << "setrlimit returned result = " << result
                          << std::endl;
            }
        }
    }

#endif
    return main_with_large_stack_space();
}
