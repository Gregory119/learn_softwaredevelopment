#include <iostream>
#include <stack>
#include <string>

struct Bracket
{
    Bracket ( char type, std::size_t position )
        : type ( type )
        , position ( position )
    {}

    bool Matchc ( char c )
    {
        if ( type == '[' && c == ']' )
            return true;
        if ( type == '{' && c == '}' )
            return true;
        if ( type == '(' && c == ')' )
            return true;
        return false;
    }

    char type;
    std::size_t position;
};

int main()
{
    std::string text;
    getline ( std::cin, text );

    std::stack<Bracket> openStack;
    std::size_t unmatchedI { 0 };
    bool foundUnmatched { false };
    for ( std::size_t i = 0; i < text.length(); ++i ) {
        char next = text [i];

        //std::cout << "next: " << next << ", i: " << i << std::endl;
        
        if ( next == '(' || next == '[' || next == '{' ) {
            // Process opening bracket, write your code here
            // push bracket onto stack
            openStack.push ( Bracket ( next, i ) );
            continue;
        }

        if ( next == ')' || next == ']' || next == '}' ) {
            // Process closing bracket, write your code here
            // read and pop top of stack
            // if popped char does not pair with closing char:
            //   set index of unmatched bracket
            //   break
            if ( openStack.empty() ) {
                //std::cout << "stack empty" << std::endl;
                // no open brackets
                foundUnmatched = true;
                unmatchedI = i;
                break;
            }
            
            auto top = openStack.top();
            //std::cout << "top - type: " << top.type << "position: " << top.position << std::endl;
            openStack.pop();
            if ( !top.Matchc ( next ) ) {
                //std::cout << "no match found" << std::endl;
                foundUnmatched = true;
                unmatchedI = i;
                break;
            }
        }
    }

    // if index of unmatched bracket is set:
    //   print it
    //   return
    if ( foundUnmatched ) {
        std::cout << unmatchedI + 1 << std::endl;
        return 0;
    }

    // check for unmatched open brackets
    // if stack is not empty:
    //   pop top of stack and print index
    //   return
    if ( !openStack.empty() ) {
        auto top = openStack.top();
        openStack.pop();
        std::cout << top.position + 1 << std::endl;
        return 0;
    }

    // print success
    std::cout << "Success" << std::endl;

    return 0;
}
