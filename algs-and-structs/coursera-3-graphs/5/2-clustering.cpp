#include <algorithm>
#include <cassert>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <limits>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using std::pair;
using std::vector;

struct Edge
{
    bool operator> ( const Edge &rhs ) const
    {
        return m_cost > rhs.m_cost;
    }

    std::size_t m_start_node;
    std::size_t m_end_node;
    double m_cost;
};

void printClusters (
    const std::unordered_map<std::size_t, std::unordered_set<std::size_t>>
        &clusters,
    const vector<int> &x,
    const vector<int> &y )
{
    for ( const auto &clusterPair : clusters ) {
        std::cout << "clust: ";
        for ( auto p : clusterPair.second ) {
            std::cout << p << " (x: " << x [p] << ", y: " << y [p] << "), ";
        }
        std::cout << std::endl;
    }
}

double clustering ( const vector<int> &x, const vector<int> &y, const int k )
{
    // Use kruskal's algorithm
    // - Start with each node being a set.
    // - Iteratively add the next lightest edge to the forest X (has multiple
    // sets) if the points of the edge are not in the same set.
    // - Stop when there are k sets in X

    std::unordered_map<std::size_t, std::unordered_set<std::size_t>> forest;
    forest.reserve ( x.size() );
    for ( std::size_t i { 0 }; i < x.size(); ++i ) {
        forest [i] = { i };
    }

    std::vector<std::size_t> point_to_set;
    point_to_set.reserve ( forest.size() );
    for ( std::size_t i { 0 }; i < forest.size(); ++i ) {
        point_to_set.push_back ( i );
    }

    // Create a queue of edges prioritized by the minimum weight
    std::priority_queue<Edge, std::vector<Edge>, std::greater<Edge>>
        prior_edges;
    for ( std::size_t i { 0 }; i < x.size(); ++i ) {
        for ( std::size_t j { 0 }; j < x.size(); ++j ) {
            // There could be duplicates but that will be checked for.
            const double dist { std::hypot ( x [j] - x [i], y [j] - y [i] ) };
            prior_edges.push ( { i, j, dist } );
        }
    }

    while ( !prior_edges.empty() ) {
        auto edge = prior_edges.top();
        prior_edges.pop();
        std::size_t &start_set_i { point_to_set [edge.m_start_node] };
        std::size_t &end_set_i { point_to_set [edge.m_end_node] };
        if ( start_set_i == end_set_i ) {
            // skip cycle and duplicates
            continue;
        }

        if ( static_cast<int> ( forest.size() ) > k ) {
            // merge sets
            auto &start_set = forest [start_set_i];
            auto &end_set = forest [end_set_i];
            start_set.insert ( end_set.begin(), end_set.end() );
            forest.erase ( end_set_i );
            end_set_i = start_set_i;
        } else if ( static_cast<int> ( forest.size() ) == k ) {
            // The minimum cluster distance will be the next prioritized edge
            // that has points in different sets.
            return edge.m_cost;
        }
    }

    // printClusters ( forest, x, y );

    return 0.0;
}

int main()
{
    size_t n;
    int k;
    std::cin >> n;
    vector<int> x ( n ), y ( n );
    for ( size_t i = 0; i < n; i++ ) {
        std::cin >> x [i] >> y [i];
    }
    std::cin >> k;
    std::cout << std::fixed << std::setprecision ( 10 )
              << clustering ( x, y, k ) << std::endl;
}
