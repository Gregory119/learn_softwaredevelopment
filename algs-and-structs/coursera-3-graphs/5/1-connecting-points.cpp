#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <limits>
#include <queue>
#include <unordered_set>
#include <vector>

using std::vector;

struct CostWithNode
{
    bool operator> ( const CostWithNode &rhs ) const
    {
        return m_Cost > rhs.m_Cost;
    }

    std::size_t m_Node;
    double m_Cost;
};

double minimum_distance ( const vector<int> &x, const vector<int> &y )
{
    // Use prim's algorithm to iteratively add the next point with the minimum
    // cost.

    // Keep cost (remaining distance) for each remaining vertex
    vector<double> costs ( x.size(), std::numeric_limits<double>::max() );
    double tot_cost = 0.;
    // Start with some vertex and set cost to zero
    std::size_t start_node { 0 };
    costs [start_node] = 0.0;
    std::unordered_set<std::size_t> processed_nodes;
    std::priority_queue<CostWithNode,
                        std::vector<CostWithNode>,
                        std::greater<CostWithNode>>
        prior_costs;
    prior_costs.push ( { start_node, 0.0 } );

    while ( !prior_costs.empty() ) {
        auto node_with_cost = prior_costs.top();
        auto node { node_with_cost.m_Node };
        auto cost { node_with_cost.m_Cost };
        prior_costs.pop();
        // skip if already processed
        if ( processed_nodes.find ( node ) != processed_nodes.end() ) {
            continue;
        }
        // consider processed
        processed_nodes.insert ( node );
        // accumulate cost
        tot_cost += cost;
        // std::cout << "processing. node: " << node << ", cost: " << cost
        //           << ", tot_cost: " << tot_cost << std::endl;

        //   for each neighbor:
        for ( std::size_t n { 0 }; n < x.size(); ++n ) {
            // skip if already processed
            if ( processed_nodes.find ( n ) != processed_nodes.end() ) {
                continue;
            }

            const double weight_n { std::hypot ( x [n] - x [node],
                                                 y [n] - y [node] ) };
            double &cost_n ( costs [n] );
            if ( cost_n <= weight_n ) {
                continue;
            }
            // change priority of neighbor (insert into priority queue with new
            // cost) - this can cause duplicates nodes with different priorities
            // (hence check if nodes have already been processed)
            cost_n = weight_n;
            prior_costs.push ( { n, cost_n } );
        }
    }

    return tot_cost;
}

int main()
{
    size_t n;
    std::cin >> n;
    vector<int> x ( n ), y ( n );
    for ( size_t i = 0; i < n; i++ ) {
        std::cin >> x [i] >> y [i];
    }
    std::cout << std::setprecision ( 10 ) << minimum_distance ( x, y )
              << std::endl;
}
