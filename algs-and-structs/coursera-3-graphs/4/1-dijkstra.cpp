#include <iostream>
#include <queue>
#include <unordered_set>
#include <vector>

using std::pair;
using std::priority_queue;
using std::queue;
using std::vector;

struct DistToNode
{
    bool operator> ( const DistToNode &rhs ) const
    {
        return m_Dist > rhs.m_Dist;
    }

    int m_Dist;
    int m_Node;
};

long long distance ( vector<vector<int>> &adj,
                     vector<vector<int>> &cost,
                     int s,
                     int t )
{
    // Array of distances to each vertex/node from start vertex/node
    std::vector<int> dist ( adj.size(), -1 );
    dist [s] = 0;

    // Hold set of nodes with found minimum paths
    std::unordered_set<int> minDistNodes;

    // Heap of distance to node pairs
    std::priority_queue<DistToNode,
                        std::vector<DistToNode>,
                        std::greater<DistToNode>>
        minHeapDistToNode;

    // Insert start node into heap
    minHeapDistToNode.push ( { 0, s } );

    // while min heap is not empty:
    //   extract the min dist node pair
    //   if min path already found:
    //     skip
    //   include in min path set
    //   if node matches end node:
    //     return dist value
    //   for each neighbor:
    //     try relax
    //     if relaxed:
    //       insert a new dist-node pair into heap
    while ( !minHeapDistToNode.empty() ) {
        auto minDistNode { minHeapDistToNode.top() };
        minHeapDistToNode.pop();
        int node { minDistNode.m_Node };
        int d { minDistNode.m_Dist };
        //std::cout << "node: " << node << ", dist: " << d << std::endl;

        if ( minDistNodes.find ( node ) != minDistNodes.end() ) {
            continue;
        }
        if ( node == t ) {
            return d;
        }
        minDistNodes.insert ( node );

        //std::cout << "neighbors: ";
        const auto &neighbors { adj [node] };
        for ( std::size_t i { 0 }; i < neighbors.size(); ++i ) {
            int n { neighbors [i] };
            //std::cout << "{ node: " << n << ", ";
            int w { cost [node][i] };
            int distN { dist [node] + w };
            //std::cout << "distN: " << distN << ", dist: " << dist [n]
            //<< ", w: " << w;
            // Set for first time
            if ( dist [n] == -1 ) {
                dist [n] = distN;
                minHeapDistToNode.push ( { dist [n], n } );
                //std::cout << "},";
                continue;
            }

            // try relax
            if ( dist [n] <= distN ) {
                //std::cout << "},";
                continue;
            }
            dist [n] = distN;

            // Relax was successful so "change priority".
            // Can't change priority so just insert a new distance for a node.
            minHeapDistToNode.push ( { dist [n], n } );
            //std::cout << "},";
        }
        //std::cout << std::endl;
    }

    return -1;
}

int main()
{
    int n, m;
    std::cin >> n >> m;
    vector<vector<int>> adj ( n, vector<int>() );
    vector<vector<int>> cost ( n, vector<int>() );
    for ( int i = 0; i < m; i++ ) {
        int x, y, w;
        std::cin >> x >> y >> w;
        adj [x - 1].push_back ( y - 1 );
        cost [x - 1].push_back ( w );
    }
    int s, t;
    std::cin >> s >> t;
    s--, t--;
    std::cout << distance ( adj, cost, s, t );
}
