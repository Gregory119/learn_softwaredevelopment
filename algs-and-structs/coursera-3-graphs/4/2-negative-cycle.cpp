#include <iostream>
#include <vector>

using std::vector;

bool hasNegativeCycle ( const vector<vector<int> > &adj,
                        const vector<vector<int> > &cost,
                        int s )
{
    // for V (number of nodes/vertices) iterations
    //   for each node in the graph
    //     for each neighbor
    //       try relax
    // if any node was relaxed on the Vth iteration, then a negative cycle
    // exists

    std::vector<int> dists ( adj.size(), -1 );
    dists [s] = 0;
    bool relaxedLast { 0 };
    for ( std::size_t i { 0 }; i < adj.size(); ++i ) {
        relaxedLast = false;
        for ( std::size_t node { 0 }; node < adj.size(); ++node ) {
            const auto &neighbors { adj [node] };
            for ( std::size_t nI { 0 }; nI < neighbors.size(); ++nI ) {
                const int n { neighbors [nI] };
                const int w { cost [node][nI] };
                const int distN { dists [node] + w };
                int &d { dists [n] };
                if ( d == -1 ) {
                    d = distN;
                }

                if ( d <= distN ) {
                    continue;
                }

                // relaxing
                d = distN;
                if ( i == adj.size() - 1 ) {
                    relaxedLast = true;
                }
            }
        }
    }
    return relaxedLast;
}

int negative_cycle ( vector<vector<int> > &adj, vector<vector<int> > &cost )
{
    // For each node:
    //   Check if a negative cycle exists
    // for ( int i { 0 }; i < static_cast<int> ( adj.size() ); ++i ) {
    //     if ( hasNegativeCycle ( adj, cost, i ) ) {
    //         return 1;
    //     }
    // }

    return hasNegativeCycle ( adj, cost, 0 );
    return 0;
}

int main()
{
    int n, m;
    std::cin >> n >> m;
    vector<vector<int> > adj ( n, vector<int>() );
    vector<vector<int> > cost ( n, vector<int>() );
    for ( int i = 0; i < m; i++ ) {
        int x, y, w;
        std::cin >> x >> y >> w;
        adj [x - 1].push_back ( y - 1 );
        cost [x - 1].push_back ( w );
    }
    std::cout << negative_cycle ( adj, cost );
}
