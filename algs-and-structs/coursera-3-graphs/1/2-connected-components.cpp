#include <iostream>
#include <unordered_set>
#include <vector>

using std::pair;
using std::vector;

const vector<int> &neighbors ( const vector<vector<int>> &adj, int x )
{
    return adj [x];
}

// return true if new vertix/vertices found
bool search ( const vector<vector<int>> &adj,
              std::unordered_set<int> &visited,
              int x )
{
    bool found { visited.find ( x ) == visited.end() };

    visited.insert ( x );
    for ( const auto &n : neighbors ( adj, x ) ) {
        if ( visited.find ( n ) != visited.end() ) {
            continue;
        }

        found = true;
        search ( adj, visited, n );
    }

    return found;
}

int number_of_components ( const vector<vector<int>> &adj )
{
    int connected = 0;

    // for each vertix in the graph
    //   search for new vertices
    //   if new vertix found:
    //     increment connected count
    std::unordered_set<int> visited;
    for ( int i { 0 }; i < static_cast<int> ( adj.size() ); ++i ) {
        if ( search ( adj, visited, i ) ) {
            connected++;
        }
    }

    return connected;
}

int main()
{
    size_t n, m;
    std::cin >> n >> m;
    vector<vector<int>> adj ( n, vector<int>() );
    for ( size_t i = 0; i < m; i++ ) {
        int x, y;
        std::cin >> x >> y;
        adj [x - 1].push_back ( y - 1 );
        adj [y - 1].push_back ( x - 1 );
    }
    std::cout << number_of_components ( adj );
}
