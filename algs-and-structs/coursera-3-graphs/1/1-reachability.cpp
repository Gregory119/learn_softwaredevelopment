#include <iostream>
#include <unordered_set>
#include <vector>

using std::pair;
using std::vector;

const vector<int>& neighbours ( const vector<vector<int>> &adj, int x )
{
    return adj [x];
}

bool search ( const vector<vector<int>> &adj,
              std::unordered_set<int> &visited,
              int x,
              int y )
{
    if ( x == y ) {
        return true;
    }

    visited.insert ( x );
    for ( const auto &n : neighbours ( adj, x ) ) {
        if ( visited.find ( n ) != visited.end() ) {
            continue;
        }

        if ( search ( adj, visited, n, y ) ) {
            return true;
        }
    }

    return false;
}

int reach ( const vector<vector<int>> &adj, int x, int y )
{
    // start at x
    // for each neighbour of x
    //   depth first search for y
    std::unordered_set<int> visited;
    for ( const auto &n : neighbours ( adj, x ) ) {
        if ( search ( adj, visited, n, y ) ) {
            return 1;
        }
    }

    return 0;
}

int main()
{
    size_t n, m;
    std::cin >> n >> m;
    vector<vector<int>> adj ( n, vector<int>() );
    for ( size_t i = 0; i < m; i++ ) {
        int x, y;
        std::cin >> x >> y;
        adj [x - 1].push_back ( y - 1 );
        adj [y - 1].push_back ( x - 1 );
    }
    int x, y;
    std::cin >> x >> y;
    std::cout << reach ( adj, x - 1, y - 1 );
}
