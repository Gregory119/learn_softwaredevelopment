#include <cassert>
#include <cstdio>
#include <iostream>
#include <limits>
#include <queue>
#include <unordered_set>
#include <utility>
#include <vector>

using namespace std;

// External vector of size 2 - for forward and backward search.
// Internal 2-dimensional vector is vector of adjacency lists for each node.
typedef vector<vector<vector<int>>> Adj;

// Distances can grow out of int type
typedef long long Len;

// Vector of two priority queues - for forward and backward searches.
// Each priority queue stores the closest unprocessed node in its head.
typedef vector<priority_queue<pair<Len, int>,
                              vector<pair<Len, int>>,
                              greater<pair<Len, int>>>>
    Queue;

const Len INFINITY = numeric_limits<Len>::max() / 4;
const std::size_t FORWARD = 0;
const std::size_t REVERSE = 1;

class Bidijkstra
{
    // Number of nodes
    int n_;
    // Graph adj_[0] and cost_[0] correspond to the initial graph,
    // adj_[1] and cost_[1] correspond to the reversed graph.
    // Graphs are stored as vectors of adjacency lists corresponding
    // to nodes.
    // Adjacency list itself is stored in adj_, and the corresponding
    // edge costs are stored in cost_.
    Adj adj_;
    Adj cost_;
    // distance_[0] stores distances for the forward search,
    // and distance_[1] stores distances for the backward search.
    vector<vector<Len>> distance_;

    // visited_[0] stores visited nodes for the forward search,
    // and visited_[1] stores visited nodes for the backward search.
    vector<std::unordered_set<int>> visited_;

    // All nodes either visited/processed or discovered (distance updated) by
    // forward and backward search.
    std::unordered_set<int> workset_;

public:
    Bidijkstra ( int n, Adj adj, Adj cost )
        : n_ ( n )
        , adj_ ( std::move ( adj ) )
        , cost_ ( std::move ( cost ) )
        , distance_ ( 2, vector<Len> ( n, INFINITY ) )
        , visited_ ( 2 )
    {
        visited_.at ( FORWARD ).reserve ( n / 2 );
        visited_.at ( REVERSE ).reserve ( n / 2 );
        workset_.reserve ( n );
    }

    // Initialize the data structures before new query,
    // clear the changes made by the previous query.
    void clear()
    {
        for ( const int v : workset_ ) {
            distance_.at ( FORWARD ).at ( v ) = INFINITY;
            distance_.at ( REVERSE ).at ( v ) = INFINITY;
        }
        workset_.clear();

        visited_.at ( FORWARD ).clear();
        visited_.at ( REVERSE ).clear();
    }

    // Processes visit of either forward or backward search
    // (determined by value of side), to node v trying to
    // relax the current distance with dist.
    void visit ( Queue &q, int side, int v, Len dist )
    {
        // mark as visited
        visited_.at ( side ).insert ( v );
        workset_.insert ( v );

        // for each neighbor
        const auto &neighbors = adj_.at ( side ).at ( v );
        const auto &neighbor_costs = cost_.at ( side ).at ( v );

        for ( std::size_t i { 0 }; i < neighbors.size(); ++i ) {
            const int n { neighbors.at ( i ) };
            const int n_cost { neighbor_costs.at ( i ) };
            const Len new_dist { dist + n_cost };
            Len &n_dist = distance_.at ( side ).at ( n );

            //   if (dist to prev node + cost to neighbor) < dist to neighbor,
            //   then update.
            if ( new_dist >= n_dist ) {
                continue;
            }
            n_dist = new_dist;
            q.at ( side ).push ( { n_dist, n } );
            workset_.insert ( n );
        }
    }

    // return extracted node else -1
    int incrSearch ( Queue &q, int side )
    {
        // get first non-duplicate
        int node { 0 };
        Len len { INFINITY };
        do {
            auto dist_node_pair = q.at ( side ).top();
            q.at ( side ).pop();
            node = dist_node_pair.second;
            len = dist_node_pair.first;
        } while ( visited_.at ( side ).find ( node )
                  != visited_.at ( side ).end() );

        visit ( q, side, node, len );
        return node;
    }

    Len findShortestPath()
    {
        // for each forward an backward processed node v:
        //   path dist is dist_f[v] + dist_r[v]
        //   if smaller than min distance:
        //     update min distance
        Len min_dist { INFINITY };
        for ( auto side : { FORWARD, REVERSE } ) {
            for ( const int v : visited_.at ( side ) ) {
                const Len dist { distance_.at ( FORWARD ).at ( v )
                                 + distance_.at ( REVERSE ).at ( v ) };
                if ( dist < min_dist ) {
                    min_dist = dist;
                }
            }
        }
        return min_dist;
    }

    // node has been processed in forward an backward search
    bool isNodeMutual ( const int node )
    {
        const bool in_forward = visited_.at ( FORWARD ).find ( node )
                                != visited_.at ( FORWARD ).end();
        const bool in_reverse = visited_.at ( REVERSE ).find ( node )
                                != visited_.at ( REVERSE ).end();
        return in_forward && in_reverse;
    }

    // Returns the distance from s to t in the graph.
    Len query ( int s, int t )
    {
        clear();
        Queue q ( 2 );

        // insert start/end nodes into priority queue and initialize
        q.at ( FORWARD ).push ( { 0, s } );
        q.at ( REVERSE ).push ( { 0, t } );
        distance_.at ( FORWARD ).at ( s ) = 0;
        distance_.at ( REVERSE ).at ( t ) = 0;

        // while queues are not empty:
        while ( !q.at ( FORWARD ).empty() && !q.at ( REVERSE ).empty() ) {
            int node { incrSearch ( q, FORWARD ) };
            if ( isNodeMutual ( node ) ) {
                return findShortestPath();
            }

            node = incrSearch ( q, REVERSE );
            if ( isNodeMutual ( node ) ) {
                return findShortestPath();
            }
        }

        return -1;
    }
};

int main()
{
    int n, m;
    scanf ( "%d%d", &n, &m );
    Adj adj ( 2, vector<vector<int>> ( n ) );
    Adj cost ( 2, vector<vector<int>> ( n ) );
    for ( int i = 0; i < m; ++i ) {
        int u, v, c;
        scanf ( "%d%d%d", &u, &v, &c );
        adj [0][u - 1].push_back ( v - 1 );
        cost [0][u - 1].push_back ( c );
        adj [1][v - 1].push_back ( u - 1 );
        cost [1][v - 1].push_back ( c );
    }

    Bidijkstra bidij ( n, adj, cost );

    int t;
    scanf ( "%d", &t );
    for ( int i = 0; i < t; ++i ) {
        int u, v;
        scanf ( "%d%d", &u, &v );
        printf ( "%lld\n", bidij.query ( u - 1, v - 1 ) );
    }
}
