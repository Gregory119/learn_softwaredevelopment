#include <iostream>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using std::queue;
using std::vector;

int distance ( vector<vector<int> > &adj, int s, int t )
{
    // depth first search
    std::unordered_map<int,int> dists;
    dists[s] = 0;

    std::queue<int> nodes;
    nodes.push ( s );
    while ( !nodes.empty() ) {
        const int node = nodes.front();
        nodes.pop();

        // look at neighbors that are new
        for ( const int n : adj [node] ) {
            // skip already visited
            if ( dists.find ( n ) != dists.end() ) {
                continue;
            }
            dists[n] = dists[node]+1;

            if ( n == t ) {
                return dists[n];
            }
            nodes.push ( n );
        }
    }

    return -1;
}

int main()
{
    int n, m;
    std::cin >> n >> m;
    vector<vector<int> > adj ( n, vector<int>() );
    for ( int i = 0; i < m; i++ ) {
        int x, y;
        std::cin >> x >> y;
        adj [x - 1].push_back ( y - 1 );
        adj [y - 1].push_back ( x - 1 );
    }
    int s, t;
    std::cin >> s >> t;
    s--, t--;
    std::cout << distance ( adj, s, t );
}
