#include <iostream>
#include <queue>
#include <vector>

using std::queue;
using std::vector;

int isBipartite ( const std::vector<std::vector<int>> &adj,
                  std::vector<int> &colors,
                  int s )
{
    colors [s] = 0;

    // start from some vertix (choose the first)
    queue<int> nodes;
    nodes.push ( s );
    // depth first search
    std::size_t count { 0 };
    while ( !nodes.empty() ) {
        count++;
        const int node { nodes.front() };
        nodes.pop();
        // std::cout << "node: " << node << ", color: " << colors[node] <<
        // std::endl;
        //   all neighbors of a node should be the opposite color
        for ( const int n : adj [node] ) {
            const int nColor { colors [n] };
            // std::cout << "neighbor: " << n << ", found color: " << nColor;
            //   if neighbor known:
            //     color should be opposite to color of current node
            //   if a neighbor is new:
            //     set the color opposite to the current node
            if ( nColor != -1 ) {
                if ( nColor == colors [node] ) {
                    return 0;
                }
                // std::cout << std::endl;
            } else {
                colors [n] = !colors [node];
                // std::cout << ", set color: " << colors[n] << std::endl;;
                nodes.push ( n );
            }
        }
    }

    return 1;
}

int bipartite ( vector<vector<int>> &adj )
{
    // keep track of color of each node (black/white)
    // -1 => unknown
    // 0 => black
    // 1 => white
    vector<int> colors ( adj.size(), -1 );

    // for every vertex/node in the graph
    for ( int i { 0 }; i < static_cast<int> ( adj.size() ); ++i ) {
        // skip if visited
        if ( colors [i] != -1 ) {
            continue;
        }
        int temp { isBipartite ( adj, colors, i ) };
        if ( !temp ) {
            return 0;
        }
    }
    return 1;
}

int main()
{
    int n, m;
    std::cin >> n >> m;
    vector<vector<int>> adj ( n, vector<int>() );
    for ( int i = 0; i < m; i++ ) {
        int x, y;
        std::cin >> x >> y;
        adj [x - 1].push_back ( y - 1 );
        adj [y - 1].push_back ( x - 1 );
    }
    std::cout << bipartite ( adj );
}
