#include <algorithm>
#include <functional>
#include <iostream>
#include <unordered_set>
#include <vector>

using std::pair;
using std::vector;

bool depthFirstSearch ( const vector<vector<int>> &adj,
                        std::unordered_set<int> &visited,
                        int v,
                        const std::function<void ( int )> &postFn )
{
    // skip if visited
    if ( visited.find ( v ) != visited.end() ) {
        return false;
    }

    visited.insert ( v );
    for ( const int n : adj [v] ) {
        depthFirstSearch ( adj, visited, n, postFn );
    }
    postFn ( v );
    return true;
}

template <class RowT>
void print ( const std::vector<RowT> &container )
{
    for ( int i { 0 }; i < static_cast<int> ( container.size() ); ++i ) {
        std::cout << i << ": ";
        for ( const int v : container [i] ) {
            std::cout << v << ", ";
        }
        std::cout << std::endl;
    }
}

void search ( const vector<vector<int>> &adj,
              const std::function<void ( int )> &postFn )
{
    std::unordered_set<int> visited;
    //   for each vertex v in the graph:
    //     postOrder(v)
    for ( int i { 0 }; i < static_cast<int> ( adj.size() ); ++i ) {
        depthFirstSearch ( adj, visited, i, postFn );
    }
}

std::vector<std::vector<int>> reverseGraph ( const vector<vector<int>> adj )
{
    // for each vertix
    //   for each neighbor
    //     create connection from neighbor to vertix
    std::vector<std::vector<int>> rev ( adj.size() );
    for ( std::size_t i { 0 }; i < adj.size(); ++i ) {
        for ( const int n : adj [i] ) {
            rev [n].push_back ( i );
        }
    }

    return rev;
}

int number_of_strongly_connected_components ( vector<vector<int>> adj )
{
    // Need to find sink SCC, which can then be removed/excluded from the graph
    // and then next sink SCC is another SCC in the graph. The vertix with the
    // largest post-order is in a source SCC, but a sink SCC needs to be
    // found. For the reverse graph, the vertex with the largest post-order is
    // in a sink SCC of the original graph.

    // NB: Assume that graph is fully connected.

    // General idea: Find and remove each latest sink SCC
    // std::cout << "Original graph: " << std::endl;
    // print ( adj );
    // generate reverse graph
    vector<vector<int>> revAdj { reverseGraph ( adj ) };
    // vector<vector<int>> revAdj { adj };
    // std::cout << "Reverse graph: " << std::endl;
    // print ( revAdj );

    // find post order of reverse graph
    vector<int> order;
    search ( revAdj, [&order] ( int v ) { order.push_back ( v ); } );

    // std::cout << "order: ";
    // for ( const int v : order ) {
    //     std::cout << v << ", ";
    // }
    // std::cout << std::endl;

    // reverse post order
    // for vertices in reverse post order
    //   DFS in original graph to find latest sink SCC
    int numSSCs { 0 };
    std::unordered_set<int> visited;
    // std::cout << "Counting SCC sinks" << std::endl;
    for ( auto it = order.crbegin(); it != order.crend(); ++it ) {
        if ( depthFirstSearch ( adj, visited, *it, [] ( int ) {
                 // std::cout << "dps done for: " << v << std::endl;
             } ) ) {
            ++numSSCs;
        }
    }

    return numSSCs;
}

int main()
{
    size_t n, m;
    std::cin >> n >> m;
    vector<vector<int>> adj ( n, vector<int>() );
    for ( size_t i = 0; i < m; i++ ) {
        int x, y;
        std::cin >> x >> y;
        adj [x - 1].push_back ( y - 1 );
    }
    std::cout << number_of_strongly_connected_components ( adj );
}
