#include <algorithm>
#include <functional>
#include <iostream>
#include <unordered_set>
#include <vector>

using std::pair;
using std::vector;

std::vector<std::vector<int>> reverseGraph ( const vector<vector<int>> adj )
{
    // for each vertix
    //   for each neighbor
    //     create connection from neighbor to vertix
    std::vector<std::vector<int>> rev ( adj.size() );
    for ( std::size_t i { 0 }; i < adj.size(); ++i ) {
        for ( const int n : adj [i] ) {
            rev [n].push_back ( i );
        }
    }

    return rev;
}

bool depthFirstSearch ( const vector<vector<int>> &adj,
                        std::unordered_set<int> &visited,
                        int v,
                        const std::function<void ( int )> &postFn )
{
    // skip if visited
    if ( visited.find ( v ) != visited.end() ) {
        return false;
    }

    visited.insert ( v );
    for ( const int n : adj [v] ) {
        depthFirstSearch ( adj, visited, n, postFn );
    }
    postFn ( v );
    return true;
}

void findSinks ( const vector<vector<int>> &adj,
                 std::unordered_set<int> &visited,
                 int v,
                 vector<int> &sinks )
{
    visited.insert ( v );
    const auto neighbors { adj [v] };
    if ( neighbors.empty() ) {
        sinks.push_back ( v );
        return;
    }

    //    std::cout << "neighbors: ";
    for ( const int n : neighbors ) {
        //std::cout << n;
        if ( visited.find ( n ) != visited.end() ) {
            continue;
        }
        findSinks ( adj, visited, n, sinks );
    }
    //std::cout << std::endl;
}

template <class RowT>
void print ( const std::vector<RowT> &container )
{
    for ( int i { 0 }; i < static_cast<int> ( container.size() ); ++i ) {
        std::cout << i << ": ";
        for ( const int v : container [i] ) {
            std::cout << v << ", ";
        }
        std::cout << std::endl;
    }
}

vector<int> toposort ( const vector<vector<int>> &adj )
{
    // for each vertex
    //   skip if visited
    //   find sink vertices which are sources in the reverse graph
    //   for each source vertex
    //     dps with post order

    std::unordered_set<int> visited;
    vector<int> order;
    const vector<vector<int>> revAdj { reverseGraph ( adj ) };
    std::unordered_set<int> tempVisited;
    vector<int> sources;

    // std::cout << "original graph: " << std::endl;
    // print ( adj );
    // std::cout << "reverse graph: " << std::endl;
    // print ( revAdj );
    
    for ( int i { 0 }; i < static_cast<int> ( adj.size() ); ++i ) {
        //std::cout << "i: " << i << std::endl;
        if ( visited.find ( i ) != visited.end() ) {
            continue;
        }

        tempVisited.clear();
        sources.clear();
        findSinks ( adj, tempVisited, i, sources );

        //std::cout << "sources: " << std::endl;
        for ( const int s : sources ) {
            //std::cout << s;
            depthFirstSearch ( revAdj, visited, s, [&order] ( int x ) {
                order.push_back ( x );
            } );
        }
        //std::cout << std::endl;
    }

    return order;
}

int main()
{
    size_t n, m;
    std::cin >> n >> m;
    vector<vector<int>> adj ( n, vector<int>() );
    for ( size_t i = 0; i < m; i++ ) {
        int x, y;
        std::cin >> x >> y;
        adj [x - 1].push_back ( y - 1 );
    }
    vector<int> order = toposort ( adj );
    for ( size_t i = 0; i < order.size(); i++ ) {
        std::cout << order [i] + 1 << " ";
    }
}
