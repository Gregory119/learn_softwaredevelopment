#include <iostream>
#include <unordered_set>
#include <vector>

using std::pair;
using std::vector;

const vector<int> &neighbors ( const vector<vector<int>> &adj, int x )
{
    return adj [x];
}

bool hasCycle ( const vector<vector<int>> &adj,
                std::unordered_set<int> &visited,
                std::unordered_set<int> &path,
                int x )
{
    // If a vertex is found that exists on the current path, then a cycle is
    // found.
    visited.insert ( x );
    path.insert ( x );
    for ( const auto &n : neighbors ( adj, x ) ) {
        // first check if it exists on the path already
        if ( path.find ( n ) != path.end() ) {
            return true;
        }

        // skip if already visited
        if ( visited.find ( n ) != visited.end() ) {
            continue;
        }

        if ( hasCycle ( adj, visited, path, n ) ) {
            return true;
        }
    }

    path.erase ( x );
    return false;
}

int acyclic ( vector<vector<int>> &adj )
{
    // for each vertex in graph
    //   if vertex has been visited:
    //     continue
    //   create new connected component set
    //   depth first search (return found cycle; fills in connected component
    //   set which it uses to find a cycle) add set to visited vertices
    std::unordered_set<int> visited;
    std::unordered_set<int> path;
    for ( int i { 0 }; i < static_cast<int> ( adj.size() ); ++i ) {
        if ( visited.find ( i ) != visited.end() ) {
            continue;
        }

        if ( hasCycle ( adj, visited, path, i ) ) {
            return 1;
        }
    }
    return 0;
}

int main()
{
    size_t n, m;
    std::cin >> n >> m;
    vector<vector<int>> adj ( n, vector<int>() );
    for ( size_t i = 0; i < m; i++ ) {
        int x, y;
        std::cin >> x >> y;
        adj [x - 1].push_back ( y - 1 );
    }
    std::cout << acyclic ( adj );
}
