#include <iostream>
#include <array>

int main(int argc, char* argv[])
{
  // An array is homogeneous (holds the same type of elements) and contiguous (memory locations are adjacent)
  // There is no bounds checking
  // Declaration by initialization
  int num_array[] = {1,4,6,8};

  double dec_array[5] = {0.2, 0.6, 1.3, 4.4, 23.3};

  // printing the addresses show that the memory addresses are contiguous
  printf("Size of int on current architecture and compiler is %lu.\n", sizeof(int));
  printf("sizeof(num_array) = %lu\n",sizeof(num_array));
  unsigned max_i = sizeof(num_array)/sizeof(int);
  for (unsigned i=0; i<max_i; ++i)
    {
      printf("The address of element %d is: %p\n",i,&num_array[i]);
    }
  
  std::cout << "Testing the standard output stream." << std::endl;

  // string arrays
  // char* char_array[] = "hello"; // NOT ALLOWED => array of char*
  char char_array1[] = "hello";
  char char_array2[] = {'w','h','a','t','?', 0};
  //char char_array3[5] = "hello"; // the size does not allow for a null character => does not compile
  char char_array4[7] = "hello"; // the size allows for a NULL character
  char* char_pt_array[] = {char_array1, char_array2};
  printf("char_array1 is \'%s\'\n",char_pt_array[0]);
  printf("char_array2 is \'%s\'\n",char_pt_array[1]);
  printf("char_array4 is \'%s\'\n",char_array4);
  printf("char_array4[5] = %d\n",static_cast<int>(char_array4[5]));
  
  // std::array
  // Combines speed of c array with reliability of a container
  std::array<int, 5> nums = {3,6,77,88,92}; // supports aggregate intialization
  std::cout << "nums has " << nums.size() << " elements." << std::endl;

  try
    {
      int var = nums.at(5);
      printf("nums[5] = %d.\n",var);
    }
  catch (...)
    {
      printf("Caught exception.\n");
    }

  for (const auto num : nums)
    {
      printf("The number is %d.\n",num);
    }
  
  return 0;
}
