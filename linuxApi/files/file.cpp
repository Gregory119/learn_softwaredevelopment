#include <iostream>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

class File {
public:
  /*
    @param flags Bitwise OR of access mode flags, file creation flags, and file status flags.
    @param mode The file mode bits to be applied if the file is created.
   */
  File ( std::string path,
	 int flags,
	 mode_t mode = 0 );

  // close on destruction

  // This must be called first and be successful before calling any other read/write functions.
  bool open();

  //void close();
  
  // write
  // read

private:
  std::string m_filepath;
  int m_flags = 0;
  mode_t m_mode = 0;
  int m_fd = -1;
};

//----------------------------------------------------------------------//
File::File ( std::string path,
	     int flags,
	     mode_t mode )
  : m_filepath ( std::move ( path ) ),
    m_flags ( flags ),
    m_mode ( mode )
{}

//----------------------------------------------------------------------//
bool File::open()
{
  m_fd = ::open ( m_filepath.data(),
		  m_flags,
		  m_mode );
  
  return m_fd != -1;
}

//----------------------------------------------------------------------//
int main ( int argc, char** argv )
{
  if ( argc != 2 ) {
    std::cout << argv[0] << " [filepath]" << std::endl;
    return EXIT_SUCCESS;
  }
  
  File file ( argv[1],
	      O_RDWR | // access mode - read and write.
	      O_TRUNC | // file creation flag - truncate the regular file to 0 length.
	      O_CREAT, // file creation flag - create if the file does not exist.
	      S_IRWXU |
	      S_IRWXG |
	      S_IRWXO );

  bool success = file.open();
  std::cout << "success = " << success << std::endl;
	      
  return EXIT_SUCCESS;
}
